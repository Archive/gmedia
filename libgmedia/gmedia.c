/* gmedia - The Gnome Media library
   Copyright (C) 1998 Tristan Tarrant
   This module is based in part on the SANE dll backend 
   Copyright (C) 1996, 1997 David Mosberger-Tang
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

*/

#include <config.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdarg.h>
#if defined(HAVE_DLOPEN) && defined(HAVE_DLFCN_H)
# include <dlfcn.h>
/* Older versions of dlopen() don't define RTLD_NOW and RTLD_LAZY.
   They all seem to use a mode of 1 to indicate RTLD_NOW and some do
   not support RTLD_LAZY at all.  Hence, unless defined, we define
   both macros as 1 to play it safe.  */
# ifndef RTLD_NOW
#  define RTLD_NOW	1
# endif
# ifndef RTLD_LAZY
#  define RTLD_LAZY	1
# endif
#endif

#include <sys/types.h>
#include <glib.h>
#include "gmedia.h"

#ifndef PATH_MAX
#define PATH_MAX	1024
#endif

#define DBG(msg, args...)	gmedia_dbg_printf (msg, ##args);

enum GMEDIA_Ops {
  OP_INIT = 0,
  OP_EXIT,		/* This function should terminate the backend */
  OP_CHECK,		/* check if a backend understands a certain file or device */
  OP_GET_DRIVER_INFO,	/* get the backend's name */
  OP_LOAD,		/* Loads a file using an appropriate backend */
  OP_PLAY,		/* Tells the current backend to play the loaded file */
  OP_PAUSE,		/* unpause the file */
  OP_STOP,		/* stop the file */
  OP_FORWARD,		/* go forward one step */
  OP_REWIND,		/* go back one step */
  OP_RESTART,		/* restarts the file */
  OP_EJECT,		/* unloads the file, but keep the backend */
  OP_GET_N_FRAMES,	/* gets the number of available frames for the current track */
  OP_SET_FRAME,		/* sets the current frame for the current track*/
  OP_GET_FRAME,		/* gets the current frame */
  OP_GET_N_TRACKS,	/* gets the number of available tracks */
  OP_SET_TRACK,		/* sets the current track */
  OP_GET_TRACK,		/* gets the current track */
  OP_GET_MEDIA_NAME,	/* get the name of the media we are playing (e.g. CD title) */
  OP_GET_TRACK_NAME,	/* get the name of the specified track */
  OP_NEEDS_DRAWABLE,	/* does the backend need a drawable (video) ? */
  OP_GET_DRAWABLE_SIZE,	/* get the required drawable size */
  OP_SET_DRAWABLE_SIZE,	/* set the drawable size */
  OP_SET_DRAWABLE,	/* set the drawable */
  OP_IS_STREAMABLE,
  OP_WAIT,
  OP_GET_FD,
  OP_SET_CALLBACK,
  OP_GET_MODE,
  OP_GET_NUM_PARAMETERS,
  OP_GET_PARAMETER,
  OP_SET_PARAMETER,
  NUM_OPS
};

static const char *op_name[] =
{
  "init", 
  "exit", 
  "check",
  "get_driver_info",
  "load", 
  "play", 
  "pause", 
  "stop",
  "forward", 
  "rewind", 
  "restart", 
  "eject", 
  "get_n_frames", 
  "set_frame",
  "get_frame", 
  "get_n_tracks", 
  "set_track", 
  "get_track", 
  "get_name",
  "get_track_name", 
  "needs_drawable", 
  "get_drawable_size", 
  "set_drawable_size",
  "set_drawable",
  "is_streamable",
  "wait",
  "get_fd",
  "set_callback",
  "get_mode",
  "get_num_parameters",
  "get_parameter",
  "set_parameter"
};

struct backend {
  struct backend *next;
  const gchar *name;
  guint loaded : 1;		/* are the functions available? */
  guint initialized : 1;		/* has the backend been initialized? */
  guint enabled : 1;
  void *handle;			/* handle returned by dlopen() */
  void *(*op[NUM_OPS]) ();
};

static struct backend *first_backend;
static int ref_count = 0;

static int debugging = -1;

void gmedia_dbg_printf(const char *fmt, ...)
{
  va_list ap;
  if(debugging<0)
    debugging = (getenv("DEBUG")!=NULL);
  if(debugging) {
    fprintf(stderr,"libgmedia: ");
    va_start (ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end (ap);
  }
}

#ifndef HAVE_STRSEP

char *strsep (char **stringp, const char *delim)
{
  char *begin, *end;

  begin = *stringp;
  if (! begin || *begin == '\0')
    return NULL;

  /* Find the end of the token.  */
  end = strpbrk (begin, delim);
  if (end) {
    /* Terminate the token and set *STRINGP past NUL character.  */
    *end++ = '\0';
    *stringp = end;
  } else
    /* No more delimiters; this is the last token.  */
    *stringp = NULL;
  
  return begin;
}

#endif /* !HAVE_STRSEP */

static void *op_unsupported (void)
{
  return (void *) (int) GMEDIA_STATUS_UNSUPPORTED;
}

static int add_backend (const char *name, struct backend **bep)
{
  struct backend *be, *prev;

  for (prev = 0, be = first_backend; be; prev = be, be = be->next)
    if (strcmp (be->name, name) == 0) {
      /* move to front so we preserve order that we'd get with
	 dynamic loading: */
	if (prev) {
	  prev->next = be->next;
	  be->next = first_backend;
	  first_backend = be;
	}
	if (bep)
	  *bep = be;
	return GMEDIA_STATUS_GOOD;
    }
  DBG("Adding backend: %s\n", name);
  be = malloc (sizeof (*be));
  if (!be)
    return GMEDIA_STATUS_NO_MEM;
  
  memset (be, 0, sizeof (*be));
  be->enabled = 1;
  be->name = strdup (name);
  if (!be->name)
    return GMEDIA_STATUS_NO_MEM;
  be->next = first_backend;
  first_backend = be;
  if (bep)
    *bep = be;
  return GMEDIA_STATUS_GOOD;
}

static int load_backend (struct backend *be)
{
#ifdef HAVE_DLOPEN
  int mode = getenv ("LD_BIND_NOW") ? RTLD_NOW : RTLD_LAZY;
  char *funcname, *src, *dst, *dir, *path = 0;
  char libname[PATH_MAX], line[PATH_MAX];
  int i, found = 0;
  FILE *fp = 0;
# define PREFIX "libgmedia_"
# define POSTFIX ".la"

  if(be->loaded)
    return GMEDIA_STATUS_GOOD;

  /* initialize all ops to "unsupported" so we can "use" the backend
     even if the stuff later in this function fails */
  be->loaded = 1;
  be->handle = 0;
  be->initialized = 0;
  for (i = 0; i < NUM_OPS; ++i)
    be->op[i] = op_unsupported;

  dir = LIBDIR;
  while (dir) {
    g_snprintf (libname, sizeof (libname), "%s/"PREFIX"%s"POSTFIX,
		dir, be->name);
    fp = fopen (libname, "r");
    if (fp)
      break;
    
    if (!path){
      path = getenv ("LD_LIBRARY_PATH");
      if (!path) {
	path = getenv ("SHLIB_PATH");	/* for HP-UX */
	if (!path)
	  path = getenv ("LIBPATH");	/* for AIX */
      }
      if (!path)
	break;
      
      path = strdup (path);
      src = path;
    }
    dir = strsep (&src, ":");
  }
  if (path)
    free (path);
  if (!fp) {
    return GMEDIA_STATUS_INVAL;
  }
  dst = strrchr (libname, '/');
  if (dst)
    ++dst;
  else
    dst = libname;

  while (fgets (line, sizeof (line), fp)) {
    if (strncmp (line, "dlname='", 8) == 0) {
      if (line[8] == '/')
	/* dlname is an absolute path */
	dst = libname;
      for (src = line + 8; *src && *src != '\''; ++src)
	if (dst >= libname + sizeof (libname) - 1)
	  break;
	else
	  *dst++ = src[0];
      *dst++ = '\0';
      found = 1;
      break;
    }
  }
  fclose (fp);
  if (!found) {
    return GMEDIA_STATUS_INVAL;
  }
  DBG("-- loading backend: %s\n", libname);
  be->handle = dlopen (libname, mode);
  if (!be->handle) {
    DBG("-- error dlopening backend %s. %s\n", libname, dlerror());
    return GMEDIA_STATUS_INVAL;
  }

  /* all is dandy---lookup and fill in backend ops: */
  funcname = alloca (strlen (be->name) + 64);
  for (i = 0; i < NUM_OPS; ++i) {
    void *(*op) ();
    
    sprintf (funcname, "_gmedia_%s_%s", be->name, op_name[i]);
    
    /* First try looking up the symbol without a leading underscore. */
    op = (void *(*)()) dlsym (be->handle, funcname + 1);
    if (op) {
      be->op[i] = op;
    } else {
      /* Try again, with an underscore prepended. */
      op = (void *(*)()) dlsym (be->handle, funcname);
      if (op)
	be->op[i] = op;
    }
  }

  DBG("-- initializing backend\n");
  if ((be->op[OP_INIT])()==0)
    be->initialized = 1;

  return GMEDIA_STATUS_GOOD;

# undef PREFIX
# undef POSTFIX
#else /* HAVE_DLOPEN */
#error "GMedia needs dlopen"
  return GMEDIA_STATUS_UNSUPPORTED;
#endif /* HAVE_DLOPEN */
}

/************************************************************************
 * Externally available functions					*
 ************************************************************************/
int gmedia_init (char *spath)
{
  char line[PATH_MAX];
  DIR *dp;
  struct dirent *dir;
  char *c, *dirname, *path, *searchpath, *src;
  int i;
  
  DBG("gmedia_init()\n");
  if(ref_count++>0)
    return GMEDIA_STATUS_GOOD;

  first_backend = 0;

  path = getenv ("LD_LIBRARY_PATH");
  if (!path) {
    path = getenv ("SHLIB_PATH");	/* for HP-UX */
    if (!path)
      path = getenv ("LIBPATH");	/* for AIX */
  }
  if(path)
    i = strlen(path);
  else
    i = 0;
  if(spath)
    i += strlen(spath)+1;
  searchpath = (char *) malloc(sizeof(char)*(i+strlen(LIBDIR)+2));
  searchpath[0] = 0;
  /* We search the user-defined dir first */
  if(spath) {
    strcat(searchpath, spath);
    strcat(searchpath, ":");
  }
  /* Then the install path */
  strcat(searchpath, LIBDIR);
  /* Any additional system paths */
  if(path) {
    strcat(searchpath, ":");
    strcat(searchpath, path);
  }
  DBG("-- search path: %s\n", searchpath);

  src = searchpath;

  for(;;) {
    dirname = strsep(&src,":");
    if(!dirname)
      break;
    dp = opendir (dirname);
    if(dp) {
      DBG("-- scanning dir: %s\n", dirname);
      while ((dir=readdir(dp))!=NULL) {
	if((!strncmp(dir->d_name,"libgmedia_",strlen("libgmedia_")))
	   &&(strstr(dir->d_name,".la"))) {
	  DBG("-- found backend: %s\n", dir->d_name); 
	  for(i=10; dir->d_name[i]!='.'; i++)
	    line[i-10] = dir->d_name[i];
	  line[i-10] = 0;
	  add_backend (line, 0);
	}
      }
      closedir (dp);
    }
  }

  free (searchpath);

  if(first_backend)
    return GMEDIA_STATUS_GOOD;
  else
    return GMEDIA_STATUS_NOT_FOUND;
}

int gmedia_exit (void)
{
  struct backend *be;

  if(--ref_count>0)
    return;

  DBG("gmedia_exit()\n");
  for(be=first_backend; be; be=be->next) {
    if(be->loaded) {
      if(be->initialized) {
	DBG("-- closing %s backend\n", be->name);
	(*be->op[OP_EXIT]) ();
	be->initialized = 0;
      }
      be->loaded = 0;
#ifdef HAVE_DLOPEN
      if (be->handle)
	dlclose (be->handle);
#endif
    }
  }
  first_backend = 0;
  
  return GMEDIA_STATUS_GOOD;
}

static int gmedia_check (gpointer handle, gchar *url)
{
  struct backend *be = (struct backend *)handle;

  return (int) (be->op[OP_CHECK]) (url);
}

int gmedia_get_driver_list (gchar ***driver_list)
{
  int n_backends, i;
  struct backend *be;
  static gchar **dlist = NULL;

  DBG("gmedia_get_driver_list(0x%x)\n", driver_list);

  if(!dlist) {
    for(be=first_backend, n_backends=0; be; be=be->next, n_backends++);
    dlist = (gchar **) malloc (sizeof(gchar *)*(n_backends+1));
    for(be=first_backend, i=0; be; be=be->next, i++)
      dlist[i] = g_strdup (be->name);
    dlist[n_backends] = 0;
  }
  *driver_list = dlist;
  
  return GMEDIA_STATUS_GOOD;
}

const gchar *gmedia_get_driver_name (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_driver_name(0x%x)\n", handle);
  return (be->name);
}

int gmedia_driver_enable (const gchar *name)
{
  struct backend *be;
  
  DBG("gmedia_driver_enable(\"%s\")\n", name);
  for(be=first_backend; be; be=be->next) {
    if(!strcmp(name,be->name)) {
      be->enabled = 1;
      return GMEDIA_STATUS_GOOD;
    }
  }
  
  return GMEDIA_STATUS_NOT_FOUND;
}

int gmedia_driver_disable (const gchar *name)
{
  struct backend *be;
  
  DBG("gmedia_driver_disable(\"%s\")\n", name);
  for(be=first_backend; be; be=be->next) {
    if(!strcmp(name,be->name)) {
      be->enabled = 0;
      return GMEDIA_STATUS_GOOD;
    }
  }
  
  return GMEDIA_STATUS_NOT_FOUND;
}

int gmedia_get_driver_info (const gchar *name, GMediaDriverInfo *info)
{
  struct backend *be;
  
  DBG("gmedia_get_driver_info(\"%s\", 0x%x)\n", name, info);
  for(be=first_backend; be; be=be->next) {
    if(!strcmp(name,be->name)) {
      load_backend(be);
      return (int) (be->op[OP_GET_DRIVER_INFO]) (info);
    }
  }
  
  return GMEDIA_STATUS_NOT_FOUND;
}

int gmedia_load (gpointer *handle, gchar *url)
{
  struct backend *be;

  DBG("gmedia_load(0x%x, \"%s\")\n", handle, url);
  for(be=first_backend; be; be=be->next) {
    if(be->enabled) {
      load_backend(be);
      /* See if it supports our file */
      if(gmedia_check (be, url)==GMEDIA_STATUS_GOOD) {
	*handle = be;
	return (int) (be->op[OP_LOAD]) (url);
      }
    }
  }
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_play (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_play(0x%x)\n", handle);
  return (int) (be->op[OP_PLAY]) ();
}

int gmedia_pause (gpointer handle)
{
  struct backend *be = (struct backend *)handle;
 
  DBG("gmedia_pause(0x%x)\n", handle);
  return (int) (be->op[OP_PAUSE]) ();
}

int gmedia_stop (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_stop(0x%x)\n", handle);
  return (int) (be->op[OP_STOP]) ();
}

int gmedia_forward (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_forward(0x%x)\n", handle);
  return (int) (be->op[OP_FORWARD]) ();
}

int gmedia_rewind (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_rewind(0x%x)\n", handle);
  return (int) (be->op[OP_REWIND]) ();
}

int gmedia_restart (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_restart(0x%x)\n", handle);
  return (int) (be->op[OP_RESTART]) ();
}

int gmedia_eject (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_eject(0x%x)\n", handle);
  return (int) (be->op[OP_EJECT]) ();
}

int gmedia_get_n_frames (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_n_frames(0x%x)\n", handle);
  return (int) (be->op[OP_GET_N_FRAMES]) ();
}

int gmedia_set_frame (gpointer handle, int nframe)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_set_frame(0x%x, %d)\n", handle, nframe);
  return (int) (be->op[OP_SET_FRAME]) (nframe);
}

int gmedia_get_frame (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_frame(0x%x)\n", handle);
  return (int) (be->op[OP_GET_FRAME]) ();
}

int gmedia_get_n_tracks (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_n_tracks(0x%x)\n", handle);
  return (int) (be->op[OP_GET_N_TRACKS]) ();
}

int gmedia_set_track (gpointer handle, int ntrack)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_set_track(0x%x, %d)\n", handle, ntrack);
  return (int) (be->op[OP_SET_TRACK]) (ntrack);
}

int gmedia_get_track (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_track(0x%x)\n", handle);
  return (int) (be->op[OP_GET_TRACK]) ();
}

const gchar* gmedia_get_media_name (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_media_name(0x%x)\n", handle);
  return (gchar *) (be->op[OP_GET_MEDIA_NAME]) ();
}

const gchar* gmedia_get_track_name (gpointer handle, int ntrack)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_track_name(0x%x, %d)\n", handle, ntrack);
  return (gchar *) (be->op[OP_GET_TRACK_NAME]) (ntrack);
}

int gmedia_needs_drawable (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_needs_drawable(0x%x)\n", handle);
  return (int) (be->op[OP_NEEDS_DRAWABLE]) ();
}

int gmedia_get_drawable_size (gpointer handle, int *width, int *height)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_drawable_size (0x%x, 0x%x, 0x%x)\n", handle, width, height);
  return (int) (be->op[OP_GET_DRAWABLE_SIZE]) (width, height);
}

int gmedia_set_drawable_size (gpointer handle, int *width, int *height)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_set_drawable_size(0x%x, %d, %d)\n", handle, *width, *height);
  return (int) (be->op[OP_SET_DRAWABLE_SIZE]) (width, height);
}

int gmedia_set_drawable (gpointer handle, gpointer display, int drawable)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_set_drawable(0x%x, 0x%x, 0x%x)\n", handle, display, drawable);
  return (int) (be->op[OP_SET_DRAWABLE]) (display, drawable);
}

int gmedia_is_streamable (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_is_streamable(0x%x)\n", handle);
  return (int) (be->op[OP_IS_STREAMABLE]) ();
}

int gmedia_wait (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_wait(0x%x)\n", handle);
  return (int) (be->op[OP_WAIT]) ();
}

int gmedia_get_fd (gpointer handle, int *fd)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_fd(0x%x, 0x%x)\n", handle, fd);
  return (int) (be->op[OP_GET_FD]) (fd);
}

int gmedia_set_callback (gpointer handle, gpointer callback)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_set_callback(0x%x, 0x%x)\n", handle, callback);
  return (int) (be->op[OP_SET_CALLBACK]) (callback);
}

GMediaMode gmedia_get_mode (gpointer handle)
{
  struct backend *be = (struct backend *)handle;
  DBG("gmedia_get_mode(0x%x)\n", handle);
  return (GMediaMode) (be->op[OP_GET_MODE]) ();
}

int gmedia_get_num_parameters (gpointer handle)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_get_num_parameters(0x%x)\n", handle);
  return (int) (be->op[OP_GET_NUM_PARAMETERS]) ();
}

int gmedia_get_parameter (gpointer handle, int n, GMediaValue *value)
{
  struct backend *be = (struct backend *)handle;
  
  DBG("gmedia_get_parameter(0x%x, %d, 0x%x)\n", handle, n, value);
  return (int) (be->op[OP_GET_PARAMETER]) (n, value);
}

int gmedia_set_parameter (gpointer handle, int n, GMediaValue *value)
{
  struct backend *be = (struct backend *)handle;

  DBG("gmedia_set_parameter(0x%x, %d, 0x%x)\n", handle, n, value);
  return (int) (be->op[OP_SET_PARAMETER]) (n, value);
}
