/* gmedia - The Gnome Media library
   Copyright (C) 1998 Tristan Tarrant
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

*/

#ifndef GMEDIA_H
#define GMEDIA_H

#include <glib.h>

typedef enum {
  GMEDIA_MODE_STOPPED = 1,
  GMEDIA_MODE_PLAYING,
  GMEDIA_MODE_PAUSED,
  GMEDIA_MODE_SEEKING,
  GMEDIA_MODE_UNAVAILABLE
} GMediaMode;

typedef enum {
  GMEDIA_STATUS_GOOD = 0,
  GMEDIA_STATUS_ERROR,
  GMEDIA_STATUS_UNSUPPORTED,
  GMEDIA_STATUS_NO_MEM,
  GMEDIA_STATUS_NOT_FOUND,
  GMEDIA_STATUS_INVAL
} GMediaStatus;

typedef enum {
  GMEDIA_TYPE_BOOL = 1,
  GMEDIA_TYPE_INT,
  GMEDIA_TYPE_FLOAT,
  GMEDIA_TYPE_STRING,
  GMEDIA_TYPE_GROUP,
  GMEDIA_TYPE_TAB
} GMediaType;

typedef struct {
  GMediaType type;
  gchar *name;
  gchar *description;
  union {
    gboolean b;
    gint i;
    gfloat f;
    gchar *s;
  } val;
} GMediaValue;

typedef struct {
  gchar *name;
  gchar *full_name;
  gint  version_major;
  gint  version_minor;
} GMediaDriverInfo;

/* Generic functions */
extern int gmedia_init (char *spath);		/* Initialize gmedia */
extern int gmedia_exit (void);		/* Close gmedia */
extern int gmedia_get_driver_list (gchar ***driver_list);	/* Returns a list of available driver names */
extern int gmedia_get_driver_info (const gchar *name, GMediaDriverInfo *info);	/* Returns the full name of a specified driver name */
extern int gmedia_driver_enable (const gchar *name);
extern int gmedia_driver_disable (const gchar *name);

/* Driver-specific functions */
extern const gchar *gmedia_get_driver_name (gpointer handle);
extern int gmedia_load (gpointer *handle, gchar *url);		/* Load a specific file and return a handle to the player */
extern int gmedia_play (gpointer handle);			/* Start playing the currently loaded file */
extern int gmedia_pause (gpointer handle);			/* Pauses/unpauses playback of the current file */
extern int gmedia_stop (gpointer handle);			/* Stop playback of the current file */
extern int gmedia_forward (gpointer handle);			/* Step forward to the next frame */
extern int gmedia_rewind (gpointer handle);			/* Step backwards to the previous frame */
extern int gmedia_restart (gpointer handle);                    /* Restart the current track from the beginning */
extern int gmedia_eject (gpointer handle);			/* Unloads the media. The handle becomes invalid */
extern int gmedia_get_n_frames (gpointer handle);		/* Returns the number of available frames */
extern int gmedia_set_frame (gpointer handle, int nframe);	/* Sets the current frame */
extern int gmedia_get_frame (gpointer handle);			/* Returns the current frame */
extern int gmedia_get_n_tracks (gpointer handle);		/* Returns the number of available tracks */
extern int gmedia_set_track (gpointer handle, int ntrack);	/* Sets the current track */
extern int gmedia_get_track (gpointer handle);			/* Returns the current track */
extern const gchar* gmedia_get_media_name (gpointer handle);	/* Returns the media title */
extern const gchar* gmedia_get_track_name (gpointer handle, int ntrack);	/* Returns the track title */
extern int gmedia_needs_drawable (gpointer handle);		/* Determines if the media needs a drawable (video) */
extern int gmedia_get_drawable_size (gpointer handle, int *width, int *height);	/* Returns the default media size */
extern int gmedia_set_drawable_size (gpointer handle, int *width, int *height);	/* Attempts to set a specific size */
extern int gmedia_set_drawable (gpointer handle, gpointer display, int drawable);	/* Sets the drawable */
extern int gmedia_is_streamable (gpointer handle);	/* Returns true or false whether the media player can stream its data */
extern int gmedia_wait (gpointer handle);		/* Waits for playback to complete */
extern int gmedia_get_fd (gpointer handle, int *fd);	/* Gets a file descriptor on which to wait for playback completion */
extern int gmedia_set_callback (gpointer handle, gpointer callback);	/* Sets a callback to be invoked when playback is complete */
/* extern int gmedia_play_sync (int n, ...);	/* Starts playing several media together */
/* extern int gmedia_wait_sync (int n, ...);	/* Waits for several media to complete */
extern GMediaMode gmedia_get_mode (gpointer handle);	/* Gets the current status of a media player */
extern int gmedia_get_num_parameters (gpointer handle);	/* Gets the number of configurable parameters */
extern int gmedia_get_parameter (gpointer handle, int n, GMediaValue *value);	/* Fetches the current value of a parameter */
extern int gmedia_set_parameter (gpointer handle, int n, GMediaValue *value);	/* Sets the value of a parameter */

#endif
