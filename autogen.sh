#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GMedia Library"

(test -f $srcdir/configure.in \
  && test -d $srcdir/libgmedia) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gmedia directory"
    exit 1
}

. $srcdir/macros/autogen.sh
