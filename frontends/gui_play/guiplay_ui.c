#include <config.h>
#include <gtk/gtk.h>

#include "libgmedia/gmedia.h"

#include "guiplay.h"

/* Icons */
#include "play.xpm"
#include "eject.xpm"
#include "next_t.xpm"
#include "prev_t.xpm"
#include "stop.xpm"
#include "pause.xpm"
#include "ff.xpm"
#include "rw.xpm"

GtkTooltips *tooltips;
GtkWidget *app;
static GtkWidget *clist = NULL;

static GtkItemFactoryEntry menu_items[] =
{
        {N_("/File/Playlist..."), N_("<control>N"), playlist_cb, NULL},
        {N_("/File/Quit"), N_("<control>Q"), quit_cb, N_("OK, I'll quit")}
};
static int nmenu_items = sizeof(menu_items) / sizeof(menu_items[0]);
static GtkItemFactory *menu_factory = NULL;

void player_cb (GtkWidget *widget, void *data)
{
  int mode = (int)data;
  int wasplaying;

  switch(mode) {
  case XPLAY:
    if(guiplay.gmedia_handle&&gmedia_get_mode(guiplay.gmedia_handle)!=GMEDIA_MODE_STOPPED)
      break;
    if(guiplay.current_file) {
      gmedia_load (&guiplay.gmedia_handle, guiplay.current_file->data);
      gmedia_play (guiplay.gmedia_handle);
    }
    break;
  case XSTOP:
    if(guiplay.gmedia_handle&&gmedia_get_mode(guiplay.gmedia_handle)!=GMEDIA_MODE_STOPPED)
      gmedia_stop (guiplay.gmedia_handle);
    break;
  case XPAUSE:
    if(guiplay.gmedia_handle&&gmedia_get_mode(guiplay.gmedia_handle)!=GMEDIA_MODE_STOPPED)
      gmedia_pause (guiplay.gmedia_handle);
    break;
  case XFWD:
    if(guiplay.gmedia_handle&&gmedia_get_mode(guiplay.gmedia_handle)!=GMEDIA_MODE_STOPPED)
      gmedia_forward(guiplay.gmedia_handle);
    break;
  case XREW:
    if(guiplay.gmedia_handle&&gmedia_get_mode(guiplay.gmedia_handle)!=GMEDIA_MODE_STOPPED)
      gmedia_rewind(guiplay.gmedia_handle);
    break;
  case XNEXT:
    if(guiplay.gmedia_handle&&gmedia_get_mode(guiplay.gmedia_handle)!=GMEDIA_MODE_STOPPED) {
      gmedia_stop (guiplay.gmedia_handle);
      wasplaying = 1;
    } else
      wasplaying = 0;
    if(guiplay.current_file)
      if(guiplay.current_file->next)
	guiplay.current_file = guiplay.current_file->next;
      else
	guiplay.current_file = guiplay.file_list;
    if(wasplaying&&guiplay.current_file) {
      gmedia_load (&guiplay.gmedia_handle, guiplay.current_file->data);
      gmedia_play (guiplay.gmedia_handle);
    }
    break;
  default:
    break;
  }
}

GtkWidget *make_button_with_pixmap( char **pic, GtkWidget *box, int func,
				    gint expand, gint fill, gchar *tooltip )
{
  GtkWidget *button, *pixmapwid;
  GdkPixmap *pixmap, *mask;
  GtkStyle *style;

  style = gtk_widget_get_style( app );
  pixmap = gdk_pixmap_create_from_xpm_d( app->window,  &mask,
					 &style->bg[GTK_STATE_NORMAL],
					 (gchar **)pic );
  pixmapwid = gtk_pixmap_new( pixmap, mask );
  gtk_widget_show (pixmapwid);
  button = gtk_button_new();
  gtk_container_add( GTK_CONTAINER(button), pixmapwid );
  gtk_box_pack_start( GTK_BOX (box), button, expand, fill, 0 );
  gtk_signal_connect(GTK_OBJECT (button), "clicked",
		     GTK_SIGNAL_FUNC (player_cb), (gpointer*)func );
  
  gtk_tooltips_set_tip( tooltips, button, tooltip, "" );
  
  gtk_widget_show_all(button);
  return button;
}

void timer_callback (gpointer data)
{
}

void prepare_app ()
{
  GtkWidget *vbox, *hbox, *frame, *menubar;

  app = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (app), _("GUIPlay"));
  gtk_widget_realize (app);
  gtk_signal_connect (GTK_OBJECT (app), "delete_event",
                      GTK_SIGNAL_FUNC (quit_cb),
                      NULL);

  tooltips = gtk_tooltips_new();
  gtk_tooltips_enable (tooltips);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (app), vbox);
  gtk_widget_show (vbox);

#ifdef ENABLE_NLS
  {
      int i;
      for (i=0;i<2;i++) {
          menu_items[i].path=_(menu_items[i].path);
          menu_items[i].accelerator=_(menu_items[i].accelerator);
      }
   }
#endif
  menu_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<Menu>", NULL);
  gtk_item_factory_create_items (menu_factory, nmenu_items,
				 menu_items, NULL);
  menubar = menu_factory->widget;
  gtk_box_pack_start (GTK_BOX (vbox), menubar, FALSE, TRUE, 0);
  gtk_widget_show (menubar);

  hbox = gtk_hbox_new (TRUE, 0);
  gtk_container_add( GTK_CONTAINER(vbox), hbox );
  gtk_widget_show (hbox);

  make_button_with_pixmap (play_xpm, hbox, XPLAY, TRUE, TRUE, _("Play"));
  make_button_with_pixmap (pause_xpm, hbox, XPAUSE, TRUE, TRUE, _("Pause"));
  make_button_with_pixmap (stop_xpm, hbox, XSTOP, TRUE, TRUE, _("Stop"));
  make_button_with_pixmap (eject_xpm, hbox, XEJECT, TRUE, TRUE, _("Exit"));

  hbox = gtk_hbox_new (TRUE, 0);
  gtk_container_add( GTK_CONTAINER(vbox), hbox );
  gtk_widget_show (hbox);
  
  make_button_with_pixmap (prev_t_xpm, hbox, XPREV, TRUE, TRUE, _("Previous Track"));
  make_button_with_pixmap (rw_xpm, hbox, XREW, TRUE, TRUE, _("Rewind"));
  make_button_with_pixmap (ff_xpm, hbox, XFWD, TRUE, TRUE, _("Fast Forward"));
  make_button_with_pixmap (next_t_xpm, hbox, XNEXT, TRUE, TRUE, _("Next Track"));
  
  gtk_widget_show (app);
  gtk_timeout_add(250, (GtkFunction)timer_callback, NULL);
}

void fill_playlist ()
{
  if(clist) {
    char text[2][256];
    char *texts[2];
    char *cptr;
    GList *l;
    int i;

    gtk_clist_freeze (GTK_CLIST (clist));
    gtk_clist_clear (GTK_CLIST(clist));
    l = guiplay.file_list;
    for (i = 0; i < 2; i++)
      texts[i] = text[i];

    while(l) {
      cptr = strrchr(l->data,'/');
      if(!cptr)
	cptr = l->data;
      else
	cptr++;
      strcpy(text[0],cptr);
      /*      strcpy(text[1],get_file_type(cptr));*/
      strcpy(text[1],"");
      gtk_clist_append (GTK_CLIST (clist), texts);
      l = l->next;
    }
    gtk_clist_thaw (GTK_CLIST (clist));
  }
}

/* Callbacks functions */
void quit_cb (GtkWidget *widget, void *data)
{ 
  gtk_main_quit ();
}

void about_cb (GtkWidget *widget, void *data)
{
  GtkWidget *about;
  gchar *authors[] = {
    "Tristan Tarrant",
    NULL
  };

  return;
}

void file_ok_cb (GtkWidget        *w,
		 GtkFileSelection *fs)
{
  char *filename;
  GList *list;

  filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs));
  guiplay.file_list = g_list_append( guiplay.file_list, g_strdup(filename) );
  fill_playlist ();
  if(!guiplay.current_file)
    guiplay.current_file = guiplay.file_list;
  gtk_widget_hide (GTK_WIDGET (fs));
}

void hide_cb (GtkWidget *widget, void *data)
{
  gtk_widget_hide (GTK_WIDGET (data));
}

void open_cb (GtkWidget *widget, void *data)
{
  static GtkWidget *window = NULL;
  GtkWidget *button;
  
  if (!window) {
    window = gtk_file_selection_new (_("Load Module File"));
    
    gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));
    
    gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);
    
    gtk_signal_connect (GTK_OBJECT (window), "destroy",
			GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			&window);
    
    gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
			"clicked", GTK_SIGNAL_FUNC(file_ok_cb),
			window);
    gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
			       "clicked", GTK_SIGNAL_FUNC(hide_cb),
			       GTK_OBJECT (window));
  }
  
  if (!GTK_WIDGET_VISIBLE (window))
    gtk_widget_show (window);  
  else
    gtk_widget_hide (window);
}

void clearlist_cb (GtkWidget *widget, void *data)
{
  gtk_clist_clear (GTK_CLIST (data));
  g_list_free (guiplay.file_list);
  guiplay.file_list = NULL;
  guiplay.current_file = NULL;
}

void remove_cb (GtkWidget *widget, void *data)
{
  GList *list;
  gint row;
  char *text;

  list = GTK_CLIST (clist)->selection;
  while (list) {
    row = (gint)(list->data);
    gtk_clist_get_text(clist, row, 0, &text);
    list = list->next;
  }
}

void playlist_cb (GtkWidget *widget, void *data)
{ 
  static GtkWidget *playlist = NULL;
  GtkWidget *scrolled;
  static char *titles[] = {
    N_("File name"),
    N_("Type")
  };
  
  if(!playlist) {
    GtkWidget *vbox, *hbox;
    GtkWidget *button;
    playlist = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    
    hbox = gtk_hbox_new (FALSE,5);
    gtk_container_add (GTK_CONTAINER (playlist), hbox);
    gtk_widget_show (hbox);

#ifdef ENABLE_NLS
    titles[0]=_(titles[0]);
    titles[1]=_(titles[1]);
#endif
    clist = gtk_clist_new_with_titles(2, titles);
    gtk_container_border_width (GTK_CONTAINER (clist), 5);
    gtk_clist_set_column_width (GTK_CLIST (clist), 0, 200);
    gtk_clist_set_column_width (GTK_CLIST (clist), 1, 100);
    gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_MULTIPLE);
/*    gtk_clist_set_policy (GTK_CLIST (clist), 
			  GTK_POLICY_AUTOMATIC,
			  GTK_POLICY_AUTOMATIC); */
    gtk_clist_set_column_justification (GTK_CLIST (clist), 0, GTK_JUSTIFY_LEFT);
    gtk_clist_set_column_justification (GTK_CLIST (clist), 1, GTK_JUSTIFY_RIGHT);
    gtk_clist_column_titles_passive (clist);
    gtk_widget_set_usize (clist, 350, 100);
    scrolled = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), 
                                    GTK_POLICY_AUTOMATIC,
                                    GTK_POLICY_AUTOMATIC);
    gtk_container_add (GTK_CONTAINER (scrolled), clist);
    gtk_box_pack_start (GTK_BOX (hbox), scrolled, TRUE, TRUE, 0);
    gtk_widget_show_all (scrolled);
    
    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_add (GTK_CONTAINER (hbox), vbox);
    gtk_widget_show (vbox);
    

    button = gtk_button_new_with_label (_("Add..."));
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button),
			"clicked",
			(GtkSignalFunc) open_cb,
			(gpointer) clist);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Remove"));
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button),
			"clicked",
			(GtkSignalFunc) remove_cb,
			(gpointer) clist);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Clear"));
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button),
			"clicked",
			(GtkSignalFunc) clearlist_cb,
			(gpointer) clist);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Save..."));
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Load..."));
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    button = gtk_button_new_with_label (_("Ok"));
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button),
			"clicked",
			(GtkSignalFunc) hide_cb,
			(gpointer) playlist);
    gtk_widget_show (button);
  }
  fill_playlist ();
  if (!GTK_WIDGET_VISIBLE (playlist))
    gtk_widget_show (playlist);
  else
    gtk_widget_hide (playlist);
}
