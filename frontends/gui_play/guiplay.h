#ifndef GUIPLAY_H
#define GUIPLAY_H

#include <config.h>
#include <gtk/gtk.h>

#ifdef ENABLE_NLS
#    include <libintl.h>
#    define _(String) gettext (String)
#    ifdef gettext_noop
#        define N_(String) gettext_noop (String)
#    else
#        define N_(String) (String)
#    endif
#else
/* Stubs that do something close enough.  */
#    define textdomain(String) (String)
#    define gettext(String) (String)
#    define dgettext(Domain,Message) (Message)
#    define dcgettext(Domain,Message,Type) (Message)
#    define bindtextdomain(Domain,Directory) (Domain)
#    define _(String) (String)
#    define N_(String) (String)
#endif

typedef struct guiplay_t {
  GList *file_list, *current_file;
  int auto_next;
  gpointer gmedia_handle;
} guiplay_t;

extern guiplay_t guiplay;

extern GtkTooltips *tooltips;
extern GtkWidget *lcd;
extern GdkColor darkgrey, timecolor, trackcolor;

enum { XPLAY, XPAUSE, XSTOP, XPREV, XNEXT, XFWD, XREW, XEJECT };

extern void lcd_update();
extern void prepare_app();

extern void about_cb (GtkWidget *widget, void *data);
extern void quit_cb (GtkWidget *widget, void *data);
extern void play_cb (GtkWidget *widget, void *data);
extern void stop_cb (GtkWidget *widget, void *data);
extern void pause_cb (GtkWidget *widget, void *data);
extern void open_cb (GtkWidget *widget, void *data);
extern void playlist_cb (GtkWidget *widget, void *data);

#endif
