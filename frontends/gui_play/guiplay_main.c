/* guiplay_main.c

/* Copyright (C) 1998 Tristan Tarrant, all rights reserved */

#include <config.h>
#include <gtk/gtk.h>

#include "libgmedia/gmedia.h"

#include "guiplay.h"

guiplay_t guiplay;

int main(int argc, char *argv[])
{
  int res;

#ifdef ENABLE_NLS
#ifndef GNOMELOCALEDIR
#define GNOMELOCALEDIR "/usr/share/locale"
#endif
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);
#endif
  res = gmedia_init (NULL);
  if(res!=GMEDIA_STATUS_GOOD) {
    if(res==GMEDIA_STATUS_NOT_FOUND)
      printf (_("No gmedia drivers available\n"));
    else
      printf (_("Could not initialize gmedia\n"));
    exit(1);
  }
  
  guiplay.file_list = NULL;
  guiplay.current_file = NULL;
  guiplay.gmedia_handle = NULL;
    
  gtk_init (&argc, &argv);
    
  prepare_app ();
  gtk_main ();

  gmedia_exit ();

  g_list_free (guiplay.file_list);
  return 0;
}
