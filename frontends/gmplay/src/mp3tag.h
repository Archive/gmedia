/*   mp3tag.h
 *	Copyright (C) 1998  Marek Blaszkowski (mb@kis.p.lodz.pl).
 *  Copyright (C) 1998  Marcin Baranowski (minek@kis.p.lodz.pl).
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <stdio.h>

#define TAG "TAG"

typedef struct {
	unsigned char genre[18];
	unsigned char ch;
} Genre;

#define GMAX 0x74

extern const Genre genre[116];

typedef struct {
	char tag[3];
	char title[30];
	char artist[30];
	char albumn[30];
	char year[4];
	char comment[30];
	char genre;
} mp3Tag;

int readtag(FILE *mp3file, mp3Tag *tag);
void savetag(FILE *mp3file, mp3Tag tag);
char checktag(FILE *mp3file);
void writetag_frag(FILE *mp3file, const char *, char );

