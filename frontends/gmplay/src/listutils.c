/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include "gmplay.h"
#include "list.h"
#include "strings.h"
#include "mp3tag.h"

/****************************
  Filename parsing routines.
 ****************************/
 
/* Currently just copies the filename and returns it. */ 

char *parseFileName(char *fileName)
{
  gint startPos=0, offset=0, strPos=0;
  char *title;
  
  title = (char *)malloc(strlen(fileName)+1);

  for (startPos = 0; fileName[startPos] != '\0'; startPos++)
    if (fileName[startPos] == '/') offset = startPos;      
  
  for (startPos = offset; (fileName[startPos] != '\0') &&
      (fileName[startPos] != '-'); startPos++);

  if (fileName[startPos] != '\0')
    {
      startPos++;          
      title = (char *)malloc(strlen(fileName)-startPos+1);
      
      for (strPos = 0; (strPos <= strlen(fileName)-startPos); strPos++)
        {
          title[strPos] = fileName[strPos+startPos];
          if (title[strPos] == '_') title[strPos] = ' ';
        }       
    }     
  else
    {      
      title = (char *)malloc(strlen(fileName)+1);
      strcpy(title, fileName);
      
      for (strPos = 0; strPos < strlen(title); strPos++)
        if (title[strPos] == '_') title[strPos] = ' ';
    }
    
  if (strcasecmp(&(title[strlen(title)-4]), ".mp3") == 0)
    title[strlen(title)-4] = '\0';
  
  return title;
} 

/************************
  Basic Utility Routines
 ************************/
 
albumnNode *newAlbumn(char *title, albumnNode *parent)
{
  albumnNode *albumn = (albumnNode *)malloc(sizeof(albumnNode));
  
  albumn->title = (char *)malloc(strlen(title)+1);
  strcpy(albumn->title, title);  
  albumn->songs = g_list_alloc();
  albumn->albumns = g_list_alloc();  
  albumn->parent = (gpointer)parent;
  
  return albumn;
}

songNode *newSong(char *fileName, char *title, albumnNode *parentAlbumn)
{
  mp3Tag tag;
  FILE *mp3file;
  songNode *newSong;
  int tagLoaded = FALSE;
  
  newSong = (songNode *)malloc(sizeof(songNode));
  
  newSong->fileName = (char *)malloc(strlen(fileName)+1);
  strcpy(newSong->fileName, fileName);
  
  if ((title != NULL) && (strlen(title) > 0))
    {
      newSong->title = (char *)malloc(strlen(title)+1);
      strcpy(newSong->title, title);
    }
  else
    {
      if (options.autoloadTags)
        {
          mp3file = fopen(fileName, "r");
          if (checktag(mp3file))
            {
              readtag(mp3file, &tag);
              newSong->title = (char *)malloc(strlen(tag.title)+1);
              strcpy(newSong->title, tag.title);
              tagLoaded = TRUE;
            }            
          fclose(mp3file);                    
        }
      
      if (options.autoparse && (! tagLoaded))
        newSong->title = parseFileName(fileName);
      else if (! tagLoaded)
        {
          newSong->title = (char *)malloc(strlen(fileName)+1);
          strcpy(newSong->title, fileName);
        }
    }
    
  newSong->played = FALSE;
  newSong->parent = (gpointer)parentAlbumn;
  
  return newSong;
}
