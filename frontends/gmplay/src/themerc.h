/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <stdio.h>

#ifndef __THEME_RC__
#define __THEME_RC__

#define MAX_KEYWORD_COUNT 10
#define MAX_KEYWORD_LENGTH 80

/* Offset constants for WidgetLoc structure. */

#define NO_LOCATION 0
#define TOP_RIGHT 1
#define TOP_LEFT 2
#define BOTTOM_RIGHT 3
#define BOTTOM_LEFT 4
#define IN_MENU 5

/* Various commands that all widgets can perform. */

#define CMD_NO_COMMAND 0
#define CMD_BACK 1
#define CMD_FORWARD 2
#define CMD_PLAY 3
#define CMD_STOP 4
#define CMD_PAUSE 5
#define CMD_TOGGLE_SHUFFLE 6
#define CMD_TOGGLE_REPEAT 7
#define CMD_TOGGLE_SIZE 8
#define CMD_OPEN_PLAYLIST_EDITOR 9
#define CMD_OPEN_OPTIONS 10
#define CMD_OPEN_ABOUT_BOX 11
#define CMD_OPEN_MIXER 12
#define CMD_POPUP_MENU 13
#define CMD_QUIT 14

/* Structure for defining a widget location. */

typedef struct {
  gint x, y;
  gint location;
} WidgetLoc;

/* Structure for a panel widget. */

typedef struct {
  GtkWidget *widget;
  WidgetLoc location;
  gint command;
} ThemeWidget;

/* Special label structure. */

typedef struct {
  ThemeWidget *widget;
  GtkWidget *eventBox;
  gint width, height;
} ThemeLabel;
    
/* "Themed" window structure. */

typedef struct {
  GtkWidget *main_window;
  GtkWidget *fixed;
  GtkWidget *titleBar;
  GtkWidget *main_menu;

  ThemeLabel song_label;
  ThemeLabel repeat_label;
  ThemeLabel pause_label;
  ThemeLabel shuffle_label;
  ThemeLabel time_label;  

  GList *buttons;
  GList *menu_items;
  ThemeWidget volume_slider;

  GtkTooltips *Tooltips;
  gchar *background;
  gint height, width;
  WidgetLoc *clickPos;
  WidgetLoc position;

  gint left_click_action;
  gint middle_click_action;
  gint right_click_action;
} ThemeWindow;

typedef struct {
  gchar *fileName;
  gint linePos;
  FILE *stream;
} rcFile;

typedef struct {
  gchar words[MAX_KEYWORD_COUNT][MAX_KEYWORD_LENGTH];
  gint wordCount;
} RcTokens;

gint parseThemeRC(ThemeWindow *big_window,
                  ThemeWindow *small_window);

#endif

