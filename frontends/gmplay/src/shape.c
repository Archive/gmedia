/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "gmplay.h"
#include "themerc.h"

int pressed = FALSE;

gint shape_pressed(GtkWidget *widget, GdkEventButton *event, 
                   ThemeWindow *window)
{
  WidgetLoc *p;
  
  if ((event->type == GDK_VISIBILITY_NOTIFY) || (event->button != 1))
    {
      return FALSE;
    }  
    
  p = gtk_object_get_user_data(GTK_OBJECT(widget));
  p->x = (int) event->x;
  p->y = (int) event->y;
  
  gtk_grab_add(widget);
  gdk_pointer_grab(window->main_window->window, TRUE,
                   GDK_BUTTON_RELEASE_MASK |
                   GDK_BUTTON_MOTION_MASK,
                   NULL, NULL, 0);
                     
  gdk_window_raise(window->main_window->window);
  return TRUE;
}                     

gint shape_released(GtkWidget *widget)
{
  gtk_grab_remove(widget);
  gdk_pointer_ungrab(0);
  
  return TRUE;
}

gint shape_motion(GtkWidget *widget, GdkEventMotion *event,
                  ThemeWindow *window)
{
  gint xp, yp;
  WidgetLoc *p;
  GdkModifierType mask;
    
  p = gtk_object_get_user_data(GTK_OBJECT(widget));
   
  gdk_window_get_pointer(root_win, &xp, &yp, &mask);
  window->position.x = xp - p->x;
  window->position.y = yp - p->y;
  
  gtk_widget_set_uposition(window->main_window, 
                           window->position.x,
                           window->position.y);
  gtk_widget_grab_focus(window->main_window);    
  
  return TRUE;
}
