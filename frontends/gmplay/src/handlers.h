/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef __HANDLERS___
#define __HANDLERS__

#include <gtk/gtk.h>
#include <gdk/gdk.h>

typedef gint (*GtkButtonHandler) (GtkWidget *widget, GdkEventButton *event);

void insertCListHandlers(GtkWidget *list);
void insertTreeHandlers(GtkWidget *tree);

gint new_clist_button_press(GtkWidget *widget, GdkEventButton *event);
gint new_clist_button_release(GtkWidget *widget, GdkEventButton *event);

gint new_tree_button_press(GtkWidget *widget, GdkEventButton *event);
gint new_tree_button_release(GtkWidget *widget, GdkEventButton *event);

#endif

