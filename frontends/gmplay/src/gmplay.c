/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#ifdef HAVE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <strings.h>

#include <gdk/gdkx.h>

#include "pixmap.h"
#include "control_funcs.h"
#include "shape.h"
#include "list.h"
#include "mixerpanel.h"
#include "gmplay.h"
#include "mixer.h"
#include "rc_parser.h"
#include "opt_dialog.h"
#include "about.h"
#include "xpmbutton.h"
#include "themerc.h"

GdkWindow *root_win;

configType options;

ThemeWindow *activeWindow;
ThemeWindow bigWindow;
ThemeWindow smallWindow;

pid_t mpgPID;

int smallSize = FALSE;
int playing = FALSE;
int doneSong = TRUE;
int paused = FALSE;
int loopOne = FALSE;
int loopAll = FALSE;
int shuffle = FALSE;
long int startSecs = 0;
long int currSecs = 0;
int oldSeconds = 0;

void bringToFront(GtkWidget *widget, GdkEventVisibility *event);
void createSmallWindow();
void createBigWindow();
void resizeMainWindow();
void installSigHandler();

/********** Main Guts ***********/

void childKill(int signal)
{
  int status;      
  
  if ((waitpid(-1, &status, WNOHANG | WUNTRACED) == mpgPID) &&
      (! WIFSTOPPED(status)))
    {
      mpgPID = 0;
      doneSong = TRUE;
    
    #ifdef __DEBUG__
      g_print(_("mpg123 exited\n"));
    #endif
    }  
  
  installSigHandler();
}

void installSigHandler()
{
  signal(SIGCHLD, childKill);
}

void adjustThemePath()
{
  char *homeDir = getenv("HOME");
  char *fullFile = (char *)malloc(1024);
  
  strcpy(fullFile, homeDir);
  strcat(fullFile, ".gmplay/");
  strcat(fullFile, options.themePack);           
  
  if (access(fullFile, F_OK) < 0)
    {
      strcpy(fullFile, GLOBALTHEMEDIR);
      strcat(fullFile, "/");
      strcat(fullFile, options.themePack);
      
      options.themePath = (char *)malloc(strlen(fullFile)+1);
      strcpy(options.themePath, fullFile);
    }
  else
    {
      options.themePath = (char *)malloc(strlen(fullFile)+1);
      strcpy(options.themePath, fullFile);  
    }
    
  free(fullFile);
}

#define TOKENCOUNT 11

void loadRCFile()
{  
  tokenInfo tokens[TOKENCOUNT] = { { "buffer_size", INTEGER, NULL },
                                   { "downsample", INTEGER, NULL },
                                   { "output_type", INTEGER, NULL },
                                   { "send_to_lineout", BOOLEAN, NULL },
                                   { "managed", BOOLEAN, NULL },
                                   { "tooltips", BOOLEAN, NULL },
                                   { "autoload", STRING, NULL },
                                   { "autoparse", BOOLEAN, NULL},
                                   { "mixerapp", STRING, NULL },
                                   { "themepack", STRING, NULL },
                                   { "autoloadtags", BOOLEAN, NULL} };
                                                             
  char *homeDir = getenv("HOME");
  char *rcFile = (char *)malloc(strlen(homeDir)+strlen("gmplayrc")+
                                strlen(".gmplay")+2);
  
  strcpy(rcFile, homeDir);
  strcat(rcFile, ".gmplay/gmplayrc");
  
  options.bufferSize = 1500;
  options.downSample = 0;              /* Create default settings. */
  options.output = 2;
  options.lineOut = FALSE;
  options.managed = TRUE;
  options.toolTips = TRUE;
  options.playListName = (char *)malloc(255);
  options.playListName[0] = '\0';
  options.autoparse = FALSE;  
  options.mixerApp = (char *)malloc(255);
  options.mixerApp[0] = '\0';
  options.themePack = (char *)malloc(1024);
  options.autoloadTags = FALSE;
  strcpy(options.themePack, "default");  

  tokens[0].dataBuffer = &options.bufferSize;
  tokens[1].dataBuffer = &options.downSample;
  tokens[2].dataBuffer = &options.output;     /* Set up the tokens correctly */
  tokens[3].dataBuffer = &options.lineOut;
  tokens[4].dataBuffer = &options.managed;
  tokens[5].dataBuffer = &options.toolTips;
  tokens[6].dataBuffer = options.playListName;
  tokens[7].dataBuffer = &options.autoparse;
  tokens[8].dataBuffer = options.mixerApp;
  tokens[9].dataBuffer = options.themePack;
  tokens[10].dataBuffer = &options.autoloadTags;
  
  parseFile(rcFile, tokens, TOKENCOUNT);

  if (options.playListName[0] != '\0')  /* Read playlist from autoload param */
    loadAlbumns(options.playListName);
  
  adjustThemePath();

  free(rcFile);
}  

void saveRCFile()
{
  FILE *outputFile;
  char *homeDir = getenv("HOME");
  char rcFile[] = "gmplayrc";
  char rcDir[] = ".gmplay";
  
  if (access(rcDir, F_OK) < 0)
    mkdir(rcDir, 0755);
  
  outputFile = fopen(rcFile, "w");
  
  if (outputFile)
    {  
      fprintf(outputFile, "buffer_size = %d\n", options.bufferSize);
      fprintf(outputFile, "downsample = %d\n", options.downSample);
      fprintf(outputFile, "output_type = %d\n", options.output);
  
      if (options.lineOut)
        fprintf(outputFile, "send_to_lineout = TRUE\n");
      else
        fprintf(outputFile, "send_to_lineout = FALSE\n");
  
      if (options.managed)
        fprintf(outputFile, "managed = TRUE\n");
      else
        fprintf(outputFile, "managed = FALSE\n");
    
      
      if (options.toolTips)
        fprintf(outputFile, "tooltips = TRUE\n");
      else
        fprintf(outputFile, "tooltips = FALSE\n");  
        
      if (options.autoparse)
        fprintf(outputFile, "autoparse = TRUE\n");
      else
        fprintf(outputFile, "autoparse = FALSE\n");    
        
      if (options.autoloadTags)
        fprintf(outputFile, "autoloadtags = TRUE\n");
      else
        fprintf(outputFile, "autoloadtags = FALSE\n");    
        
      if (options.playListName[0] != '\0')
        fprintf(outputFile, "autoload = %s\n", options.playListName);

      if (options.mixerApp[0] != '\0')
        fprintf(outputFile, "mixerapp = %s\n", options.mixerApp);
        
      fprintf(outputFile, "themepack = %s\n", options.themePack);
              
      fclose(outputFile);    
    }  
}

void loadCommandlineFiles(int argc, char *argv[])
{
  int i;
  
  for (i = 1; i < argc; i++)
    {
      addFileToPlayList(argv[i]);
      
      playing = TRUE;
      doneSong = TRUE;
    }
}

void printSongName()
{          
  char *currentSongName;
  
  if (activeWindow->song_label.eventBox == NULL)
    return;

  if ((activePlayList == NULL) || (g_list_first(activePlayList)->next == NULL))
    return;

  currentSongName = ((songNode *)(currentSong->data))->title;
  gtk_label_set(GTK_LABEL(activeWindow->song_label.widget->widget), currentSongName);
}

void resetClock()
{
  if (activeWindow->time_label.eventBox == NULL)
    return;
    
  gtk_label_set(GTK_LABEL(activeWindow->time_label.widget->widget), "0:00");
}

int playLoop()
{  
  if (playing && doneSong)
    {    
      if (! nextSong(TRUE))
        {
          play(NULL, &options);

          playing = TRUE;
          doneSong = FALSE;
                    
          printSongName();
        }  
      else
        {
          if (shuffle)
            clearPlayedFlags();
            
          playing = FALSE;
          doneSong = TRUE;
          
          currentSong = g_list_first(activePlayList)->next;
        }  
                
      startSecs = currSecs+1;  /* Add 1 so the timer starts at 0:00 */
    }      
    
  return TRUE;    
}

int adjustTimer()
{    
  char secondStr[4];
  char timeStr[8];
  struct timeval time;
  struct timezone zone;
  int seconds;
  
  gettimeofday(&time, &zone);
  currSecs = time.tv_sec;

  if (playing && ! doneSong && ! paused)
    {           
      seconds = currSecs - startSecs;
      
      if (oldSeconds != currSecs)
        { 
          oldSeconds = currSecs;
        
          sprintf(secondStr, "%d", seconds % 60);
        
          if (strlen(secondStr) > 1)
            sprintf(timeStr, "%d:%s", seconds / 60, secondStr);
          else
            sprintf(timeStr, "%d:0%s", seconds / 60, secondStr);  
            
          if (activeWindow->time_label.eventBox != NULL)
            gtk_label_set(GTK_LABEL(activeWindow->time_label.widget->widget), timeStr);  
       }   
    }
  else if (paused)
         {
           seconds = currSecs - startSecs;  
           
           if (currSecs != oldSeconds)
             {               
               oldSeconds = currSecs;       /* This part adjusts for pausing */   
               startSecs++;                 /* time. */
             }
         }  
         
  return TRUE;         
}

void stopCallback()
{
  if (paused && activeWindow->pause_label.eventBox)
    gtk_widget_hide(activeWindow->pause_label.eventBox);
    
  stop();  
}

void playCallback()
{
  if (playing)
    return;
      
  if (shuffle)
    {
      clearPlayedFlags();
      nextSong(TRUE);
    }

  resetClock();
  play(NULL, &options);    
}

void togglePause()
{    
  pauseSong();  
  
  if (activeWindow->pause_label.eventBox == NULL)
    return;

  if (paused)
    gtk_widget_show(activeWindow->pause_label.widget->widget);
  else
    gtk_widget_hide(activeWindow->pause_label.widget->widget);
}

void toggleShuffle()
{
  if (shuffle)
    {
      shuffle = FALSE;
      
      if (activeWindow->shuffle_label.eventBox != NULL)
        gtk_widget_hide(activeWindow->shuffle_label.widget->widget);
    }  
  else
    {
      shuffle = TRUE;
      
      if (activeWindow->shuffle_label.eventBox != NULL)      
        gtk_widget_show(activeWindow->shuffle_label.widget->widget);
    }  
}

void toggleLoop()
{
  if (loopAll || loopOne)
    {
      loopOne = loopAll;
      loopAll = FALSE;
      
      if (activeWindow->repeat_label.eventBox == NULL)
        return;
             
      if (loopOne)
        gtk_label_set(GTK_LABEL(activeWindow->repeat_label.widget->widget), "Repeat");
      else 
        gtk_widget_hide(activeWindow->repeat_label.widget->widget);    
    }
  else
    {
      loopAll = TRUE;
      
      if (activeWindow->repeat_label.eventBox == NULL)
        return;
        
      gtk_label_set(GTK_LABEL(activeWindow->repeat_label.widget->widget), "Repeat All");
      gtk_widget_show(activeWindow->repeat_label.widget->widget);
    }  
}

void quitCallback()
{
  if ((playing) && (! doneSong))
    stop();
   
  gtk_main_quit();
}

void destroy_window(GtkWidget *widget, GtkWidget **window)
{  
  *window = NULL;
}

void volumeSliderCallback(GtkAdjustment *adjustment, gpointer *data)
{
  mixerSettings.globalVolume = adjustment->value;
  
  if (mixerExists)
    setVolume();
}

void popupMenu(GtkWidget *menu)
{
  gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 3, 0);
}

void toggleWindowSize()
{
  int x = activeWindow->position.x;
  int y = activeWindow->position.y;
      
  if (smallSize)
    {
       smallSize = FALSE;
       activeWindow = &bigWindow;
       gtk_widget_hide(smallWindow.main_window);
    }
  else
    {
       smallSize = TRUE;
       activeWindow = &smallWindow;       
       gtk_widget_hide(bigWindow.main_window);
    }
    
  gtk_widget_set_uposition(activeWindow->main_window, x, y);
  activeWindow->position.x = x;
  activeWindow->position.y = y;  
    
  if (activeWindow->shuffle_label.eventBox)
    {
      if (shuffle)
        gtk_widget_show(activeWindow->shuffle_label.widget->widget);
      else
        gtk_widget_hide(activeWindow->shuffle_label.widget->widget);
    }    
      
  if (activeWindow->repeat_label.eventBox)
    {     
      if (loopOne)
        {
          gtk_label_set(GTK_LABEL(activeWindow->repeat_label.widget->widget), 
                        "Repeat");
          gtk_widget_show(activeWindow->repeat_label.widget->widget);
        }
      else if (loopAll)
        {
          gtk_label_set(GTK_LABEL(activeWindow->repeat_label.widget->widget), 
                        "Repeat All");
          gtk_widget_show(activeWindow->repeat_label.widget->widget);        
        }
      else
        gtk_widget_hide(activeWindow->repeat_label.widget->widget);
    }

  if (activeWindow->song_label.eventBox != NULL)
    printSongName();
    
  gtk_widget_show(activeWindow->main_window);
}    

void widgetCommandCallback(GtkWidget *widget, gint command)
{
  switch (command)
    {
    case CMD_NO_COMMAND : { break; }
    case CMD_BACK : { back(NULL, &options); break; }
    case CMD_FORWARD : { forward(NULL, &options); break; }
    case CMD_PLAY : { playCallback(NULL, &options); break; }
    case CMD_STOP : { stopCallback(NULL, &options); break; }
    case CMD_PAUSE : { togglePause(NULL, &options); break; }
    case CMD_TOGGLE_SHUFFLE : { toggleShuffle(NULL, &options); break; }
    case CMD_TOGGLE_REPEAT : { toggleLoop(NULL, &options); break; }      
    case CMD_OPEN_PLAYLIST_EDITOR : { createListDialog(); break; }
    case CMD_OPEN_OPTIONS : { createOptionsDialog(); break; }
    case CMD_OPEN_ABOUT_BOX : { createAboutBox(); break; }
    case CMD_OPEN_MIXER : { openMixer(); break; }      
    case CMD_TOGGLE_SIZE : { toggleWindowSize(); break; }
    case CMD_QUIT : { quitCallback(); break; }
    default :
      {
        g_print(_("Warning, invalid signal %i.\n"), command);
        break;
      }        
    }
}

gint widgetLClickCommandCallback(GtkWidget *widget, GdkEventButton *event,
                                 gint command)
{
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1))
    return FALSE;

  switch (command)
    {
    case CMD_POPUP_MENU : 
      {
        gtk_menu_popup(GTK_MENU(activeWindow->main_menu),
                       NULL, NULL, NULL, NULL,
                       3, event->time);
        break;
      }  
    case CMD_NO_COMMAND :
      return FALSE;  
    default : { widgetCommandCallback(widget, command); break; }
    }
    
  return TRUE;
}                                 

gint widgetMClickCommandCallback(GtkWidget *widget, GdkEventButton *event,
                                 gint command)
{
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 2))
    return FALSE;

  switch (command)
    {
    case CMD_POPUP_MENU : 
      {
        gtk_menu_popup(GTK_MENU(activeWindow->main_menu),
                       NULL, NULL, NULL, NULL,                             
                       3, event->time);
        break;
      }  
    case CMD_NO_COMMAND :
      return FALSE;        
    default : { widgetCommandCallback(widget, command); break; }
    }    
    
  return TRUE;  
}

void filenames_dropped (GtkWidget *widget, GdkEventDropDataAvailable *event)
{
  int count = event->data_numbytes;
  char *p = event->data;
  int len,num;
   
  do {
    len = 1 + strlen (p);
    count -= len;
    printf("%s\n",p);
    p += len;
  } while (count > 0);
}

gint widgetRClickCommandCallback(GtkWidget *widget, GdkEventButton *event,
                                 gint command)
{
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 3))
    return FALSE;

  switch (command)
    {
    case CMD_POPUP_MENU : 
      {
        gtk_menu_popup(GTK_MENU(activeWindow->main_menu),
                       NULL, NULL, NULL, NULL,
                       3, event->time);
        break;
      }  
    case CMD_NO_COMMAND :
      return FALSE;
    default : { widgetCommandCallback(widget, command); break; }
    }    
    
  return TRUE;
}                                 

int main(int argc, char *argv[])
{
  char *accepted_drop_types[] = {"url:ALL"};
#ifdef HAVE_GNOME
  GnomeClient *client;

  argp_program_version = VERSION;
#endif 
  printf("%s\n",argv[0]);

  bindtextdomain ("gmp", GNOMELOCALEDIR);

  textdomain ("gmp");

  installSigHandler();
#ifdef HAVE_GNOME
  gnome_init ("gmp", NULL, argc, argv, 0, NULL);
  client = gnome_master_client ();
#else
  gtk_init(&argc, &argv);
  gtk_rc_init();
  gdk_imlib_init();
#endif
  root_win = gdk_window_foreign_new(GDK_ROOT_WINDOW());    

  initializeMixer();
  initializePlaylist();  
  initializeAlbumnList(&albumnList);

  loadRCFile();
  parseThemeRC(&bigWindow,
               &smallWindow);
  activeWindow = &bigWindow;
  
  gtk_widget_show(activeWindow->main_window);
  gtk_signal_connect (GTK_OBJECT (activeWindow->main_window), 
		      "drop_data_available_event",
		      GTK_SIGNAL_FUNC(filenames_dropped),
		      activeWindow->main_window);
  
  gtk_widget_dnd_drop_set (activeWindow->main_window, TRUE, 
			   accepted_drop_types, 1, FALSE);
    
  loadCommandlineFiles(argc, argv);

  gtk_timeout_add(1000, (GtkFunction) adjustTimer, NULL);
  adjustTimer();
  
  gtk_timeout_add(100, (GtkFunction) playLoop, NULL);
  gtk_main();  
  
  destroyPlaylist();
  destroyAlbumnList(albumnList);
  closeMixer();
  
  return 0;
}
