/*
 *	mp3tag.c  -- MP3 TAG Editor.
 *
 *	Copyright (C) 1998  Marcin Baranowski (minek@kis.p.lodz.pl).
 *      [ send comments and bug reports to minek@kis.p.lodz.pl]
 *
 *    This is very ALPHA code (may be unstable in some cases)
 * 
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	
 *  
 */


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "mp3tag.h"

  const Genre genre[116] = 
{
	{	"Acid", 			0x22	},
	{	"Acid Jazz",		0x4A	},
	{	"Acid Punk",		0x49	},
	{	"Acoustic",			0x63	},
	{	"Alternative",		0x14	},
	{	"Alt. Rock",		0x28	},
	{	"Ambient",			0x1A	},
	{	"Avantgarde",		0x5A	},
	{	"Bass",				0x29	},
	{	"Bebob",			0x55	},
	{	"Big Band",			0x60	},
	{	"Bluegrass",		0x59	},
	{	"Blues",			0x00	},
	{	"Booty Bass",		0x6B	},
	{	"Cabaret",			0x41	},
	{	"Celtic",			0x58	},
	{	"Chamber Music",	0x68	},
	{	"Chanson",			0x66	},
	{	"Chorus",			0x61	},
	{	"Christian Rap",	0x3D	},
	{	"Classical",		0x20	},
	{	"Classic Rock",		0x01	},
	{	"Club",				0x70	},
	{	"Comedy",			0x39	},
	{	"Country",			0x02	},
	{	"Cult",				0x3A	},
	{	"Darkwave",			0x32	},
	{	"Dance", 			0x03	},
	{	"Death Metal",		0x16	},
	{	"Disco",			0x04	},
	{	"Dream",			0x37	},
	{	"Easy Listening",	0x62	},
	{	"Electronic",		0x34	},
	{	"Ethnic",			0x30	},
	{	"Eurodance",		0x36	},
	{	"Euro-Techno",		0x19	},
	{	"Fast-Fusion",		0x54	},
	{	"Folk",				0x50	},
	{	"Folk/Rock",		0x51	},
	{	"Folklore",			0x73	},
	{	"Funk",				0x05	},
	{	"Fusion",			0x1E	},
	{	"Game",				0x24	},
	{	"Gangsta",			0x3B	},
	{	"Gothic",			0x31	},
	{	"Gothic Rock",		0x5B	},
	{	"Gospel",			0x26	},
	{	"Grunge",			0x06	},
	{	"Hard Rock",		0x4F	},
	{	"Hip-Hop",			0x07	},
	{	"House", 			0x23	},
	{	"Humour",			0x64	},
	{	"Industrial",		0x13	},
	{	"Instrumental",		0x21	},
	{	"Instrumental Pop",	0x2E	},
	{	"Instrumental Rock",0x2F	},
	{	"Jazz", 			0x08	},
	{	"Jazz+Funk",		0x1D	},
	{	"Jungle",			0x3F	},
	{	"Latin",			0x56	},
	{	"Lo-Fi",			0x47	},
	{	"Meditative",		0x2D	},
	{	"Metal",			0x09	},
	{	"Musical",			0x4D	},
	{	"National Folk",	0x52	},
	{	"Native American",	0x40	},
	{	"New Age",			0x0A	},
	{	"New Wave",			0x42	},
	{	"Noise",			0x27	},
	{	"Oldies",			0x0B	},
	{	"Opera",			0x67	},
	{	"Other",			0x0C	},
	{	"Polka",			0x4B	},
	{	"Pop",				0x0D	},
	{	"Pop/Funk",			0x3E	},
	{	"Pop-Folk",			0x35	},
	{	"Porn Groove",		0x6D	},
	{	"Primus",			0x6C	},
	{	"Progressive Rock",	0x5C	},
	{	"Punk",				0x2B	},
	{	"Pranks",			0x17	},
	{	"Psychadelic",		0x43	},
	{	"Psychedelic Rock",	0x5D	},
	{	"Rap",				0x0F	},
	{	"Rave",				0x44	},
	{	"Reggae",			0x10	},
	{	"Revival",			0x57	},
	{	"Retro",			0x4C	},
	{	"Rock",				0x11	},
	{	"Rock & Roll",		0x4E	},
	{	"R&B",				0x0E	},
	{	"Samba", 			0x72	},
	{	"Satire",			0x6E	},
	{	"Showtunes",		0x45	},
	{	"Ska",				0x15	},
	{	"Slow Jam",			0x6F	},
	{	"Slow Rock",		0x5F	},
	{	"Sonata",			0x69	},
	{	"Soul",				0x2A	},
	{	"Soundtrack",		0x18	},
	{	"Sound Clip",		0x25	},
	{	"Southern Rock",	0x38	},
	{	"Space",			0x2C	},
	{	"Speech",			0x65	},
	{	"Swing",			0x53	},
	{	"Symphony",			0x6A	},
	{	"Symphonic Rock",	0x5E	},
	{	"Tango",			0x71	},
	{	"Techno",			0x12	},
	{	"Techno-Industrial",0x33	},
	{	"Top 40",			0x3C	},
	{	"Trailer",			0x46	},
	{	"Tribal",			0x48	},
	{	"Trance",			0x1F	},
	{	"Trip-Hop",			0x1B	},
	{	"Vocal",			0x1C	}
};

void trimTrailingSpaces(char *string, int length)
{
  int i;
  
  for (i = length-1; (string[i] == ' ') && (i >= 0); i--);
  
  string[i+1] = '\0';  
}
 
char checktag(FILE *mp3file)
{
	char tag[3];
	fseek( mp3file, -128, SEEK_END );
	fscanf( mp3file, "%3s", tag );
	if ( strcmp( tag, TAG ) )
		return 0;
	return 1;
}

int readtag(FILE *mp3file, mp3Tag *mp3tag)
{
	if ( ! checktag(mp3file) ) 
	  return -1;
	
	fscanf( mp3file, "%30c", mp3tag->title );
	fscanf( mp3file, "%30c", mp3tag->artist );
	fscanf( mp3file, "%30c", mp3tag->albumn );
	fscanf( mp3file, "%4c", mp3tag->year );
	fscanf( mp3file, "%30c", mp3tag->comment );
	fscanf( mp3file, "%c", &mp3tag->genre );
	
	trimTrailingSpaces(mp3tag->title, 30);
	trimTrailingSpaces(mp3tag->artist, 30);
	trimTrailingSpaces(mp3tag->albumn, 30);
	trimTrailingSpaces(mp3tag->year, 4);
	trimTrailingSpaces(mp3tag->comment, 30);
	return(0);
}

void writetag_frag(FILE *mp3file, const char *tag, char count )
{
	char i; //, *buf;
	for ( i = 0; i < count; i++ )
		fputc( tag[i] ? tag[i] : 0x21, mp3file );
}

void savetag(FILE *mp3file, mp3Tag mp3tag)
{
	
	if ( checktag(mp3file) )
		fseek( mp3file, -128, SEEK_END );
	else 
		fseek( mp3file, 0, SEEK_END );

	writetag_frag(mp3file, TAG, 3 ); 
	writetag_frag(mp3file, mp3tag.title, 30 );
	writetag_frag(mp3file, mp3tag.artist, 30 );
	writetag_frag(mp3file, mp3tag.albumn, 30 );
	writetag_frag(mp3file, mp3tag.year, 4 );
	writetag_frag(mp3file, mp3tag.comment, 30 );
	writetag_frag(mp3file, &mp3tag.genre, 1 );
}