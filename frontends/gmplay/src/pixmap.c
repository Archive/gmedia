/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include <gtk/gtk.h>
#include <strings.h>
#include <gdk_imlib.h>
#include "gmplay.h"

GtkWidget *
load_image (char *filename,
	    GdkWindow *window,
	    GdkColor  *background,
	    gint *width, gint *height,
	    gint useThemePath)
{
  GtkWidget *wpixmap;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  GdkImlibImage *image;
  int w, h;
  char *fullFileName;
  
  if (useThemePath)
    {
      fullFileName = (char *)malloc(strlen(filename)+strlen(options.themePath)+2);
      strcpy(fullFileName, options.themePath);
      strcat(fullFileName, "/");
      strcat(fullFileName, filename);
    }
  else
    {
      fullFileName = (char *)malloc(strlen(filename)+strlen(PIXMAPDIR)+2);
      strcpy(fullFileName, PIXMAPDIR);
      strcat(fullFileName, "/");
      strcat(fullFileName, filename);
    }    

#ifdef __DEBUG__
  g_print("%s\n", fullFileName);
#endif
  
  image = gdk_imlib_load_image(fullFileName);
  w = image->rgb_width;
  h = image->rgb_height;

  gdk_imlib_render(image, w, h);    
  pixmap = gdk_imlib_copy_image(image);
  mask = gdk_imlib_copy_mask(image);

  wpixmap = gtk_pixmap_new (pixmap, mask);

  if (width)
    *width = w;
    
  if (height)
    *height = h;

  free(fullFileName);
  return wpixmap;
}

