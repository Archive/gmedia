/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pixmap.h"
#include "xpmbutton.h"
#include "shape.h"
#include "themerc.h"
#include "gmplay.h"
#include "mixer.h"

rcFile rcFileStack[256];
gint stackPos = -1;

void splitString(char *buffer, RcTokens *tokens)
{
  gint strPos, wordBufferPos;
  char wordBuffer[MAX_KEYWORD_LENGTH];

  wordBuffer[0] = '\0';
  wordBufferPos = 0;
  tokens->wordCount = 0;

  for (strPos = 0; 
       (buffer[strPos] != '\0') && (buffer[strPos] != '\n') &&
       (buffer[strPos] != '#');
       strPos++)
    {
      if ((buffer[strPos] == ' ') && (wordBufferPos > 0))
        {
          wordBuffer[wordBufferPos] = '\0';

          strcpy(tokens->words[tokens->wordCount], wordBuffer);
          tokens->wordCount++;
         
          wordBufferPos = 0;
          wordBuffer[0] = '\0';
        }
      else if (buffer[strPos] != ' ')        
        {
          wordBuffer[wordBufferPos] = buffer[strPos];
          wordBufferPos++;
        }
    }

  /* Take care of the last word. */

  wordBuffer[wordBufferPos] = '\0';
  strcpy(tokens->words[tokens->wordCount], wordBuffer);
  tokens->wordCount++;
  tokens->words[tokens->wordCount][0] = '\0';
}

void grabQuotedString(char *buffer, RcTokens *tokens)
{
  gint first=-1, last=-1;
  gint i;
  
  for (i = 0; i < tokens->wordCount; i++)
    {
      if ((tokens->words[i][0] == '\"') && (first < 0))
        first = i;
      if (tokens->words[i][strlen(tokens->words[i])-1] == '\"')
        {
          last = i;
          break;
        }
    }

  buffer[0] = '\0';
  if (first == last)
    {
      tokens->words[i][strlen(tokens->words[first])-1] = '\0';
      strcat(buffer, &(tokens->words[first][1]));
    }
  else
    for (i = first; i <= last; i++)
      {
        if (i == first)
          {
            strcat(buffer, &(tokens->words[i][1]));
            strcat(buffer, " ");
          }
        else if (i == last)
          {
            tokens->words[i][strlen(tokens->words[i])-1] = '\0';
            strcat(buffer, tokens->words[i]);
          }
        else
          {
            strcat(buffer, tokens->words[i]);
            strcat(buffer, " ");
          }
      }
}

void closeRcFile()
{
  if (stackPos < 0)
    return;

  if (rcFileStack[stackPos].stream)
    fclose(rcFileStack[stackPos].stream);
  
  if (rcFileStack[stackPos].fileName)
    free(rcFileStack[stackPos].fileName);

  stackPos--;
}

void closeAllRcFiles()
{
  while (stackPos >= 0)
    closeRcFile();
}

void storeFileName(char *fileName)
{
  char *fullName = (char *)malloc(1024);
  
  strcpy(fullName, options.themePath);
  strcat(fullName, "/");
        
  strcat(fullName, fileName);          

  rcFileStack[stackPos].fileName = fullName;
}

void openRcFile(char *fileName)
{
  stackPos++;
  storeFileName(fileName);
  
  rcFileStack[stackPos].stream = fopen(rcFileStack[stackPos].fileName, "r");

  if (!rcFileStack[stackPos].stream)
    {
      g_print("Error opening file: %s\n", rcFileStack[stackPos].fileName);
      perror("Error");
      closeAllRcFiles();
      gtk_main_quit();
      exit(1);
    }
  else
    rcFileStack[stackPos].linePos = 0;
}

void rcFileError(char *errorStr)
{
  g_print("Error, %s in %s:%i\n", 
          errorStr,
          rcFileStack[stackPos].fileName,
          rcFileStack[stackPos].linePos);

  closeAllRcFiles();
  gtk_main_quit();
  exit(1);
}

void includeRcFile(char *lineBuffer)
{
  char token[30], fileName[80];

  sscanf(lineBuffer, "%30s %80s", token, fileName);

#ifdef __DEBUG__
  g_print("Including %s from %s:%i\n", 
          fileName,
          rcFileStack[stackPos].fileName,
          rcFileStack[stackPos].linePos);
#endif          
          
  openRcFile(fileName);
}

void readRcFileLine(RcTokens *tokens, gint inBlock)
{
  char lineBuffer[1024];
  int startPos = 0;

  lineBuffer[0] = '\0';
  while ((stackPos >= 0) && 
         ((lineBuffer[startPos] == '\0') ||
          (lineBuffer[startPos] == '#') ||
          (lineBuffer[startPos] == '%') ||
          (lineBuffer[startPos] == '\n')))
    {
      if (feof(rcFileStack[stackPos].stream))
        {
          closeRcFile();
          continue;
        }

      fgets(lineBuffer, 1024, rcFileStack[stackPos].stream);      
      rcFileStack[stackPos].linePos++;
      for (startPos = 0; lineBuffer[startPos] == ' '; startPos++);

      if (lineBuffer[startPos] == '%')
        includeRcFile(lineBuffer);
    }  

  if ((stackPos < 0) && inBlock)
    rcFileError("premature end of rcFile");

  splitString(lineBuffer, tokens);
}

void initializeThemeWidget(ThemeWidget *widget)
{
  widget->widget = NULL;
  widget->location.x = 0;
  widget->location.y = 0;
  widget->location.location = NO_LOCATION;
  widget->command = CMD_NO_COMMAND;
}

void initializeWindow(ThemeWindow *window)
{
  window->main_window = NULL;
  window->main_menu = NULL;

  window->song_label.widget = malloc(sizeof(ThemeWidget));
  window->repeat_label.widget = malloc(sizeof(ThemeWidget));
  window->pause_label.widget = malloc(sizeof(ThemeWidget));
  window->shuffle_label.widget = malloc(sizeof(ThemeWidget));
  window->time_label.widget = malloc(sizeof(ThemeWidget));

  window->song_label.width = 0;
  window->song_label.height = 0;

  window->repeat_label.width = 0;
  window->repeat_label.height = 0;

  window->pause_label.width = 0;
  window->pause_label.height = 0;

  window->shuffle_label.width = 0;
  window->shuffle_label.height = 0;

  window->time_label.width = 0;
  window->time_label.height = 0;

  initializeThemeWidget(window->song_label.widget);
  initializeThemeWidget(window->repeat_label.widget);
  initializeThemeWidget(window->pause_label.widget);
  initializeThemeWidget(window->shuffle_label.widget);
  initializeThemeWidget(window->time_label.widget);
  initializeThemeWidget(&(window->volume_slider));

  window->song_label.eventBox = NULL;
  window->repeat_label.eventBox = NULL;
  window->pause_label.eventBox = NULL;
  window->shuffle_label.eventBox = NULL;
  window->time_label.eventBox = NULL;

  window->buttons = NULL;
  window->menu_items = NULL;
  window->Tooltips = NULL;
  
  window->middle_click_action = CMD_NO_COMMAND;
  window->right_click_action = CMD_NO_COMMAND;

  if (options.managed)
    window->main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  else
    window->main_window = gtk_window_new(GTK_WINDOW_POPUP);
  
  gtk_window_set_policy(GTK_WINDOW(window->main_window),
                        FALSE, FALSE, FALSE);
  gtk_window_set_wmclass(GTK_WINDOW(window->main_window), "gmplay.main_window", NULL);
  gtk_widget_realize(window->main_window);

  if (options.toolTips)
    window->Tooltips = gtk_tooltips_new();
}

gint parseAction(char *actionStr)
{
  if (strcasecmp(actionStr, "cmd_no_command") == 0)
    return CMD_NO_COMMAND;
  if (strcasecmp(actionStr, "cmd_back") == 0)
    return CMD_BACK;
  if (strcasecmp(actionStr, "cmd_foward") == 0)
    return CMD_FORWARD;
  if (strcasecmp(actionStr, "cmd_play") == 0)
    return CMD_PLAY;
  if (strcasecmp(actionStr, "cmd_stop") == 0)
    return CMD_STOP;
  if (strcasecmp(actionStr, "cmd_pause") == 0)
    return CMD_PAUSE;
  if (strcasecmp(actionStr, "cmd_toggle_size") == 0)
    return CMD_TOGGLE_SIZE;
  if (strcasecmp(actionStr, "cmd_toggle_shuffle") == 0)
    return CMD_TOGGLE_SHUFFLE;
  if (strcasecmp(actionStr, "cmd_toggle_repeat") == 0)
    return CMD_TOGGLE_REPEAT;
  if (strcasecmp(actionStr, "cmd_open_playlist_editor") == 0)
    return CMD_OPEN_PLAYLIST_EDITOR;
  if (strcasecmp(actionStr, "cmd_open_options") == 0)
    return CMD_OPEN_OPTIONS;
  if (strcasecmp(actionStr, "cmd_open_about_box") == 0)
    return CMD_OPEN_ABOUT_BOX;
  if (strcasecmp(actionStr, "cmd_open_mixer") == 0)
    return CMD_OPEN_MIXER;
  if (strcasecmp(actionStr, "cmd_popup_menu") == 0)
    return CMD_POPUP_MENU;
  if (strcasecmp(actionStr, "cmd_quit") == 0)
    return CMD_QUIT;

  rcFileError("invalid command string");
  return -1;
}

void parsePos(WidgetLoc *location,
              char *offsetStr,
              char *xStr,
              char *yStr)
{
  if (strcasecmp(offsetStr, "topleft") == 0)
    location->location = TOP_LEFT;
  else if (strcasecmp(offsetStr, "topright") == 0)
    location->location = TOP_RIGHT;
  else if (strcasecmp(offsetStr, "bottomleft") == 0)
    location->location = BOTTOM_LEFT;
  else if (strcasecmp(offsetStr, "bottomright") == 0)
    location->location = BOTTOM_RIGHT;
  else if (strcasecmp(offsetStr, "inmenu") == 0)
    location->location = IN_MENU;
  else
    rcFileError("invalid offset position");

  sscanf(xStr, "%i", &(location->x));
  sscanf(yStr, "%i", &(location->y));
}

void convertPos(ThemeWindow *window,
                ThemeWidget *widget,
                gint *x,
                gint *y)
{
  switch (widget->location.location)
    {
    case TOP_LEFT:
      {
        *x = widget->location.x;
        *y = widget->location.y;
        break;
      }
    case TOP_RIGHT:
      {
        *x = window->width - widget->location.x;
        *y = widget->location.y;
        break;
      }
    case BOTTOM_LEFT:
      {
        *x = widget->location.x;
        *y = window->height - widget->location.y;
        break;
      }
    case BOTTOM_RIGHT:
      {
        *x = window->width - widget->location.x;
        *y = window->height - widget->location.y;
        break;
      }
    }
}

void parseLabel(ThemeLabel *label)
{
  RcTokens tokens;
  char *font = (char *)malloc(1024);
  GdkFont *gfont;
  gint r=0, g=0, b=0;
  GtkStyle *style;

  tokens.words[0][0] = '\0';
  font[0] = '\0';
  style = gtk_style_new();
  r = style->fg[GTK_STATE_NORMAL].red = r;
  g = style->fg[GTK_STATE_NORMAL].green = g;
  b = style->fg[GTK_STATE_NORMAL].blue = b;
  
  label->widget->command = CMD_NO_COMMAND;

  while (strcasecmp(tokens.words[0], "end") != 0)
    {
      readRcFileLine(&tokens, TRUE);

      if (strcasecmp(tokens.words[0], "end") == 0)
        continue;
      else if (strcasecmp(tokens.words[0], "font") == 0)
        {
          strcpy(font, tokens.words[1]);
          gfont = gdk_font_load(font);
          if (gfont != NULL)
            style->font = gfont;
          else
            rcFileError("unable to load font");     
        }  
      else if (strcasecmp(tokens.words[0], "color") == 0)
        {
          sscanf(tokens.words[1], "%i", &r);
          sscanf(tokens.words[2], "%i", &g);
          sscanf(tokens.words[3], "%i", &b);
        }
      else if (strcasecmp(tokens.words[0], "size") == 0)
        {
          sscanf(tokens.words[1], "%i", &(label->width));
          sscanf(tokens.words[2], "%i", &(label->height));
        }
      else if (strcasecmp(tokens.words[0], "location") == 0)
        parsePos(&(label->widget->location),
                 tokens.words[1],
                 tokens.words[2],
                 tokens.words[3]);
      else if (strcasecmp(tokens.words[0], "action") == 0)
        label->widget->command = parseAction(tokens.words[1]);      
      else
        rcFileError("invalid identifier");
    }

  label->eventBox = gtk_event_box_new();
  gtk_widget_show(label->eventBox);
  label->widget->widget = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(label->widget->widget), 0.0, 0.5);
  
  style->fg[GTK_STATE_NORMAL].red = r;
  style->fg[GTK_STATE_NORMAL].green = g;
  style->fg[GTK_STATE_NORMAL].blue = b;

  gtk_widget_set_style(label->widget->widget, style);  
  gtk_container_add(GTK_CONTAINER(label->eventBox),
                    label->widget->widget);
  gtk_widget_show(label->widget->widget);                    
  
  free(font);
}

void parseMenuItem(ThemeWindow *window,
                   RcTokens *tokens)
{
  ThemeWidget *menuItem = malloc(sizeof(ThemeWidget));
  char *menuStr = (char *)malloc(1024);

  grabQuotedString(menuStr, tokens);
  menuItem->widget = gtk_menu_item_new_with_label(menuStr);
  gtk_widget_show(menuItem->widget);
  menuItem->command = parseAction(tokens->words[tokens->wordCount-1]);
  menuItem->location.location = IN_MENU;
  menuItem->location.x = 0;
  menuItem->location.y = 0;  

  free(menuStr);

  if (window->menu_items == NULL)
    window->menu_items = g_list_alloc();

  g_list_append(window->menu_items, menuItem);
}

void parseMenu(ThemeWindow *window)
{
  RcTokens tokens;

  tokens.words[0][0] = '\0';

  while(strcasecmp(tokens.words[0], "end") != 0)
    {
      readRcFileLine(&tokens, TRUE);

      if (strcasecmp(tokens.words[0], "end") == 0)
        continue;
      else if (strcasecmp(tokens.words[0], "item") == 0)
        parseMenuItem(window, &tokens);
      else
        rcFileError("invalid identifier");
    }
}

void parseVolumeSlider(ThemeWindow *window)
{
  GtkObject *adjustment;
  gint horizontal = TRUE;
  gint width=0, height=0;
  RcTokens tokens;

  tokens.words[0][0] = '\0';

  while(strcasecmp(tokens.words[0], "end") != 0)
    {
      readRcFileLine(&tokens, TRUE);

      if (strcasecmp(tokens.words[0], "end") == 0)
        continue;
      else if (strcasecmp(tokens.words[0], "orientation") == 0)
        {
          if (strcasecmp(tokens.words[1], "vertical") == 0)
            horizontal = FALSE;
          else if (strcasecmp(tokens.words[1], "horizontal") != 0)
            rcFileError("invalid orientation identifier");
        }
      else if (strcasecmp(tokens.words[0], "size") == 0)
        {
          sscanf(tokens.words[1], "%i", &width);
          sscanf(tokens.words[2], "%i", &height);
        }
      else if (strcasecmp(tokens.words[0], "location") == 0)
        parsePos(&(window->volume_slider.location),
                 tokens.words[1],
                 tokens.words[2],
                 tokens.words[3]);     
    }
  
  adjustment = gtk_adjustment_new(mixerSettings.globalVolume, 0.0, 101.0,
                                  1.0, 5.0, 1.0);
  gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                     GTK_SIGNAL_FUNC(volumeSliderCallback), NULL);
  if (horizontal)
    window->volume_slider.widget = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
  else
    window->volume_slider.widget = gtk_vscale_new(GTK_ADJUSTMENT(adjustment));

  gtk_widget_set_name(window->volume_slider.widget, "volume slider");
  gtk_scale_set_draw_value(GTK_SCALE(window->volume_slider.widget), FALSE);
  gtk_widget_set_usize(window->volume_slider.widget, width, height);
  gtk_widget_show(window->volume_slider.widget);
}

void parseButton(ThemeWindow *window)
{
  RcTokens tokens;
  ThemeWidget *button = (ThemeWidget *)malloc(sizeof (ThemeWidget));
  char *defStr = (char *)malloc(1024);
  char *focusedStr = (char *)malloc(1024);
  char *pressedStr = (char *)malloc(1024);
  char *tooltip = (char *)malloc(1024);

  tokens.words[0][0] = '\0';

  defStr[0] = '\0';
  focusedStr[0] = '\0';
  pressedStr[0] = '\0';
  tooltip[0] = '\0';

  while(strcasecmp(tokens.words[0], "end") != 0)
    {
      readRcFileLine(&tokens, TRUE);

      if (strcasecmp(tokens.words[0], "end") == 0)
        continue;
      else if (strcasecmp(tokens.words[0], "tooltip") == 0)
        grabQuotedString(tooltip, &tokens);
      else if (strcasecmp(tokens.words[0], "default") == 0)
        strcpy(defStr, tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "focused") == 0)
        strcpy(focusedStr, tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "pressed") == 0)
        strcpy(pressedStr, tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "location") == 0)
        parsePos(&(button->location),
                 tokens.words[1],
                 tokens.words[2],
                 tokens.words[3]);
      else if (strcasecmp(tokens.words[0], "action") == 0)
        button->command = parseAction(tokens.words[1]);
      else
        rcFileError("invalid identifier");
    }

  button->widget = xpm_button_new(defStr,
                                  focusedStr,
                                  pressedStr);

  if ((window->Tooltips) && (tooltip[0] != '\0'))
    gtk_tooltips_set_tip(window->Tooltips, button->widget, tooltip, NULL);
        
  if (!window->buttons)
    window->buttons = g_list_alloc();

  g_list_append(window->buttons, button);
  
  free(defStr);
  free(focusedStr);
  free(pressedStr);
  free(tooltip);
}

void generateLabel(ThemeWindow *window, ThemeLabel *label)
{
  GtkWidget *pixmapWid;
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;
  gint x, y;

  if (label->eventBox == NULL)
    return;

  convertPos(window, label->widget, &x, &y);
  gtk_fixed_put(GTK_FIXED(window->fixed), label->eventBox, x, y);

  if ((label->width != 0) && (label->height != 0))
    gtk_widget_set_usize(label->eventBox, label->width, label->height);

  gtk_widget_show(label->eventBox);

/* This reeks of hack... :) */
  
  if (label->widget->command == CMD_NO_COMMAND)
    {
      gtk_object_set_user_data(GTK_OBJECT(label->eventBox), window->clickPos);

      gtk_signal_connect(GTK_OBJECT(label->eventBox), "button_press_event",
                         GTK_SIGNAL_FUNC(shape_pressed), window);
      gtk_signal_connect(GTK_OBJECT(label->eventBox), "button_release_event",
                         GTK_SIGNAL_FUNC(shape_released), window);
      gtk_signal_connect(GTK_OBJECT(label->eventBox), "motion_notify_event",
                         GTK_SIGNAL_FUNC(shape_motion), window);             
    }                         
  else  
    gtk_signal_connect(GTK_OBJECT(label->eventBox), "button_press_event",
                       GTK_SIGNAL_FUNC(widgetLClickCommandCallback),
                       (int *)(label->widget->command));
                       
  gtk_signal_connect(GTK_OBJECT(label->eventBox), "button_press_event",
                     GTK_SIGNAL_FUNC(widgetMClickCommandCallback),
                     (int *)(window->middle_click_action));
  gtk_signal_connect(GTK_OBJECT(label->eventBox), "button_press_event",
                     GTK_SIGNAL_FUNC(widgetRClickCommandCallback),
                     (int *)(window->right_click_action));

  if (window->background != NULL)
    {
      pixmapWid = load_image(window->background,
                             window->fixed->window,
                             &window->fixed->style->bg[GTK_STATE_NORMAL],
                             NULL, NULL, TRUE);      
      
      gtk_widget_realize(label->eventBox);
      gtk_pixmap_get(GTK_PIXMAP(pixmapWid), &pixmap, &bitmap);
      gdk_window_set_back_pixmap(label->eventBox->window,
                                 pixmap,
                                 TRUE);
    }
}

void generateLabels(ThemeWindow *window)
{ 
  if (window->song_label.eventBox != NULL)    
    generateLabel(window, &(window->song_label));
    
  if (window->repeat_label.eventBox != NULL)
    {
      generateLabel(window, &(window->repeat_label));

      gtk_label_set(GTK_LABEL(window->repeat_label.widget->widget), "");
      gtk_widget_hide(window->repeat_label.widget->widget);
    }
    
  if (window->pause_label.eventBox != NULL)
    {
      generateLabel(window, &(window->pause_label));
  
      gtk_label_set(GTK_LABEL(window->pause_label.widget->widget), "Paused");
      gtk_widget_hide(window->pause_label.widget->widget);
    }
    
  if (window->shuffle_label.eventBox != NULL)
    {
      generateLabel(window, &(window->shuffle_label));
  
      gtk_label_set(GTK_LABEL(window->shuffle_label.widget->widget), "Shuffle");
      gtk_widget_hide(window->shuffle_label.widget->widget);
    }  
    
  if (window->time_label.eventBox != NULL)
    {
      generateLabel(window, &(window->time_label));
        
      gtk_label_set(GTK_LABEL(window->time_label.widget->widget), "0:00");
    }  
}

void generateMenu(ThemeWindow *window)
{
  ThemeWidget *menuItem;  
  GList *menuItems = g_list_first(window->menu_items)->next;

  window->main_menu = gtk_menu_new();
  gtk_widget_realize(window->main_menu);
  gdk_window_set_cursor(window->main_menu->window, 
                        gdk_cursor_new(GDK_LEFT_PTR));

  do
    {
      menuItem = menuItems->data;
      gtk_menu_append(GTK_MENU(window->main_menu),
                      menuItem->widget);
      gtk_signal_connect(GTK_OBJECT(menuItem->widget),
                         "activate",
                         GTK_SIGNAL_FUNC(widgetCommandCallback),
                         (int *)(menuItem->command));
      
      menuItems = g_list_next(menuItems);
    }
  while (menuItems != NULL);
}

void generateButtons(ThemeWindow *window)
{
  gint x, y;
  GList *buttons = g_list_first(window->buttons)->next;
  ThemeWidget *button;

  do
    {
      button = buttons->data;
      convertPos(window, button, &x, &y);
      gtk_fixed_put(GTK_FIXED(window->fixed), button->widget, x, y);
      gtk_widget_show(button->widget);

      if (button->command != CMD_POPUP_MENU)
        gtk_signal_connect(GTK_OBJECT(button->widget), "clicked",
                           GTK_SIGNAL_FUNC(widgetCommandCallback),
                           (int *)(button->command));
      else
        gtk_signal_connect(GTK_OBJECT(button->widget), "button_release_event",
                           GTK_SIGNAL_FUNC(widgetLClickCommandCallback),
                           (int *)(button->command));                           

      buttons = g_list_next(buttons);
    }
  while (buttons != NULL);
}

void parseWindow(ThemeWindow *window)
{
  GtkWidget *pixmapWid;
  GtkWidget *eventBox;
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;    
  char *titlePixmap = (char *)malloc(1024);
  char *tooltip = (char *)malloc(1024);
  char *mask = NULL;

  RcTokens tokens;
  gint height = 0, width = 0;
  gint x = 0, y = 0;
  gint dragbar_x=0, dragbar_y=0, 
       dragbar_width=0, dragbar_height=0;

  initializeWindow(window);
  tokens.words[0][0] = '\0';  
  titlePixmap[0] = '\0';
  tooltip[0] = '\0';

  while (strcasecmp(tokens.words[0], "end") != 0)
    {
      readRcFileLine(&tokens, TRUE);
      
      if (strcasecmp(tokens.words[0], "size") == 0)
        {
          sscanf(tokens.words[1], "%i", &width);
          sscanf(tokens.words[2], "%i", &height);          

          window->width = width;
          window->height = height;          
        }
      else if (strcasecmp(tokens.words[0], "tooltip") == 0)
        grabQuotedString(tooltip, &tokens);          
      else if (strcasecmp(tokens.words[0], "dragbar") == 0)
        {
          sscanf(tokens.words[1], "%i", &dragbar_x);
          sscanf(tokens.words[2], "%i", &dragbar_y);
          sscanf(tokens.words[3], "%i", &dragbar_width);
          sscanf(tokens.words[4], "%i", &dragbar_height);
        }  
      else if (strcasecmp(tokens.words[0], "titlebarimage") == 0)          
        strcpy(titlePixmap, tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "background") == 0)
        {
          window->background = (char *)malloc(strlen(tokens.words[1])+1);
          strcpy(window->background, tokens.words[1]);
        }
      else if (strcasecmp(tokens.words[0], "mask") == 0)
        {
          mask = (char *)malloc(strlen(tokens.words[1])+1);
          strcpy(mask, tokens.words[1]);
        }  
      else if (strcasecmp(tokens.words[0], "location") == 0)
        {
          sscanf(tokens.words[1], "%i", &x);
          sscanf(tokens.words[2], "%i", &y);
        }
      else if (strcasecmp(tokens.words[0], "leftclick") == 0)
        window->left_click_action = parseAction(tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "rightclick") == 0)
        window->right_click_action = parseAction(tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "middleclick") == 0)
        window->middle_click_action = parseAction(tokens.words[1]);
      else if (strcasecmp(tokens.words[0], "begin") == 0)
        {
          if (strcasecmp(tokens.words[1], "song_label") == 0)
            parseLabel(&(window->song_label));
          else if (strcasecmp(tokens.words[1], "pause_label") == 0)
            parseLabel(&(window->pause_label));
          else if (strcasecmp(tokens.words[1], "shuffle_label") == 0)
            parseLabel(&(window->shuffle_label));
          else if (strcasecmp(tokens.words[1], "time_label") == 0)
            parseLabel(&(window->time_label));
          else if (strcasecmp(tokens.words[1], "repeat_label") == 0)
            parseLabel(&(window->repeat_label));
          else if (strcasecmp(tokens.words[1], "menu") == 0)
            parseMenu(window);
          else if (strcasecmp(tokens.words[1], "volume_slider") == 0)
            parseVolumeSlider(window);
          else if (strcasecmp(tokens.words[1], "button") == 0)
            parseButton(window);
          else
            rcFileError("invalid identifier");
        }
      else if (strcasecmp(tokens.words[0], "end") == 0)
        continue;
      else
        rcFileError("invalid window flag");
    }
  
  if (mask)
    {
      pixmapWid = load_image(mask,
                             window->main_window->window,
                             &(window->main_window->style->bg[GTK_STATE_NORMAL]),
                             NULL, NULL, TRUE);

      gtk_pixmap_get(GTK_PIXMAP(pixmapWid), &pixmap, &bitmap);                          
      gdk_window_shape_combine_mask(window->main_window->window,
                                    bitmap, 0, 0);
    }
                     
  window->fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(window->main_window), window->fixed);
  gtk_widget_show(window->fixed);  
  gtk_widget_realize(window->fixed);
                     
  if (window->background)
    {
      pixmapWid = load_image(window->background,
                             window->fixed->window,
                             &(window->fixed->style->bg[GTK_STATE_NORMAL]),
                             NULL, NULL, TRUE);

      gtk_pixmap_get(GTK_PIXMAP(pixmapWid), &pixmap, &bitmap);
      gdk_window_set_back_pixmap(window->fixed->window, pixmap, FALSE);
    }
    
  eventBox = gtk_event_box_new();   
  gtk_widget_set_usize(eventBox, dragbar_width, dragbar_height);
  gtk_fixed_put(GTK_FIXED(window->fixed), eventBox, dragbar_x, dragbar_y);
  gtk_widget_show(eventBox);
      
  gdk_window_set_back_pixmap(eventBox->window, pixmap, TRUE);
                     
  window->clickPos = g_new(WidgetLoc, 1);
  gtk_object_set_user_data(GTK_OBJECT(eventBox), window->clickPos);

  gtk_signal_connect(GTK_OBJECT(eventBox), "button_press_event",
                     GTK_SIGNAL_FUNC(shape_pressed), window);
  gtk_signal_connect(GTK_OBJECT(eventBox), "button_release_event",
                     GTK_SIGNAL_FUNC(shape_released), window);
  gtk_signal_connect(GTK_OBJECT(eventBox), "motion_notify_event",
                     GTK_SIGNAL_FUNC(shape_motion), window);             

  if (! options.managed)
    gdk_window_set_cursor(window->main_window->window, gdk_cursor_new(GDK_LEFT_PTR));
                                 
  gtk_widget_set_usize(window->fixed, width, height);
  gtk_widget_set_uposition(window->main_window, x, y);

  if (window->buttons) generateButtons(window);
  generateLabels(window);  	
  if (window->menu_items) generateMenu(window);

  if (window->volume_slider.widget)
    {
      convertPos(window, &(window->volume_slider), &x, &y);
      gtk_fixed_put(GTK_FIXED(window->fixed), window->volume_slider.widget, x, y);
    }
    
  gtk_signal_connect(GTK_OBJECT(window->fixed), "button_press_event",
                     GTK_SIGNAL_FUNC(widgetMClickCommandCallback),
                     (int *)(window->middle_click_action));
  gtk_signal_connect(GTK_OBJECT(window->fixed), "button_press_event",
                     GTK_SIGNAL_FUNC(widgetRClickCommandCallback),
                     (int *)(window->right_click_action));
  gtk_signal_connect(GTK_OBJECT(window->main_window), "delete_event",
                     GTK_SIGNAL_FUNC(quitCallback), NULL);                                          
    
  if ((window->Tooltips) && (tooltip[0] != '\0'))
    gtk_tooltips_set_tip(window->Tooltips, window->main_window, tooltip, NULL);    
}

gint parseThemeRC(ThemeWindow *bigWindow,
                  ThemeWindow *smallWindow)
{
  RcTokens tokens;
  char *gtkRcName = (char *)malloc(strlen(options.themePath)+
                                   strlen("widgets.rc")+2);
  strcpy(gtkRcName, options.themePath);
  strcat(gtkRcName, "/");
  strcat(gtkRcName, "widgets.rc");
  gtk_rc_parse(gtkRcName);  
  
  openRcFile("theme.rc");

  while (stackPos >= 0)
    {
      readRcFileLine(&tokens, FALSE);

      if (stackPos < 0)
        continue;
      else if (strcasecmp(tokens.words[0], "begin") != 0)
        rcFileError("invalid keyword");
      else if (strcasecmp(tokens.words[1], "big_window") == 0)
        parseWindow(bigWindow);
      else if (strcasecmp(tokens.words[1], "small_window") == 0)
        parseWindow(smallWindow);
      else
        rcFileError("invalid window identifier");
    }
    
  return 0;
}
