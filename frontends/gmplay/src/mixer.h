/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef __MIXER__
#define __MIXER__

typedef struct {
  int globalVolume;
  int lineVolume;
  int bass;
  int treble;
  int PCM;
  int oGain;
} mixer_vals;
  

extern int mixerExists;
extern int mixerOpen;
extern int mixer_fd;
extern float balance;

extern mixer_vals mixerSettings;

int getVolume();
int setVolume();
int getLineVolume();
int setLineVolume();
int getBass();
int setBass();
int getTreble();
int setTreble();
int getPCM();
int setPCM();
int getOGain();
int setOGain();

int initializeMixer();
void closeMixer();

#endif