/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GMPLAY__
#define __GMPLAY__
 
#include <unistd.h>
#include <sys/types.h>
#include <gtk/gtk.h>

#ifndef RCFILENAME
  #define RCFILENAME ".gmplayrc"
#endif

#define CMDLINESIZE 8

#define PIXMAPDIR GNOMEDATADIR "/pixmaps/gmplay"
#define GLOBALTHEMEDIR GNOMEDATADIR "/gmplay"

typedef struct _cursoroffset { gint x, y; } CursorOffset;

#define LEFT 0
#define RIGHT 1
#define STEREO 2
#define MONO 3

typedef struct {
  int bufferSize;
  int downSample;
  int lineOut;
  int output;
  int managed;
  int toolTips;
  int autoparse;
  int autoloadTags;
  char *playListName;
  char *mixerApp;
  char *themePack;
  char *themePath;
} configType;    

extern GdkWindow *root_win;

extern int doneSong;
extern int playing;
extern int paused;
extern int loopOne;
extern int loopAll;
extern int shuffle;
extern pid_t mpgPID;
extern GtkWidget *mainMenu;
extern long int currSecs;
extern long int startSecs;
extern configType options;

void destroy_window(GtkWidget *widget, GtkWidget **window);
void saveRCFile();

void quitCallback();
void widgetCommandCallback(GtkWidget *widget, gint command);
gint widgetLClickCommandCallback(GtkWidget *widget, GdkEventButton *event, gint command);
gint widgetMClickCommandCallback(GtkWidget *widget, GdkEventButton *event, gint command);
gint widgetRClickCommandCallback(GtkWidget *widget, GdkEventButton *event, gint command);
void printSongName();
void resetClock();
void volumeSliderCallback(GtkAdjustment *adjustment, gpointer *data);

#endif
