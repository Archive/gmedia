/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include "mp3tag.h"
#include "fileseldefs.h"
#include "list.h"
#include "handlers.h"
#include "listutils.h"

const char *albumnlist_id_string = "*gmplay_albumnlist*";
const char *playlist_id_string = "*gmplay_playlist_2*";
const char *tree_item_id_string = "tree_item";
const char *clist_item_id_string = "list_item";

albumnNode *albumnList;

GList *activePlayList;
GList *currentSong;

GtkWidget *tree;
GtkWidget *playlist;
GtkWidget *songlist;

GtkWidget *treeMenu;
GtkWidget *songMenu;
GtkWidget *playlistMenu;

/************************
  Basic Utility Routines
 ************************/

void chop(char *string)
{
  if (strlen(string) > 0)
    string[strlen(string)-1] = '\0';
}

void openDialog(GtkWidget *widget, GtkWidget *dialog)
{
  if (! GTK_WIDGET_VISIBLE(dialog))
    gtk_widget_show(dialog);
}

void closeDialog(GtkWidget *widget, GtkWidget *dialog)
{
  if (GTK_WIDGET_VISIBLE(dialog))
    gtk_widget_hide(dialog);
}

char *completeFileName(GtkFileSelection *fileSel, char *fileName)
{
  CompletionState *cmpl_state;
  char *fullFileName;
  
  cmpl_state = fileSel->cmpl_state;
  
  fullFileName = (char *)malloc(strlen(fileName)+
                                strlen(cmpl_state->reference_dir->fullname)+2);
  
  strcpy(fullFileName, cmpl_state->reference_dir->fullname);;
  strcat(fullFileName, "/");
  strcat(fullFileName, fileName);
  
  return fullFileName;
}

GtkWidget *createFileDialog(char *title, GtkSignalFunc addFunc,
                                         GtkSignalFunc addDirFunc)
{
  GtkWidget *fileSelWid;      /* Having both of these reduces need for casting */
  GtkFileSelection *fileSel;
  
  GtkWidget *button, *dirButton;
  GtkWidget *hbox;  
  
  fileSel = GTK_FILE_SELECTION(gtk_file_selection_new(title));
  fileSelWid = GTK_WIDGET(fileSel);
  gtk_file_selection_hide_fileop_buttons(fileSel);
  
  gtk_widget_destroy(fileSel->ok_button);
  gtk_widget_destroy(fileSel->cancel_button);
  
  gtk_clist_set_selection_mode(GTK_CLIST(fileSel->file_list),
                               GTK_SELECTION_MULTIPLE);
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_widget_show(hbox);
  gtk_box_pack_end(GTK_BOX(fileSel->main_vbox), hbox, TRUE, TRUE, 0);
  
  button = gtk_button_new_with_label("Add");
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  fileSel->ok_button = button;
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);  
  gtk_widget_show(button);    
  
  dirButton = gtk_button_new_with_label("Add Dir");
  gtk_box_pack_start(GTK_BOX(hbox), dirButton, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(dirButton, GTK_CAN_DEFAULT);
  gtk_widget_show(dirButton);
  
  button = gtk_button_new_with_label("Close");
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  fileSel->cancel_button = button;
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_show(button);  
  
  gtk_signal_connect(GTK_OBJECT(fileSel), "destroy",
                     GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
  gtk_signal_connect(GTK_OBJECT(fileSel), "delete_event",
                     GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
                     
  gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                     addFunc, fileSel);
  gtk_signal_connect(GTK_OBJECT(dirButton), "clicked",
                     addDirFunc, fileSel);
  gtk_signal_connect(GTK_OBJECT(fileSel->cancel_button), "clicked",
                     GTK_SIGNAL_FUNC(closeDialog), fileSelWid);
                     
  return fileSelWid;
}                                         

/*****************
  Popup Menu Code 
 *****************/

static gint popupMenu(GtkWidget *widget, GdkEventButton *event, GtkWidget **menu)
{
  if (event->button == 3)
    {
      gtk_menu_popup(GTK_MENU(*menu), NULL, NULL, NULL, NULL,
                     3, event->time);
      return TRUE;
    }

  return FALSE;
}

/***********************************
  Albumn List Startup/Shutdown Code
 ***********************************/

void initializeAlbumnList(albumnNode **albumn)
{
  *albumn = newAlbumn("MP3", NULL);
}

void destroyAlbumnList(albumnNode *albumn)
{
  GList *currentAlbumn;
  GList *currentSong;
  songNode *song;
  
  currentSong = g_list_first(albumn->songs)->next;
  while (currentSong)
    {
      song = (songNode *)(currentSong->data);
      free(song->title);
      free(song->fileName);
      
      currentSong = currentSong->next;
    }
    
  g_list_free(albumn->songs);
  free(albumn->title);
  
  if (albumn->albumns)
    {
      currentAlbumn = g_list_first(albumn->albumns)->next;
      
      while (currentAlbumn)
        {
          destroyAlbumnList((albumnNode *)(currentAlbumn->data));
          currentAlbumn = currentAlbumn->next;
        }
        
      g_list_free(albumn->albumns);  
    }
    
  free(albumn);
}

/********************************
  Playlist Startup/Shutdown Code
 ********************************/

void initializePlaylist()
{
  activePlayList = g_list_alloc();
    
  currentSong = activePlayList;
}

void destroyPlaylist()
{
  g_list_free(activePlayList);
}

/******************************
  Albumnlist Loading/Saving Code
 ******************************/

void loadAlbumns(char *fileName)
{
  FILE *inputFile;
  albumnNode *albumnStack[ALBUMN_STACK_SIZE];  /* Stack for loading albumns */
  songNode *currentSong;
  
  int stackPos = 0;
  char *name = (char *)malloc(255);
  char *title = (char *)malloc(255);
  char *tempBuffer = (gchar *)malloc(255);
  char buffer;
  
  inputFile = fopen(fileName, "r");
  
  if (! inputFile)
    return;
    
  fgets(tempBuffer, 255, inputFile);
  chop(tempBuffer);
    
  if (strcmp(tempBuffer, albumnlist_id_string) != 0)
    {
      fclose(inputFile);
      return;
    }
    
  destroyPlaylist();
  initializePlaylist();
  
  destroyAlbumnList(albumnList);
  albumnStack[0] = NULL;
  
  while (!feof(inputFile) && (stackPos >= 0))
    {
      buffer = fgetc(inputFile);
      
      if (buffer == '\\')
        {
          fgets(name, 255, inputFile);
          if (name[0] == '\n')
            {
              stackPos--;
              continue; 
            }
          else
            stackPos++;    
        
          chop(name);
          albumnStack[stackPos] = newAlbumn(name, albumnStack[stackPos-1]);
          
          if (albumnStack[stackPos-1])
            g_list_append(albumnStack[stackPos-1]->albumns, albumnStack[stackPos]);
        }
      else if (albumnStack[stackPos] != NULL)
        {
          ungetc(buffer, inputFile);
          fscanf(inputFile, "%254[^\t\n]", name);
          
          if (feof(inputFile))
            continue;
            
          fgets(title, 255, inputFile);
          if (strlen(title) > 0)
            {
              title[strlen(title)-1] = '\0';
              currentSong = newSong(name, &title[1], albumnStack[stackPos]);
            }
          else
            currentSong = newSong(name, NULL, albumnStack[stackPos]);
            
          g_list_append(albumnStack[stackPos]->songs, currentSong);
        }  
    }
    
  albumnList = albumnStack[1];

  fclose(inputFile);
}

/*
  As the playlist is built in a roughly tree-like fashion, a nice
  recursive save routine is easy to write...
*/

void doSaveAlbumns(albumnNode *albumn, FILE *outputFile, int root)
{
  GList *currentSong;
  GList *currentAlbumn;
  songNode *currentSongData;
  
  fprintf(outputFile, "\\%s\n", albumn->title);
    
  currentSong = g_list_first(albumn->songs)->next;
  currentAlbumn = g_list_first(albumn->albumns)->next;
  
  while (currentSong)
    {
      currentSongData = (songNode *)(currentSong->data);
      fprintf(outputFile, "%s\t%s\n", currentSongData->fileName,
                                      currentSongData->title);
      currentSong = currentSong->next;
    }
    
  while (currentAlbumn)
    {
      doSaveAlbumns((albumnNode *)(currentAlbumn->data), outputFile, FALSE);
      currentAlbumn = currentAlbumn->next;
    }
    
  fprintf(outputFile, "\\\n");
}

/*
  Calls doSave with the base case for the recursion.
*/

void saveAlbumns(char *fileName)
{
  FILE *outputFile = fopen(fileName, "w");    
 
  if (outputFile && albumnList)
    {
      fprintf(outputFile, "%s\n", albumnlist_id_string);
      doSaveAlbumns(albumnList, outputFile, TRUE);
    }  
  
  fclose(outputFile);    
}

/******************************
  Playlist loading/saving code
 ******************************/

void loadPlayList(char *fileName)
{
  FILE *inputFile;
  char *title = (char *)malloc(255);
  char *fname = (char *)malloc(255);  
  char *buffer = (char *)malloc(255);
  songNode *song;
  gint row;

  inputFile = fopen(fileName, "r");
  if (! inputFile)
    return;
    
  fgets(buffer, 255, inputFile);  
  if (! strcmp(buffer, playlist_id_string))
    {
      fclose(inputFile);
      return;
    }
    
  while (! feof(inputFile))
    {
      fscanf(inputFile, "%254[^\t\n]", fname);      
      
      if (feof(inputFile))
        continue;
        
      fgets(title, 255, inputFile);
      title[strlen(title)-1] = '\0';
      
      song = newSong(fname, &(title[1]), NULL);
      row = gtk_clist_append(GTK_CLIST(playlist), &(song->title));
      gtk_clist_set_row_data(GTK_CLIST(playlist), row, song);      
      
      g_list_append(activePlayList, song);
      
    }
    
  fclose(inputFile);
}

void savePlayList(char *fileName)
{
  FILE *outputFile;
  GList *currentSong = g_list_first(activePlayList)->next;
  songNode *song;
  
  outputFile = fopen(fileName, "w");

  if (! currentSong)
    return;
    
  fprintf(outputFile, "%s\n", playlist_id_string);
    
  while(currentSong)
    {
      song = (songNode *)(currentSong->data);
      fprintf(outputFile, "%s\t%s\n", song->fileName, song->title);
      currentSong = currentSong->next;
    }
  
  fclose(outputFile);
}

void clearPlayList(GtkWidget *widget, gpointer *data)
{
  GList *currentNode;
  songNode *song;
  
  currentNode = g_list_first(activePlayList)->next;
  
  while (currentNode)
    {
      song = (songNode *)(currentNode->data);
      
      if (song->parent == NULL)
        {
          free(song->fileName);
          free(song->title);
        }
        
      currentNode = currentNode->next;
    }
    
  g_list_free(activePlayList);
  activePlayList = g_list_alloc();
  currentSong = activePlayList;
  
  gtk_clist_clear(GTK_CLIST(playlist));  
}

/********************
  Misc List Routines
 ********************/  

void addFileToPlayList(char *fileName)
{
  songNode *song;
  
  song = newSong(fileName, NULL, NULL);
  g_list_append(activePlayList, song);
}

void clearPlayedFlags()
{
  GList *currentSong;
  songNode *song;
  
  currentSong = g_list_first(activePlayList)->next;
  
  while (currentSong)
    {
      song = (songNode *)(currentSong->data);
      song->played = FALSE;
      
      currentSong = currentSong->next;
    }
}

/***************
  Tree Routines 
 ***************/

void constructTree(GtkWidget *tree, albumnNode *albumn)
{
  GtkWidget *item;
  GtkWidget *branch;
  GList *currentAlbumn;
  
  if (! albumn)
    return;

  item = gtk_tree_item_new_with_label(albumn->title);
  gtk_widget_show(item);
  gtk_tree_append(GTK_TREE(tree), item);
  gtk_object_set_data(GTK_OBJECT(item), tree_item_id_string, albumn);
  gtk_signal_connect(GTK_OBJECT(item), "button_press_event",
                     GTK_SIGNAL_FUNC(popupMenu), &treeMenu);

  currentAlbumn = g_list_first(albumn->albumns)->next;

  if (currentAlbumn)
    {
      branch = gtk_tree_new();
      gtk_widget_show(branch);
      gtk_tree_item_set_subtree(GTK_TREE_ITEM(item), branch);
    }

  while (currentAlbumn)
    {      
      constructTree(branch, (albumnNode *)(currentAlbumn->data));      
      currentAlbumn = currentAlbumn->next;
    }
}

void clearTree(GtkWidget *widget, GtkWidget *tree)
{
  gtk_tree_clear_items(GTK_TREE(tree), 0, -1);  
}

void treeSelectionChanged()
{
  GList *selection;
  GList *songs;
  songNode *currentSong;
  albumnNode *currentAlbumn;
  gint row;

  selection = GTK_TREE_SELECTION(tree);
  if (! selection)
    {
      gtk_clist_clear(GTK_CLIST(songlist));
      return;
    }

  currentAlbumn = gtk_object_get_data(GTK_OBJECT(selection->data), tree_item_id_string);  
  songs = g_list_first(currentAlbumn->songs)->next;

  gtk_clist_freeze(GTK_CLIST(songlist));
  gtk_clist_clear(GTK_CLIST(songlist));
  while (songs)
    {
      currentSong = (songNode *)(songs->data);
      row = gtk_clist_append(GTK_CLIST(songlist), &(currentSong->title));
      gtk_clist_set_row_data(GTK_CLIST(songlist), row, currentSong);

      songs = songs->next;
    }
  gtk_clist_thaw(GTK_CLIST(songlist));  
}

/************************
  Albumn Management Code 
 ************************/
 
void addWholeAlbumn()
{
  GList *selection = GTK_TREE_SELECTION(tree);
  GList *songs;
  albumnNode *albumn;
  songNode *currentSong;
  gint row;
  
  if (! selection)
    return;
  
  albumn = gtk_object_get_data(GTK_OBJECT(selection->data), tree_item_id_string);
  songs = g_list_first(albumn->songs)->next;
  
  while (songs)
    {
      currentSong = (songNode *)(songs->data);
    
      g_list_append(activePlayList, songs->data);
      row = gtk_clist_append(GTK_CLIST(playlist), &(currentSong->title));
      gtk_clist_set_row_data(GTK_CLIST(playlist), row, currentSong);
      
      songs = songs->next;
    }
}

void createAlbumn(GtkWidget *widget, GtkEntry *entry)
{
  GList *selection;
  GtkWidget *treeItem;
  GtkWidget *newTree;
  char *albumnTitle;
  
  albumnNode *albumn;
  albumnNode *hostAlbumn;    
  
  selection = GTK_TREE_SELECTION(tree);
  albumnTitle = gtk_entry_get_text(entry);
  
  if (! selection || ! albumnTitle || (strlen(albumnTitle) == 0))
    return;
    
  treeItem = (GtkWidget *)(selection->data);
  hostAlbumn = gtk_object_get_data(GTK_OBJECT(treeItem), 
                                   tree_item_id_string);
                                   
  albumn = newAlbumn(albumnTitle, hostAlbumn);
  g_list_append(hostAlbumn->albumns, albumn);

  if (! GTK_TREE_ITEM(treeItem)->subtree)
    {
      newTree = gtk_tree_new();
      gtk_widget_show(newTree);
      gtk_tree_item_set_subtree(GTK_TREE_ITEM(treeItem), newTree);
    }
  else
    newTree = GTK_TREE_ITEM(treeItem)->subtree; 
  
  treeItem = gtk_tree_item_new_with_label(albumnTitle);
  gtk_object_set_data(GTK_OBJECT(treeItem), tree_item_id_string, albumn);
  gtk_widget_show(treeItem);
  gtk_tree_append(GTK_TREE(newTree), treeItem);
  
  gtk_signal_connect(GTK_OBJECT(treeItem), "button_press_event",
                     GTK_SIGNAL_FUNC(popupMenu), &treeMenu);
}

void createAlbumnDialog()
{
  GtkWidget *window;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *button;
    
  if (! GTK_TREE_SELECTION(tree))
    return;

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "Create Albumn");
  gtk_window_set_wmclass(GTK_WINDOW(window), "gmplay.create_albumn", NULL);
  
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(window), vbox);
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  
  label = gtk_label_new("Albumn Name");
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  
  entry = gtk_entry_new();
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  
  button = gtk_button_new_with_label("OK");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);    
  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(createAlbumn), entry);
                     
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_destroy),
                            GTK_OBJECT(window));
  
  button = gtk_button_new_with_label("Cancel");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_destroy),
                            GTK_OBJECT(window));
                            
  gtk_widget_show(window);                            
}

void deleteAlbumn()
{
  GList *selection = GTK_TREE_SELECTION(tree);
  GtkWidget *treeItem;
  albumnNode *albumn;
  albumnNode *parentAlbumn;
  
  if (! selection)
    return;
    
  treeItem = (GtkWidget *)(selection->data);
  albumn = gtk_object_get_data(GTK_OBJECT(treeItem), tree_item_id_string);
  parentAlbumn = (gpointer)(albumn->parent);
  
/* Can't delete the root albumn node... */
    
  if (parentAlbumn == NULL)
    return;
  
  gtk_widget_destroy(treeItem);
  destroyAlbumnList(albumn);
  
  g_list_remove(parentAlbumn->albumns, albumn);  
  gtk_signal_emit_by_name(GTK_OBJECT(tree), "selection_changed");
}

/*
  This function uses the ROW value not only as the row, but also as a
  method of passing control-commands to the callback.  It's messy, but it
  works nicely.
*/  

void changeAlbumnName(GtkWidget *widget, gint command)
{    
  static albumnNode *albumn = NULL;
  static GtkWidget *entry;
  static GtkTreeItem *item;
  GList *selection;
  char *text;
  
  if (command == -2)
    {
      selection = (GList *)(widget);
      item = GTK_TREE_ITEM(selection->data);
      albumn = gtk_object_get_data(GTK_OBJECT(item),
                                   tree_item_id_string);      
    }
  else if ((command == -1) && (! entry))
    entry = widget;
  else if ((command == 0) && (entry))
    {
      text = gtk_entry_get_text(GTK_ENTRY(entry));
      
      free(albumn->title);
      albumn->title = (char *)malloc(strlen(text)+1);
      strcpy(albumn->title, text);
      
      gtk_label_set(GTK_LABEL(GTK_BIN(item)->child), text);      
      entry = NULL;
    }      
}

void renameAlbumnDialog()
{
  GList *selection = GTK_TREE_SELECTION(tree);
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *entry;
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *hbox;
  
  if (! selection)
    return;
  
  changeAlbumnName((GtkWidget *)(selection), -2);
  
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "Rename Albumn");
  gtk_window_set_wmclass(GTK_WINDOW(window), "gmplay.rename_albumn", NULL);
  
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(window), vbox);
  gtk_widget_show(vbox);
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);
  
  label = gtk_label_new("Albumn Name");
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  
  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
  gtk_widget_show(entry);  
  
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(changeAlbumnName),
                     (gint *)(-1));
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);
  
  button = gtk_button_new_with_label("OK");
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  gtk_widget_show(button);
  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(changeAlbumnName),
                     (gint *)(0));
                     
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_destroy),
                            GTK_OBJECT(window));                     
  
  button = gtk_button_new_with_label("Cancel");
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  gtk_widget_show(button);    
  
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_destroy),
                            GTK_OBJECT(window));
  
  gtk_widget_show(window);
}

void createTreeMenu()
{
  GtkWidget *menuItem;

  if (treeMenu)
    return;
  
  treeMenu = gtk_menu_new();
  
  gtk_widget_realize(treeMenu);
  gdk_window_set_cursor((treeMenu)->window, gdk_cursor_new(GDK_LEFT_PTR));
  
  menuItem = gtk_menu_item_new_with_label("Add to Playlist");
  gtk_menu_append(GTK_MENU(treeMenu), menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(addWholeAlbumn), NULL);
  gtk_widget_show(menuItem);
                     
  menuItem = gtk_menu_item_new_with_label("Create Albumn...");
  gtk_menu_append(GTK_MENU(treeMenu), menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(createAlbumnDialog), tree);
  gtk_widget_show(menuItem);    

  menuItem = gtk_menu_item_new_with_label("Rename Albumn...");
  gtk_menu_append(GTK_MENU(treeMenu), menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(renameAlbumnDialog), tree);
  gtk_widget_show(menuItem);    
                     
  menuItem = gtk_menu_item_new_with_label("Delete Albumn");
  gtk_menu_append(GTK_MENU(treeMenu), menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(deleteAlbumn), tree);
  gtk_widget_show(menuItem);        
}

/**********************
  Song Management Code
 **********************/

void copyToPlayList()
{
  GList *selectedRows = GTK_CLIST(songlist)->selection;
  GList *selections;
  songNode *song;
  gint row, newRow;
  
  selections = g_list_alloc();

  while (selectedRows)
    {
      g_list_append(selections, selectedRows->data);
      selectedRows = selectedRows->next;
    }
    
  selections = g_list_first(selections)->next;    
  
  while (selections)
    {
      row = (gint)(selections->data);
      song = gtk_clist_get_row_data(GTK_CLIST(songlist), row);
    
      g_list_append(activePlayList, song);
      newRow = gtk_clist_append(GTK_CLIST(playlist), &(song->title));
      gtk_clist_set_row_data(GTK_CLIST(playlist), newRow, song);
      
      gtk_clist_unselect_row(GTK_CLIST(songlist), row, 0);
            
      selections = selections->next;
    }
  
  g_list_free(selections);
}

void addToSongList(GtkWidget *widget, GtkFileSelection *fileSel, albumnNode *albumn)
{
  GList *selections;
  static albumnNode *currentAlbumn = NULL;  
  songNode *song;
  char *fileName;
  gint row;
  
  if ((widget == NULL) && (fileSel == NULL))
    {
      currentAlbumn = albumn;
      return;
    }
  else if (currentAlbumn == NULL)
    return;
    
  selections = GTK_CLIST(fileSel->file_list)->selection;
  
  while (selections)
    {
      row = (gint)(selections->data);
      gtk_clist_get_text(GTK_CLIST(fileSel->file_list), row, 0, &fileName);
      song = newSong(completeFileName(fileSel, fileName), NULL, currentAlbumn);
      g_list_append(currentAlbumn->songs, song);
      selections = selections->next;
    }
    
  treeSelectionChanged();
  
  currentAlbumn = NULL;
}

void addDirToSongList(GtkWidget *widget, GtkFileSelection *fileSel, albumnNode *albumn)
{
  static albumnNode *currentAlbumn = NULL;  
  songNode *song;
  char *fileName;
  gint row;
  
  if ((widget == NULL) && (fileSel == NULL))
    {
      currentAlbumn = albumn;
      return;
    }
  else if (currentAlbumn == NULL)
    return;
    
  for (row = 0; row < GTK_CLIST(fileSel->file_list)->rows; row++)
    {
      gtk_clist_get_text(GTK_CLIST(fileSel->file_list), row, 0, &fileName);
      song = newSong(completeFileName(fileSel, fileName), NULL, currentAlbumn);
      g_list_append(currentAlbumn->songs, song);
    }
    
  treeSelectionChanged();
  
  currentAlbumn = NULL;
}

void addToSongListDialog()
{
  static GtkWidget *fileSel = NULL;
  GList *selection;
  albumnNode *albumn;
      
  selection = GTK_TREE_SELECTION(tree);
  if (! selection)
    return;
  else if ((! fileSel) || (! GTK_WIDGET_VISIBLE(fileSel)))
    {
      albumn = gtk_object_get_data(GTK_OBJECT(selection->data),
                                   tree_item_id_string);    
      addToSongList(NULL, NULL, albumn);
      addDirToSongList(NULL, NULL, albumn);
    }
  
  if (! fileSel)
    {      
      fileSel = createFileDialog("Add to Albumn", 
                                 GTK_SIGNAL_FUNC(addToSongList),
                                 GTK_SIGNAL_FUNC(addDirToSongList));
      gtk_widget_show(fileSel);
    }
  else 
    openDialog(NULL, fileSel);
}

void deleteFromSongList()
{
  GList *selections = GTK_CLIST(songlist)->selection;
  GList *songTitles;
  songNode *song;
  albumnNode *albumn;
  char *text, *textCopy;
  gint row, i, count;

  gtk_clist_freeze(GTK_CLIST(songlist));
  songTitles = g_list_alloc();

  count = 0;
  while (selections)
    {
      row = (gint)(selections->data);
      song = gtk_clist_get_row_data(GTK_CLIST(songlist), row);
      albumn = (albumnNode *)(song->parent);
      
      g_list_remove(albumn->songs, song);
      free(song->title);
      free(song->fileName);
      free(song);      

      gtk_clist_get_text(GTK_CLIST(songlist), row, 0, &(text));
      textCopy = (char *)malloc(strlen(text)+1);
      strcpy(textCopy, text);
      
      g_list_append(songTitles, textCopy);
      
      selections = selections->next;
      count++;
    }
      
  i = -1;
  songTitles = g_list_first(songTitles)->next;        
  
  while (songTitles)
    {      
      i = (i + 1) % (GTK_CLIST(songlist)->rows);      
      gtk_clist_get_text(GTK_CLIST(songlist), i, 0, &(text));
      
      if (strcmp(text, (char *)(songTitles->data)) == 0)
        {
          gtk_clist_remove(GTK_CLIST(songlist), i);
          songTitles = songTitles->next;
        }
    }

  g_list_free(songTitles);    
  gtk_clist_thaw(GTK_CLIST(songlist));
}

/*
  This function uses the ROW value not only as the row, but also as a
  method of passing control-commands to the callback.  It's messy, but it
  works nicely.
*/  

void changeSongProperties(GtkWidget *widget, gint row)
{    
  GList *selection;
  static GtkWidget *entry = NULL;
  static gint selectedRow = -1;
  char *text;
  songNode *song;
  
  if (row == -2)
    {
      selection = (GList *)(widget);
      selectedRow = (gint)(selection->data);
    }
  else if ((row == -1) && (! entry))
    entry = widget;
  else if ((row >= 0) && (entry))
    {
      text = gtk_entry_get_text(GTK_ENTRY(entry));
      
      song = gtk_clist_get_row_data(GTK_CLIST(songlist), selectedRow);
      free(song->title);
      song->title = (char *)malloc(strlen(text)+1);
      strcpy(song->title, text);
      
      gtk_clist_set_text(GTK_CLIST(songlist), selectedRow, 0, song->title);
      
      entry = NULL;
    }
}

void songPropertiesDialog()
{
  GList *selection = GTK_CLIST(songlist)->selection;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *entry;
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *hbox;
  songNode *song;
  
  if (! selection)
    return;
    
  song = gtk_clist_get_row_data(GTK_CLIST(songlist), (gint)(selection->data));
  
  /* Initialize callback */
  changeSongProperties((GtkWidget *)(selection), -2);
  
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "Song Properties");
  gtk_window_set_wmclass(GTK_WINDOW(window), "gmplay.song_properties", NULL);
  
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(window), vbox);
  gtk_widget_show(vbox);
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);
  
  label = gtk_label_new("Song Title");
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  
  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
  gtk_entry_set_text(GTK_ENTRY(entry), song->title);
  gtk_widget_show(entry);  
  
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(changeSongProperties),
                     (gint *)(-1));
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);
  
  button = gtk_button_new_with_label("OK");
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  gtk_widget_show(button);
  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(changeSongProperties),
                     (gint *)(selection->data));
                     
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_destroy),
                            GTK_OBJECT(window));                     
  
  button = gtk_button_new_with_label("Cancel");
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  gtk_widget_show(button);    
  
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_destroy),
                            GTK_OBJECT(window));
  
  gtk_widget_show(window);
}

void writeID3Tags()
{
  FILE *mp3file;
  mp3Tag tag;
  GList *selections;
  songNode *song;
  gint i;
  gint row;

  selections = GTK_CLIST(songlist)->selection;  

  while (selections)
    {      
      row = (gint)(selections->data);
      song = gtk_clist_get_row_data(GTK_CLIST(songlist), row);

      mp3file = fopen(song->fileName, "r+");
      if (mp3file)
        {
          if (checktag(mp3file))
            {
              readtag(mp3file, &tag);
              tag.title[0] = '\0';
            }
          else
            {
              for (i = 0; i < 3; i++)
                tag.tag[i] = ' ';

              for (i = 0; i < 4; i++)
                tag.year[i] = ' ';

              for (i = 0; i < 30; i++)
                {
                  tag.title[i] = ' ';
                  tag.artist[i] = ' ';
                  tag.albumn[i] = ' ';
                  tag.comment[i] = ' ';
                }

              tag.genre = 0;
            }

          tag.tag[0] = 'T'; tag.tag[1] = 'A'; tag.tag[2] = 'G';
          
          for (i = 0; (i < strlen(song->title)) && (i < 30); i++)
            tag.title[i] = song->title[i];

          for (i = strlen(song->title); i < 30; i++)
            tag.title[i] = ' ';

          savetag(mp3file, tag);
          fclose(mp3file);
        }
        
      selections = selections->next;
    }
}
 
void createSongListMenu()
{
  GtkWidget *menuItem;
  
  if (songMenu)
    return;
    
  songMenu = gtk_menu_new();
  
  gtk_widget_realize(songMenu);
  gdk_window_set_cursor(songMenu->window, gdk_cursor_new(GDK_LEFT_PTR));
  
  menuItem = gtk_menu_item_new_with_label("Add to Playlist");
  gtk_menu_append(GTK_MENU(songMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(copyToPlayList), NULL);
                     
  menuItem = gtk_menu_item_new_with_label("Add Song(s)...");
  gtk_menu_append(GTK_MENU(songMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(addToSongListDialog), NULL);

  menuItem = gtk_menu_item_new_with_label("Delete Song(s)");
  gtk_menu_append(GTK_MENU(songMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(deleteFromSongList), NULL);

  menuItem = gtk_menu_item_new_with_label("Song Properties...");
  gtk_menu_append(GTK_MENU(songMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(songPropertiesDialog), NULL);
                     
  menuItem = gtk_menu_item_new_with_label("Write ID3 Tag(s)");
  gtk_menu_append(GTK_MENU(songMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(writeID3Tags), NULL);
}

/**************************
  Playlist Management Code 
 **************************/

void addToPlayList(GtkWidget *widget, GtkFileSelection *fileSel, albumnNode *albumn)
{
  GList *selections;
  songNode *song;
  char *fileName;
  gint row;
  
  selections = GTK_CLIST(fileSel->file_list)->selection;
  
  while (selections)
    {
      row = (gint)(selections->data);
      gtk_clist_get_text(GTK_CLIST(fileSel->file_list), row, 0, &fileName);
      song = newSong(completeFileName(fileSel, fileName), NULL, NULL);
      
      gtk_clist_append(GTK_CLIST(playlist), &(song->title));
      gtk_clist_set_row_data(GTK_CLIST(playlist), row, song);
            
      g_list_append(activePlayList, song);
      selections = selections->next;
    }
}

void addDirToPlayList(GtkWidget *widget, GtkFileSelection *fileSel, albumnNode *albumn)
{
  songNode *song;
  char *fileName;
  gint row;
  
  for (row = 0; row < GTK_CLIST(fileSel->file_list)->rows; row++)
    {
      gtk_clist_get_text(GTK_CLIST(fileSel->file_list), row, 0, &fileName);
      song = newSong(completeFileName(fileSel, fileName), NULL, NULL);
      
      gtk_clist_append(GTK_CLIST(playlist), &(song->title));
      gtk_clist_set_row_data(GTK_CLIST(playlist), row, song);      
      
      g_list_append(activePlayList, song);
    }
}

void addToPlayListDialog()
{
  static GtkWidget *fileSel = NULL;
      
  if (! fileSel)
    {      
      fileSel = createFileDialog("Add to Albumn", 
                                 GTK_SIGNAL_FUNC(addToPlayList),
                                 GTK_SIGNAL_FUNC(addDirToPlayList));
      gtk_widget_show(fileSel);
    }
  else
    openDialog(NULL, fileSel);  
}
 
void deleteFromPlayList()
{
  GList *selections = GTK_CLIST(playlist)->selection;
  GList *songTitles;
  GList *songListNode;
  songNode *song;
  char *text, *textCopy;
  gint row, i, count;

  gtk_clist_freeze(GTK_CLIST(playlist));
  songTitles = g_list_alloc();

  count = 0;
  while (selections)
    {
      row = (gint)(selections->data);
      song = gtk_clist_get_row_data(GTK_CLIST(playlist), row);
      
      songListNode = g_list_find(activePlayList, song);
      if (currentSong == songListNode)
        currentSong = activePlayList;
      
      g_list_remove(activePlayList, song);
      
      gtk_clist_get_text(GTK_CLIST(playlist), row, 0, &(text));
      textCopy = (char *)malloc(strlen(text)+1);
      strcpy(textCopy, text);
      
      g_list_append(songTitles, textCopy);
      
      selections = selections->next;
      count++;
    }
      
  i = -1;
  songTitles = g_list_first(songTitles)->next;        
  
  while (songTitles)
    {      
      i = (i + 1) % (GTK_CLIST(playlist)->rows);      
      gtk_clist_get_text(GTK_CLIST(playlist), i, 0, &(text));
      
      if (strcmp(text, (char *)(songTitles->data)) == 0)
        {
          gtk_clist_remove(GTK_CLIST(playlist), i);
          songTitles = songTitles->next;
        }
    }

  g_list_free(songTitles);    
  gtk_clist_thaw(GTK_CLIST(playlist));
}

void createPlayListMenu()
{
  GtkWidget *menuItem;
  
  if (playlistMenu)
    return;
    
  playlistMenu = gtk_menu_new();
  
  gtk_widget_realize(playlistMenu);
  gdk_window_set_cursor(playlistMenu->window, gdk_cursor_new(GDK_LEFT_PTR));
  
  menuItem = gtk_menu_item_new_with_label("Add Song(s)...");
  gtk_menu_append(GTK_MENU(playlistMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(addToPlayListDialog), NULL);
  
  menuItem = gtk_menu_item_new_with_label("Remove Song(s)...");
  gtk_menu_append(GTK_MENU(playlistMenu), menuItem);
  gtk_widget_show(menuItem);
  gtk_signal_connect(GTK_OBJECT(menuItem), "activate",
                     GTK_SIGNAL_FUNC(deleteFromPlayList), NULL);
}

/*******************************
  Albumnlist file management code
 *******************************/

void loadAlbumnCallback(GtkWidget *widget, GtkFileSelection *fileSel)
{
  char *fileName;
  
  fileName = gtk_file_selection_get_filename(fileSel);
  gtk_tree_clear_items(GTK_TREE(tree), 0, -1);
  gtk_object_set_data(GTK_OBJECT(tree), tree_item_id_string, &albumnList);
  
  loadAlbumns(fileName);  
  constructTree(tree, albumnList);
}

void loadAlbumnDialog()
{
  static GtkFileSelection *fileSel = NULL;
  
  if (! fileSel)
    {
      fileSel = GTK_FILE_SELECTION(gtk_file_selection_new("Load Albumnlist"));
      
      gtk_file_selection_hide_fileop_buttons(fileSel);

      gtk_signal_connect(GTK_OBJECT(fileSel), "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
      gtk_signal_connect(GTK_OBJECT(fileSel), "delete_event",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
                               
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(loadAlbumnCallback), fileSel);
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);
      
      gtk_signal_connect(GTK_OBJECT(fileSel->cancel_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);
                         
      gtk_widget_show(GTK_WIDGET(fileSel));
    }    
  else
    openDialog(NULL, GTK_WIDGET(fileSel));
}

void saveAlbumnCallback(GtkWidget *widget, GtkFileSelection *fileSel)
{
  char *fileName;
  
  fileName = gtk_file_selection_get_filename(fileSel);
  saveAlbumns(fileName);
}

void saveAlbumnDialog()
{
  static GtkFileSelection *fileSel = NULL;
  
  if (! fileSel)
    {
      fileSel = GTK_FILE_SELECTION(gtk_file_selection_new("Save Albumnlist"));
      
      gtk_file_selection_hide_fileop_buttons(fileSel);

      gtk_signal_connect(GTK_OBJECT(fileSel), "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
      gtk_signal_connect(GTK_OBJECT(fileSel), "delete_event",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
                               
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(saveAlbumnCallback), fileSel);
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);

      gtk_signal_connect(GTK_OBJECT(fileSel->cancel_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);
                         
      gtk_widget_show(GTK_WIDGET(fileSel));
    }    
  else
    openDialog(NULL, GTK_WIDGET(fileSel));
}

/*******************************
  Playlist file management code
 *******************************/

void loadPlayListCallback(GtkWidget *widget, GtkFileSelection *fileSel)
{
  char *fileName;
  
  fileName = gtk_file_selection_get_filename(fileSel);
  
  clearPlayList(NULL, NULL);
  loadPlayList(fileName);  
}

void loadPlayListDialog()
{
  static GtkFileSelection *fileSel = NULL;
  
  if (! fileSel)
    {
      fileSel = GTK_FILE_SELECTION(gtk_file_selection_new("Load Playlist"));
      
      gtk_file_selection_hide_fileop_buttons(fileSel);

      gtk_signal_connect(GTK_OBJECT(fileSel), "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
      gtk_signal_connect(GTK_OBJECT(fileSel), "delete_event",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
                               
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(loadPlayListCallback), fileSel);
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);
      
      gtk_signal_connect(GTK_OBJECT(fileSel->cancel_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);
                         
      gtk_widget_show(GTK_WIDGET(fileSel));
    }    
  else
    openDialog(NULL, GTK_WIDGET(fileSel));
}

void savePlayListCallback(GtkWidget *widget, GtkFileSelection *fileSel)
{
  char *fileName;
  
  fileName = gtk_file_selection_get_filename(fileSel);
  savePlayList(fileName);
}

void savePlayListDialog()
{
  static GtkFileSelection *fileSel = NULL;
  
  if (! fileSel)
    {
      fileSel = GTK_FILE_SELECTION(gtk_file_selection_new("Save Playlist"));
      
      gtk_file_selection_hide_fileop_buttons(fileSel);

      gtk_signal_connect(GTK_OBJECT(fileSel), "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
      gtk_signal_connect(GTK_OBJECT(fileSel), "delete_event",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
                               
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(savePlayListCallback), fileSel);
      gtk_signal_connect(GTK_OBJECT(fileSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);

      gtk_signal_connect(GTK_OBJECT(fileSel->cancel_button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), fileSel);
                         
      gtk_widget_show(GTK_WIDGET(fileSel));
    }    
  else
    openDialog(NULL, GTK_WIDGET(fileSel));
}

static addDroppedToPlaylist(GtkWidget *widget, GdkEventDropDataAvailable *event)
{
  int count = event->data_numbytes;
  char *p = event->data;
  int len,num;
  gint row;
  songNode *song;
   
  do {
    len = 1 + strlen (p);
    count -= len;
    addFileToPlayList(p);
    song = newSong(p, NULL, NULL);
    
    gtk_clist_append(GTK_CLIST(playlist), &(song->title));
    gtk_clist_set_row_data(GTK_CLIST(playlist), row, song);
    p += len;
  } while (count > 0);
} 

/****************************************
  Routines for creating the list dialog. 
 ****************************************/

void createListDialog()
{
  static GtkWidget *editorWindow;
  static GtkWidget *playlistWindow;  
  
  GtkWidget *scrolledWindow;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *button;
  songNode *song;
  GList *currentSong;
  gint row;
  char *accepted_drop_types[] = {"url:ALL"};
    
  if (! playlistWindow)
    {
      playlistWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      gtk_window_set_title(GTK_WINDOW(playlistWindow), "Active Playlist");
      gtk_window_set_wmclass(GTK_WINDOW(playlistWindow), "gmplay.active_playlist", NULL);
      gtk_signal_connect(GTK_OBJECT(playlistWindow), "delete_event",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);
      
      
      vbox = gtk_vbox_new(FALSE, 0);
      gtk_container_add(GTK_CONTAINER(playlistWindow), vbox);
      gtk_widget_show(vbox);
      
      /* Setup CList */

      playlist = gtk_clist_new(1);
      gtk_clist_set_selection_mode(GTK_CLIST(playlist), GTK_SELECTION_MULTIPLE);
      gtk_widget_set_usize(playlist, 200, 150);
      gtk_widget_show(playlist);
      gtk_box_pack_start(GTK_BOX(vbox), playlist, TRUE, TRUE, 0);
      
      currentSong = g_list_first(activePlayList)->next;
      while (currentSong)
        {
          song = (songNode *)(currentSong->data);
          row = gtk_clist_append(GTK_CLIST(playlist), &(song->title));
          gtk_clist_set_row_data(GTK_CLIST(playlist), row, song);
          
          currentSong = currentSong->next;
        }
      
      createPlayListMenu();
      gtk_widget_realize (playlist);
      gtk_signal_connect(GTK_OBJECT(playlist), "button_press_event",
                         GTK_SIGNAL_FUNC(popupMenu), &playlistMenu);
      
      gtk_signal_connect (GTK_OBJECT (playlist), 
			  "drop_data_available_event",
			  GTK_SIGNAL_FUNC(addDroppedToPlaylist),
			  playlist);
  
      gdk_window_dnd_drop_set (GTK_CLIST(playlist)->clist_window, TRUE, 
			       accepted_drop_types, 1, FALSE);

      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
      gtk_widget_show(hbox);
                         
      button = gtk_button_new_with_label("Clear");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);
      
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(clearPlayList), NULL);
                        
      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
      gtk_widget_show(hbox);
                         
      button = gtk_button_new_with_label("Load");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(loadPlayListDialog), NULL);

      button = gtk_button_new_with_label("Save");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(savePlayListDialog), NULL);

      button = gtk_button_new_with_label("Close");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), playlistWindow);
                                                       
      insertCListHandlers(playlist);      
    }

  if (! editorWindow)
    {          
      editorWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      gtk_window_set_title(GTK_WINDOW(editorWindow), "Albumn Editor");
      gtk_window_set_wmclass(GTK_WINDOW(editorWindow), "gmplay.albumn_editor", NULL);
      gtk_signal_connect(GTK_OBJECT(editorWindow), "delete_event",
                         GTK_SIGNAL_FUNC(gtk_widget_hide), NULL);

      vbox = gtk_vbox_new(FALSE, 0);
      gtk_container_add(GTK_CONTAINER(editorWindow), vbox);
      gtk_widget_show(vbox);

      hbox = gtk_hbox_new(FALSE, 5);
      gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
      gtk_widget_show(hbox);

      /* Tree Setup Code */

      createTreeMenu();
      
      scrolledWindow = gtk_scrolled_window_new(NULL, NULL);
      gtk_widget_show(scrolledWindow);
      gtk_widget_set_usize(scrolledWindow, 150, 150);
      gtk_box_pack_start(GTK_BOX(hbox), scrolledWindow, FALSE, FALSE, 0);

      tree = gtk_tree_new();
      gtk_object_set_data(GTK_OBJECT(tree), tree_item_id_string, &albumnList);
      constructTree(tree, albumnList);
      gtk_widget_show(tree);
      gtk_container_add(GTK_CONTAINER(scrolledWindow), tree);
      
      insertTreeHandlers(tree);      

      /* End Tree Setup */

      /* Setup CList */

      songlist = gtk_clist_new(1);
      gtk_clist_set_selection_mode(GTK_CLIST(songlist), GTK_SELECTION_MULTIPLE);
      gtk_widget_set_usize(songlist, 200, 150);
      gtk_widget_show(songlist);
      gtk_box_pack_start(GTK_BOX(hbox), songlist, TRUE, TRUE, 0);
      
      createSongListMenu();
      gtk_signal_connect(GTK_OBJECT(songlist), "button_press_event",
                         GTK_SIGNAL_FUNC(popupMenu), &songMenu);

      /* End CList Setup */

      /* Setup Control Buttons */

      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
      gtk_widget_show(hbox);
                         
      button = gtk_button_new_with_label("Active Playlist");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);
      
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(openDialog), playlistWindow);

      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
      gtk_widget_show(hbox);
                         
      button = gtk_button_new_with_label("Load");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);
      
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(loadAlbumnDialog), NULL);

      button = gtk_button_new_with_label("Save");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(saveAlbumnDialog), NULL);
 
      button = gtk_button_new_with_label("Close");
      gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
      gtk_widget_show(button);

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(closeDialog), editorWindow);
                         
      /* Set up tree signal */

      gtk_signal_connect(GTK_OBJECT(tree), "selection_changed",
                         GTK_SIGNAL_FUNC(treeSelectionChanged), NULL);

      /* Done tree signal */

      gtk_widget_show(editorWindow);
    }
  else
    openDialog(NULL, editorWindow);
}
