/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "mixer.h"

#ifdef DRIVER_SOLARIS
  #include <sys/audioio.h>
#endif

#ifdef DRIVER_OSS_BSD
  #include <machine/soundcard.h>
#endif

#ifdef DRIVER_OSS_LINUX
  #include <linux/soundcard.h>
#endif

int mixerExists;
int mixerOpen;
int mixer_fd;
float balance;

mixer_vals mixerSettings;

void balanceAdjust(int value, int *result)
{
  int adjustedValue;
                                      
  if (value > 100)
    value = 100;
    
  if (-balance > value)
    balance = -value;
  else if (balance > value)
    balance = value;
    
  adjustedValue = (balance > 0) ? (adjustedValue = (int)(value - balance)) :
                                  (adjustedValue = (int)(value + balance));  
       
  if (balance == 0)
    *result = (value << 8) | value;
  else if (balance < 0)
    *result = (adjustedValue << 8) | value;
  else if (balance > 0)
    *result = (value << 8) | adjustedValue;
}

/*
  Global Volume  
*/  

#ifdef DRIVER_SOLARIS
/* Convert local gain into device parameters */
unsigned
scale_gain(unsigned g)
{
	return (AUDIO_MIN_GAIN + (unsigned)
	    (((double) (AUDIO_MAX_GAIN - AUDIO_MIN_GAIN)) *
	    ((double)g / (double)AUDIO_MAX_GAIN)));
}

/* Convert device gain into the local scaling factor */
unsigned
unscale_gain(unsigned g)
{
	return ((unsigned) ((double)AUDIO_MAX_GAIN *
	    (((double)(g - AUDIO_MIN_GAIN)) /
	    (double)(AUDIO_MAX_GAIN - AUDIO_MIN_GAIN))));
}
#endif /* SOLARIS */

int getVolume()
{
#ifdef DRIVER_SOLARIS
  audio_info_t ap;
  
  if (ioctl(mixer_fd, AUDIO_GETINFO, &ap) < 0)
    return -1;
  else
    {
      mixerSettings.globalVolume = unscale_gain(ap.play.gain);
      return 0;
    }
#else /* ! SOLARIS */
  int volume;
  if (ioctl(mixer_fd, MIXER_READ(SOUND_MIXER_VOLUME), &volume) < 0)
    return -1;
  else
    {
      mixerSettings.globalVolume = volume & 0x00ff;
      return 0;  
    }      
#endif /* SOLARIS */
}

int setVolume()
{
#ifdef DRIVER_SOLARIS
  audio_info_t ap;
  
  AUDIO_INITINFO(&ap);
  ap.play.gain = scale_gain(mixerSettings.globalVolume);
  
  if (ioctl(mixer_fd, AUDIO_SETINFO, &ap) < 0)
    return -1;
  else
    return 0;
#else /* ! SOLARIS */
  int volume;
  
  balanceAdjust(mixerSettings.globalVolume, &volume);

  if (ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_VOLUME), &volume) < 0)
    return -1;
  else
    return 0;  
#endif /* SOLARIS */
}

/*
  Line Volume
*/  

int getLineVolume()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int volume;
  
  if (ioctl(mixer_fd, MIXER_READ(SOUND_MIXER_LINE), &volume) < 0)
    return -1;
  else
    {
      mixerSettings.lineVolume = volume & 0x00ff;
      return 0;  
    }
#endif /* SOLARIS */
}

int setLineVolume()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int volume;
  
  balanceAdjust(mixerSettings.lineVolume, &volume);

  if (ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_LINE), &volume) < 0)
    return -1;
  else
    return 0;  
#endif /* SOLARIS */
}

/*
  Bass Setting  
*/  

int getBass()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  if (ioctl(mixer_fd, MIXER_READ(SOUND_MIXER_BASS), &value) < 0)
    return -1;
  else
    {
      mixerSettings.bass = value & 0x00ff;
      return 0;  
    }      
#endif /* SOLARIS */
}

int setBass()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  value = (mixerSettings.bass << 8) + mixerSettings.bass;

  if (ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_BASS), &value) < 0)
    return -1;
  else
    return 0;  
#endif /* SOLARIS */
}

/*
  Treble Setting
*/  

int getTreble()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  if (ioctl(mixer_fd, MIXER_READ(SOUND_MIXER_TREBLE), &value) < 0)
    return -1;
  else
    {
      mixerSettings.treble = value & 0x00ff;
      return 0;  
    }      
#endif /* SOLARIS */
}

int setTreble()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  value = (mixerSettings.treble << 8) + mixerSettings.treble;
  
  if (ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_TREBLE), &value) < 0)
    return -1;
  else
    return 0;  
#endif /* SOLARIS */
}

/*
  PCM Setting  
*/  

int getPCM()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  if (ioctl(mixer_fd, MIXER_READ(SOUND_MIXER_PCM), &value) < 0)
    return -1;
  else
    {
      mixerSettings.PCM = value & 0x00ff;
      return 0;  
    }      
#endif /* SOLARIS */
}

int setPCM()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  value = (mixerSettings.PCM << 8) + mixerSettings.PCM;  

  if (ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_PCM), &value) < 0)
    return -1;
  else
    return 0;  
#endif /* SOLARIS */
}

/*
  Output Gain
*/  

int getOGain()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  if (ioctl(mixer_fd, MIXER_READ(SOUND_MIXER_OGAIN), &value) < 0)
    return -1;
  else
    {
      mixerSettings.oGain = value & 0x00ff;
      return 0;  
    }      
#endif /* SOLARIS */
}

int setOGain()
{
#ifdef DRIVER_SOLARIS
  return -1;
#else /* ! SOLARIS */
  int value;
  
  value = (mixerSettings.oGain << 8) + mixerSettings.oGain;
  
  if (ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_OGAIN), &value) < 0)
    return -1;
  else
    return 0;  
#endif /* SOLARIS */
}

int initializeMixer()
{
#ifdef DRIVER_SOLARIS
  mixer_fd = open("/dev/audioctl", O_RDWR);
#else /* ! SOLARIS */  
  mixer_fd = open("/dev/mixer", O_RDWR);
#endif /* SOLARIS */
  if (mixer_fd < 0)
    {
      fprintf(stderr, "Error, could not open mixer.\n");
      mixerExists = 0;
      mixerOpen = 0;      
      return -1;
    }
  else
    {
      mixerOpen = 1;
      mixerExists = 1;  
      
      getVolume();
      getLineVolume();
      getBass();
      getTreble();
      getPCM();
      getOGain();
      balance = 0;
      
      return 0;      
    }  
}

void closeMixer()
{
  if (mixerExists)
    {
      close(mixer_fd);
      mixerOpen = 0;
    }  
}
