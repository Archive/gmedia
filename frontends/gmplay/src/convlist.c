/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include "gmplay.h"
#include "listutils.h"

configType options;  /* Here to satisfy linker */

const char *albumlist_id_string = "*gmplay_playlist*";
const char *new_albumlist_id_string = "*gmplay_albumlist*";
albumNode *albumList;

void loadAlbums()
{
  albumNode *currentAlbum = NULL;
  songNode *currentSong = NULL;
  
  int stackPos = 0;
  char *name = (char *)malloc(255);
  char *title = (char *)malloc(255);
  char *tempBuffer = (gchar *)malloc(255);
  char buffer;
  
  fgets(tempBuffer, 255, stdin);
  tempBuffer[strlen(tempBuffer)-1] = '\0';
    
  if (strcmp(tempBuffer, albumlist_id_string) != 0)
    return;
    
  while (!feof(stdin) && (stackPos >= 0))
    {
      buffer = fgetc(stdin);
      
      if (buffer == '\\')
        {          
          fgets(name, 255, stdin);
          if (name[0] == '\n')
            continue; 
        
          name[strlen(name)-1] = '\0';
          currentAlbum = newAlbum(name, albumList);
          
          g_list_append(albumList->albums, currentAlbum);
        }
      else if (currentAlbum != NULL)
        {
          ungetc(buffer, stdin);
          fscanf(stdin, "%254[^ \n]", name);
          
          if (feof(stdin))
            continue;
            
          fgets(title, 255, stdin);
          if (strlen(title) > 0)
            {
              title[strlen(title)-1] = '\0';
              currentSong = newSong(name, &title[1], currentAlbum);
            }
          else
            currentSong = newSong(name, NULL, currentAlbum);
            
          g_list_append(currentAlbum->songs, currentSong);
        }  
    }
}

void printAlbums()
{
  GList *currentAlbumNode;
  GList *currentSongNode;
  albumNode *currentAlbum;
  songNode *currentSong;  
 
  printf("%s\n", new_albumlist_id_string);    
  printf("\\%s\n", albumList->title);
  currentAlbumNode = g_list_first(albumList->albums)->next;
  
  while (currentAlbumNode)
    {
      currentAlbum = (albumNode *)(currentAlbumNode->data);
      currentSongNode = g_list_first(currentAlbum->songs)->next;
      
      printf("\\%s\n", currentAlbum->title);
      
      while (currentSongNode)
        {
          currentSong = (songNode *)(currentSongNode->data);
          
          printf("%s\t%s\n", currentSong->fileName, currentSong->title);
          currentSongNode = currentSongNode->next;
        }
        
      printf("\\\n");      
      currentAlbumNode = currentAlbumNode->next;
    } 
    
  printf("\\\n");
}

int main(int argc, char *argv[])
{
  if (argc < 1)
    {
      printf("convert < inputfile > outputfile\n");
      return 0;
    }

  albumList = newAlbum(ROOTNAME, NULL);
  
  loadAlbums();
  printAlbums();    
  
  return 0;
}