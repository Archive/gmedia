/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gtk/gtk.h>
#include <string.h>
#include <gdk_imlib.h>
#include "pixmap.h"
#include "about.h"

void createAboutBox()
{
  static GtkWidget *window = NULL;
  GtkWidget *pixmap;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *vbox;
  char *labelStr = (char *)malloc(strlen(VERSION)+
                                  strlen("GMP ")+
                                  strlen(" by Brett Kosinski"));
  strcpy(labelStr, "GMP ");
  strcat(labelStr, VERSION);
  strcat(labelStr, " by Brett Kosinski");

  if (! window)
    {
      window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      gtk_window_set_title(GTK_WINDOW(window), "About GMPlau");
      gtk_window_set_wmclass(GTK_WINDOW(window), "gmplay.about_gmplay", NULL);
      gtk_widget_realize(window);

      vbox = gtk_vbox_new(FALSE, 0);
      gtk_container_add(GTK_CONTAINER(window), vbox);
      gtk_widget_show(vbox);

      pixmap = load_image("gmplaylogo.jpg", window->window,
                          &window->style->bg[GTK_STATE_NORMAL],
                          NULL, NULL, FALSE);
      gtk_box_pack_start(GTK_BOX(vbox), pixmap, FALSE, FALSE, 0);
      gtk_widget_show(pixmap);

      label = gtk_label_new(labelStr);
      free(labelStr);
      gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
      gtk_widget_show(label);

      button = gtk_button_new_with_label("Close");
      gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
      gtk_widget_show(button);

      gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(window));

      gtk_signal_connect_object(GTK_OBJECT(window), "delete_event",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(window));
      gtk_widget_show(window);
    }
  else
    {
      if (! GTK_WIDGET_VISIBLE(window))
        gtk_widget_show(window);
      else
        return;
    }
}

