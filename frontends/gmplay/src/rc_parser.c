/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include "rc_parser.h"

int parseLine(char *line, tokenInfo *token)
{
  char *buffer = (char *)malloc(strlen(line)+1);
  int pos = 1;
  char *startPos = buffer;

  if (line[0] == '#')
    return 0;
  
  strcpy(buffer, line);
  
  do  
    buffer++;
  while ((buffer) && (buffer[pos-1] != '=') && (buffer[pos] != '\0'));
  
  do
    buffer++;
  while ((buffer) && (buffer[pos-1] == ' ') && (buffer[pos] != '\0'));
   
  pos = strlen(buffer);
  
  while ((buffer[pos] == ' ') || (buffer[pos] == '\n') || (buffer[pos] == '\0'))
    pos--;

  buffer[pos+1] = '\0';  

  if ((token->type == STRING) && (strlen(buffer) < 255))
    strcpy(token->dataBuffer, buffer);
  else if (token->type == INTEGER)
    sscanf(buffer, "%d", (int *)(token->dataBuffer));
  else if (token->type == FLOAT)
    sscanf(buffer, "%f", (float *)(token->dataBuffer));
  else if (token->type == BOOLEAN)
    {
      if (! strcasecmp(buffer, "TRUE"))
        *(int *)(token->dataBuffer) = 1;
      else if (! strcasecmp(buffer, "FALSE"))
        *(int *)(token->dataBuffer) = 0;
      else  
        {
          buffer = startPos;
          free(buffer);
          return -1;
        }  
    }

  buffer = startPos;    
  free(buffer);
  return 0;  
}

void parseFile(char *file, tokenInfo tokens[], int tokenCount)
{
  FILE *inputFile;
  char *lineBuffer = (char *)malloc(255);
  char *name = (char *)malloc(255);
  int startPos, endPos;
  int i;
  
  inputFile = fopen(file, "r"); 

  while (inputFile && ! feof(inputFile))
    {
      startPos = 0;
      endPos = 0;

      fgets(lineBuffer, 255, inputFile);
      strcpy(name, lineBuffer);
      
      while (name[startPos] == ' ')
        startPos++;
      name += startPos;  
      
      while ((name[endPos] != ' ') && (name[endPos] != '=') &&
             (name[endPos] != '\n'))
        endPos++;        
      name[endPos] = '\0';
      
      for (i = 0; i < tokenCount; i++)
        if (! strcasecmp(name, tokens[i].id_string))
          parseLine(lineBuffer, &tokens[i]);
    }
    
  if (inputFile) fclose(inputFile);
}