/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <strings.h>
#include "mixerpanel.h"
#include "mixer.h"
#include "gmplay.h"

void balanceSliderCallback(GtkAdjustment *adjustment, gpointer *data)
{
  balance = adjustment->value;

  if (mixerExists)
    setVolume();
}

void lineVolumeCallback(GtkAdjustment *adjustment, gpointer *data)
{
  mixerSettings.lineVolume = adjustment->value;
  
  if (mixerExists)
    setLineVolume();
}

void bassCallback(GtkAdjustment *adjustment, gpointer *data)
{
  mixerSettings.bass = adjustment->value;
  
  if (mixerExists)
    setBass();
}

void trebleCallback(GtkAdjustment *adjustment, gpointer *data)
{
  mixerSettings.treble = adjustment->value;
  
  if (mixerExists)
    setTreble();
}

void PCMCallback(GtkAdjustment *adjustment, gpointer *data)
{
  mixerSettings.PCM = adjustment->value;
  
  if (mixerExists)
    setPCM();
}

void oGainCallback(GtkAdjustment *adjustment, gpointer *data)
{
  mixerSettings.oGain = adjustment->value;
  
  if (mixerExists)
    setOGain();
}

void destroyMixerPanel(GtkWidget *widget, GtkWidget *mixerWindow)
{
  if (mixerWindow)
    gtk_widget_hide(mixerWindow);
}

void openMixer()
{
//  int myPID = 0;
  char *cmdLine = (char *)malloc(300);
  
  if (options.mixerApp[0] != '\0')
    {
      strcpy(cmdLine, options.mixerApp);
      strcat(cmdLine, " &");
      system(cmdLine);
    }  
  else
    createMixerPanel();    
}

void createMixerPanel()
{
  static GtkWidget *mixerWindow = NULL;
  GtkObject *adjustment;
  GtkWidget *slider;  
  GtkWidget *label;
  GtkWidget *table;
  GtkWidget *box;
  GtkWidget *button;
  
  if (! mixerWindow)
    {
      mixerWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      gtk_widget_set_usize(mixerWindow, 350, 190); 
      
      box = gtk_vbox_new(TRUE, 10);
      gtk_container_add(GTK_CONTAINER(mixerWindow), box);
      gtk_widget_show(box);

      table = gtk_table_new(8, 2, FALSE);
      gtk_box_pack_start(GTK_BOX(box), table, TRUE, TRUE, FALSE);
      gtk_widget_show(table);
      
      /* Balance Adjuster */
      
      label = gtk_label_new("Balance");
      gtk_widget_show(label);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL,
                       5, 5);                                         

      adjustment = gtk_adjustment_new(balance, -101, 102, 1, 5, 1);
      gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                         GTK_SIGNAL_FUNC(balanceSliderCallback), NULL);
      
      slider = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
      gtk_scale_set_draw_value(GTK_SCALE(slider), FALSE);
      gtk_widget_show(slider);
      gtk_table_attach(GTK_TABLE(table), slider, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, 
                       GTK_FILL, 5, 5);

      /* Lineout Volume Adjuster */
      
      label = gtk_label_new("Line Volume");
      gtk_widget_show(label);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL,
                       5, 5);                                         

      adjustment = gtk_adjustment_new(mixerSettings.lineVolume, 0, 101, 1, 5, 1);
      gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                         GTK_SIGNAL_FUNC(lineVolumeCallback), NULL);
      
      slider = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
      gtk_scale_set_draw_value(GTK_SCALE(slider), FALSE);
      gtk_widget_show(slider);
      gtk_table_attach(GTK_TABLE(table), slider, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, 
                       GTK_FILL, 5, 5);

      /* Bass Adjuster */
      
      label = gtk_label_new("Bass");
      gtk_widget_show(label);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3, GTK_FILL, GTK_FILL,
                       5, 5);                                         

      adjustment = gtk_adjustment_new(mixerSettings.bass, 0.0, 101.0, 1.0, 5.0, 1.0);
      gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                         GTK_SIGNAL_FUNC(bassCallback), NULL);
      
      slider = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
      gtk_scale_set_draw_value(GTK_SCALE(slider), FALSE);
      gtk_widget_show(slider);
      gtk_table_attach(GTK_TABLE(table), slider, 1, 2, 2, 3, GTK_EXPAND | GTK_FILL, 
                       GTK_FILL, 5, 5);

      /* Treble Adjuster */
      
      label = gtk_label_new("Treble");
      gtk_widget_show(label);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4, GTK_FILL, GTK_FILL,
                       5, 5);                                         

      adjustment = gtk_adjustment_new(mixerSettings.treble, 0.0, 101.0, 1.0, 5.0, 1.0);
      gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                         GTK_SIGNAL_FUNC(trebleCallback), NULL);
      
      slider = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
      gtk_scale_set_draw_value(GTK_SCALE(slider), FALSE);
      gtk_widget_show(slider);
      gtk_table_attach(GTK_TABLE(table), slider, 1, 2, 3, 4, GTK_EXPAND | GTK_FILL, 
                       GTK_FILL, 5, 5);

      /* PCM Adjuster */
      
      label = gtk_label_new("PCM");
      gtk_widget_show(label);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5, GTK_FILL, GTK_FILL,
                       5, 5);                                         

      adjustment = gtk_adjustment_new(mixerSettings.PCM, 0.0, 101.0, 1.0, 5.0, 1.0);
      gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                         GTK_SIGNAL_FUNC(PCMCallback), NULL);
      
      slider = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
      gtk_scale_set_draw_value(GTK_SCALE(slider), FALSE);
      gtk_widget_show(slider);
      gtk_table_attach(GTK_TABLE(table), slider, 1, 2, 4, 5, GTK_EXPAND | GTK_FILL, 
                       GTK_FILL, 5, 5);

      /* Output Gain Adjuster */
      
      label = gtk_label_new("Output Gain");
      gtk_widget_show(label);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, 6, 7, GTK_FILL, GTK_FILL,
                       5, 5);                                         

      adjustment = gtk_adjustment_new(mixerSettings.oGain, 0.0, 101.0, 1.0, 5.0, 1.0);
      gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                         GTK_SIGNAL_FUNC(oGainCallback), NULL);
      
      slider = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
      gtk_scale_set_draw_value(GTK_SCALE(slider), FALSE);
      gtk_widget_show(slider);
      gtk_table_attach(GTK_TABLE(table), slider, 1, 2, 6, 7, GTK_EXPAND | GTK_FILL, 
                       GTK_FILL, 5, 5);
                       
      button = gtk_button_new_with_label("Close Panel");
      gtk_widget_show(button);
      gtk_table_attach(GTK_TABLE(table), button, 0, 3, 7, 8, GTK_EXPAND | GTK_FILL,
                       GTK_FILL, 5, 5);                       
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(destroyMixerPanel), 
                         mixerWindow);
                       
      gtk_widget_show(mixerWindow);                       
    }
  else  
    {
      if (GTK_WIDGET_VISIBLE(mixerWindow))
        return;
      else
        gtk_widget_show(mixerWindow);  
    }
}
