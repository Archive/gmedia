/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "pixmap.h"
#include "xpmbutton.h"

#define DEFIMAGE 0
#define FOCUSIMAGE 1
#define PRESSIMAGE 2

enum {
  CLICKED,
  LAST_SIGNAL
};

enum {
  ARG_0
};

static guint button_signals[LAST_SIGNAL] = { 0 };

static void xpm_button_class_init (XpmButtonClass *klass);
static void xpm_button_init (XpmButton *button);
static gint xpm_button_button_pressed(GtkWidget *widget, GdkEventButton *event);
static gint xpm_button_button_released(GtkWidget *widget, GdkEventButton *event);
static gint xpm_button_enter_notify(GtkWidget *widget, GdkEventCrossing *event);
static gint xpm_button_leave_notify(GtkWidget *widget, GdkEventCrossing *event);
static gint xpm_button_expose(GtkWidget *widget, GdkEventExpose *event);

guint xpm_button_get_type()
{
  static guint xpm_button_type = 0;

  if (! xpm_button_type)
    {
      GtkTypeInfo xpm_button_info =
      {
        "XpmButton",
        sizeof (XpmButton),
        sizeof (XpmButtonClass),
        (GtkClassInitFunc) xpm_button_class_init,
        (GtkObjectInitFunc) xpm_button_init,
        (GtkArgSetFunc) NULL,
        (GtkArgSetFunc) NULL
      };

      xpm_button_type = gtk_type_unique(gtk_drawing_area_get_type(), &xpm_button_info);
    }

  return xpm_button_type;
}

static void xpm_button_class_init(XpmButtonClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass *) klass;
  widget_class = (GtkWidgetClass *) klass;

  button_signals[CLICKED] =
    gtk_signal_new("clicked",
                   GTK_RUN_FIRST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET(XpmButtonClass, clicked),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals(object_class, button_signals, LAST_SIGNAL);

  widget_class->activate_signal = button_signals[CLICKED];
  widget_class->button_press_event = xpm_button_button_pressed;
  widget_class->button_release_event = xpm_button_button_released;
  widget_class->enter_notify_event = xpm_button_enter_notify; 
  widget_class->leave_notify_event = xpm_button_leave_notify;
  widget_class->expose_event = xpm_button_expose;

  klass->clicked = NULL;
}

static void xpm_button_init(XpmButton *button)
{
  int i;

  for (i = 0; i < 3; i++)
    button->images[i] = NULL;

  gtk_widget_set_events(GTK_WIDGET(button), 
                        GDK_EXPOSURE_MASK |
                        GDK_ENTER_NOTIFY_MASK |
                        GDK_LEAVE_NOTIFY_MASK |
                        GDK_BUTTON_PRESS_MASK |
                        GDK_BUTTON_RELEASE_MASK);

  button->in_button = 0;
  button->button_down = 0;
}

GtkWidget *xpm_button_new(gchar *defImage,
                          gchar *focusImage,
                          gchar *pressImage)
{
  XpmButton *newButton;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  int i;

  newButton = XPM_BUTTON(gtk_type_new(xpm_button_get_type()));  

  newButton->images[DEFIMAGE] = 
    load_image(defImage,
               GTK_WIDGET(newButton)->window,
               &GTK_WIDGET(newButton)->style->bg[GTK_STATE_NORMAL],
               &(newButton->w), &(newButton->h), TRUE);

  newButton->images[FOCUSIMAGE] =
    load_image(focusImage,
               GTK_WIDGET(newButton)->window,
               &GTK_WIDGET(newButton)->style->bg[GTK_STATE_NORMAL],
               NULL, NULL, TRUE);

  newButton->images[PRESSIMAGE] =
    load_image(pressImage,
               GTK_WIDGET(newButton)->window,
               &GTK_WIDGET(newButton)->style->bg[GTK_STATE_NORMAL],
               NULL, NULL, TRUE);

  gtk_drawing_area_size(GTK_DRAWING_AREA(newButton),
                        newButton->w, newButton->h);

  for (i = 0; i < 3; i++)
    gtk_widget_show(newButton->images[i]);
    
  gtk_pixmap_get(GTK_PIXMAP(newButton->images[DEFIMAGE]), &pixmap, &mask);
  gtk_widget_shape_combine_mask(GTK_WIDGET(newButton), mask, 0, 0);
  
  return GTK_WIDGET(newButton);
}

void xpm_button_clicked(XpmButton *button)
{
  gtk_signal_emit(GTK_OBJECT(button), button_signals[CLICKED]);
}

static gint xpm_button_button_pressed(GtkWidget *widget, GdkEventButton *event)
{
  XpmButton *button = XPM_BUTTON(widget);
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;

  if (button->in_button && (event->type == GDK_BUTTON_PRESS) && (event->button == 1))
    {
      button = XPM_BUTTON(widget);
      
      gtk_pixmap_get(GTK_PIXMAP(button->images[PRESSIMAGE]),
                     &pixmap, &bitmap);

      gdk_draw_pixmap(widget->window,
                      widget->style->black_gc,
                      pixmap,
                      0, 0, 0, 0,
                      button->w, button->h);

      button->button_down = 1;
      
      return TRUE;
    }

  return FALSE;
}

static gint xpm_button_button_released(GtkWidget *widget, GdkEventButton *event)
{
  XpmButton *button = XPM_BUTTON(widget);
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;

  if (button->button_down && (event->type = GDK_BUTTON_RELEASE) && (event->button == 1))
    {
      gtk_pixmap_get(GTK_PIXMAP(button->images[FOCUSIMAGE]),
                     &pixmap, &bitmap);

      gdk_draw_pixmap(widget->window,
                      widget->style->black_gc,
                      pixmap,
                      0, 0, 0, 0,
                      button->w, button->h);
      
      button->button_down = 0;
      
      if (button->in_button)
        xpm_button_clicked(button);
        
      return TRUE;
    }

  return FALSE;
}

static gint xpm_button_enter_notify(GtkWidget *widget, GdkEventCrossing *event)
{
  XpmButton *button = XPM_BUTTON(widget);
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;

  if (event->detail != GDK_NOTIFY_INFERIOR)
    {
      if (! button->button_down)
        gtk_pixmap_get(GTK_PIXMAP(button->images[FOCUSIMAGE]), &pixmap, &bitmap);
      else
        gtk_pixmap_get(GTK_PIXMAP(button->images[PRESSIMAGE]), &pixmap, &bitmap);
        
      gdk_draw_pixmap(widget->window,
                      widget->style->black_gc,
                      pixmap,
                      0, 0, 0, 0,
                      button->w, button->h);

      button->in_button = 1;
    }

  return FALSE;
}

static gint xpm_button_leave_notify(GtkWidget *widget, GdkEventCrossing *event)
{
  XpmButton *button = XPM_BUTTON(widget);
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;

  if (event->detail != GDK_NOTIFY_INFERIOR)
    {
      gtk_pixmap_get(GTK_PIXMAP(button->images[DEFIMAGE]), &pixmap, &bitmap);

      gdk_draw_pixmap(widget->window,
                      widget->style->black_gc,
                      pixmap,
                      0, 0, 0, 0,
                      button->w, button->h);

      button->in_button = 0;
    }

  return FALSE;
}

static gint xpm_button_expose(GtkWidget *widget, GdkEventExpose *event)
{
  XpmButton *button;
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;
  int pixmapIndex = -1;

  button = XPM_BUTTON(widget);

  if (button->button_down)
    pixmapIndex = PRESSIMAGE;
  else if (button->in_button)
    pixmapIndex = FOCUSIMAGE;
  else
    pixmapIndex = DEFIMAGE;

  gtk_pixmap_get(GTK_PIXMAP(button->images[pixmapIndex]), &pixmap, &bitmap);

  gdk_draw_pixmap(widget->window,
                  widget->style->black_gc,
                  pixmap,
                  0, 0, 0, 0,
                  button->w, button->h);

  return FALSE;
}






