      for (startPos = 0; newFileName[startPos] != '\0'; startPos++)
       if (newFileName[startPos] == '/') offset = startPos;      
  
      for (startPos = offset; (newFileName[startPos] != '\0') && 
                              (newFileName[startPos] != '-'); startPos++);

      if (newFileName[startPos] != '\0')
        {
          startPos++;          
          title = (char *)malloc(strlen(newFileName)-startPos+1);
      
          for (strPos = 0; (strPos <= strlen(newFileName)-startPos); strPos++)
            {
              title[strPos] = newFileName[strPos+startPos];
              if (title[strPos] == '_') title[strPos] = ' ';
            }  
        }
      else
        {      
          title = (char *)malloc(strlen(newFileName)+1);
          strcpy(title, newFileName);
      
          for (strPos = 0; strPos < strlen(title); strPos++)
            if (title[strPos] == '_') title[strPos] = ' ';
