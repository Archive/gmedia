/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __RC_PARSER__
#define __RC_PARSER__

typedef enum {
  STRING,
  INTEGER,
  BOOLEAN,
  FLOAT
} tokenType;

typedef struct {
  char *id_string;
  tokenType type;
  void *dataBuffer;
} tokenInfo;

int parseLine(char *line, tokenInfo *token);
void parseFile(char *file, tokenInfo tokens[], int tokenCount);

#endif