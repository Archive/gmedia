/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "gmplay.h"
#include "list.h"

int randomSelect(int checkFlags)
{
  struct timeval time;
  struct timezone zone;
  int listLength = g_list_length(activePlayList);
  int selected;
  int songsLeft = FALSE;
  GList *selectedNode;
  
  gettimeofday(&time, &zone);
  srand(time.tv_usec);
  
  selectedNode = g_list_first(activePlayList)->next;

  if (checkFlags)
    while (selectedNode)
      {
        if (! ((songNode *)selectedNode->data)->played)
          songsLeft = TRUE;
        
        selectedNode = selectedNode->next;
      }      
  
  if (songsLeft || ! checkFlags)
    {
      selected = rand() % (listLength-1) + 1;
      selectedNode = g_list_nth(activePlayList, selected);      
  
      if (checkFlags) 
        {
          while (((songNode *)selectedNode->data)->played)
            {
              selected = rand() % (listLength-1) + 1;
              selectedNode = g_list_nth(activePlayList, selected);
            }
          ((songNode *)selectedNode->data)->played = TRUE;  
        }  
    
      currentSong = selectedNode;      
      return FALSE;
    }
  else 
    return TRUE;  
}

int nextSong(int checkFlags)
{
  int randomResults = FALSE;
  
  if (g_list_first(activePlayList)->next == NULL)
    return FALSE;
    
  if (loopOne)
    return FALSE;  
    
  if (shuffle)    
    {
      randomResults = randomSelect(checkFlags);
      if (randomResults && loopAll)
        {
          randomResults = FALSE;
          clearPlayedFlags();
        }  
      
      return randomResults;
    }  
  else if ((currentSong == g_list_last(activePlayList)) && (loopAll))
    currentSong = g_list_first(activePlayList);
  else if (currentSong == g_list_last(activePlayList))
    return TRUE;
  else
    currentSong = currentSong->next;
    
  return FALSE;
}

int prevSong(int checkFlags)
{
  int randomResults;

  if (loopOne)
    return FALSE;
  
  if (shuffle)    
    {
      randomResults = randomSelect(checkFlags);
      return randomResults;
    }    
  else if ((currentSong == g_list_first(activePlayList)->next) ||
          (currentSong == activePlayList))
    return TRUE;
  else    
    currentSong = currentSong->prev;
    
  return FALSE;  
}

void play(GtkWidget *widget, configType *options)
{      
  char *cmdLine[CMDLINESIZE];
  int i;
  int optionNum = 0;
  
  if ((! playing) || (doneSong))
    {
      if (g_list_first(activePlayList)->next == NULL)
        return;
        
      if (currentSong == activePlayList)
        currentSong = currentSong->next;
                    
      for (i = 0; i < CMDLINESIZE; i++)
        {
          cmdLine[i] = (char *)malloc(255);
          cmdLine[i][0] = '\0';
        }  
        
    #ifdef __DEBUG__
      g_print("Copying name: %s\n", MPG123_NAME);
    #endif

      strcpy(cmdLine[0], MPG123_NAME);                  
      optionNum++;
      
    #ifdef __DEBUG__
      g_print("Copying buffer: %d\n", options->bufferSize);
    #endif
        
      if (options->bufferSize != 0)
        {
          strcpy(cmdLine[optionNum], "-b");
          sprintf(cmdLine[optionNum+1], "%d", options->bufferSize);
          
          optionNum += 2;
        }
        
    #ifdef __DEBUG__
      g_print("Copying down-sample: %d\n", options->downSample);
    #endif
      
      if (options->downSample != 0)
        {
          sprintf(cmdLine[optionNum], "-%d", options->downSample);
          optionNum++;
        }  
        
    #ifdef __DEBuG__
      g_print("Copying lineout: %d\n", options->downSample);
    #endif
                 
      if (options->lineOut == TRUE)
        {
          strcpy(cmdLine[optionNum], "-ol");
          optionNum++;
        }  
        
    #ifdef __DEBUG__
      g_print("Copying output: %d\n", options->output);
    #endif
        
      if (options->output != STEREO)
        {
          switch (options->output)
            {
              case LEFT : { strcpy(cmdLine[optionNum], "-0"); break; }
              case RIGHT : { strcpy(cmdLine[optionNum], "-1"); break; }
              case MONO : { strcpy(cmdLine[optionNum], "-m"); break; }
            }
            
          optionNum++;                          
        }
        
    #ifdef __DEBUG__
      g_print("Copying filename: %s\n", ((songNode *)(currentSong->data))->fileName);        
    #endif     
            
      strcpy(cmdLine[optionNum], ((songNode *)(currentSong->data))->fileName);
      optionNum++;           
      
      for (i = optionNum; i < CMDLINESIZE; i++)
        free(cmdLine[i]);
        
      cmdLine[optionNum] = NULL;        
      
      mpgPID = fork();
      
      if (mpgPID == 0)
        {        
        #ifndef __DEBUG__
          close(2);        /* Choke off mpg123's output. */
        #endif
                   
          execvp(MPG123_NAME, cmdLine); 
        }
      
      for (i = 0; i < optionNum; i++)
        free(cmdLine[i]);
       
      startSecs = currSecs; 
      
      playing = TRUE;  
      doneSong = FALSE;            
      
      printSongName();
      resetClock();
    } 
}

void pauseSong()
{
  if (playing && ! doneSong)
    {
      if (! paused)
        {
          kill(mpgPID, SIGUSR2);
          paused = TRUE;
        }
      else
        {
          kill(mpgPID, SIGCONT);
          paused = FALSE;
        }  
    }
}

void stop()
{
  if (playing && ! doneSong)
    {      
      if (paused)
        pauseSong();
        
      kill(mpgPID, SIGINT);
      waitpid(mpgPID, NULL, 0);
      playing = FALSE;
      doneSong = TRUE;
      paused = FALSE;      
    }
}

void forward(GtkWidget *widget, configType *options)
{
  if (playing && ! doneSong)
    {
      stop();    
      nextSong(FALSE);
      play(NULL, options);        
    }
  else
    {
      nextSong(FALSE);    
      printSongName();  
      resetClock();
    }  
}

void back(GtkWidget *widget, configType *options)
{
  if (playing && ! doneSong)
    {
      stop(NULL, NULL);
      prevSong(FALSE);
      play(NULL, options);
    }
  else
    {
      prevSong(FALSE);   
      printSongName();
      resetClock();  
    }  
}
