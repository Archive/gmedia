/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __XPMBUTTON_H__
#define __XPMBUTTON_H__

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define XPM_BUTTON(obj) GTK_CHECK_CAST(obj, xpm_button_get_type(), XpmButton)
#define XPM_BUTTON_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, xpm_button_get_type(), XpmButton)
#define IS_XPM_BUTTON(obj) GTK_CHECK_TYPE(obj, xpm_button_get_type())

typedef struct _XpmButton XpmButton;
typedef struct _XpmButtonClass XpmButtonClass;

struct _XpmButton
{
  GtkDrawingArea drawing_area;

  GtkWidget *images[3];

  gint w, h;
  guint in_button : 1;
  guint button_down : 1; 
};

struct _XpmButtonClass
{
  GtkDrawingAreaClass parent_class;

  void (* clicked) (XpmButton *button);
};

guint xpm_button_get_type (void);
GtkWidget* xpm_button_new(gchar *defImage,
                          gchar *focusImage,
                          gchar *pressImage);
void xpm_button_clicked (XpmButton *button);

#ifdef __cplusplus
}
#endif /*__cplusplus */
#endif




