
/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include <gtk/gtk.h>
#include <string.h>
#include "gmplay.h"

GtkFileSelection *autoloadSel;  /* Gawd I hate globals... */
GtkFileSelection *mixerSel;

void toggleVisible(GtkWidget *wid, GtkWidget *widget)
{
  if (! GTK_WIDGET_VISIBLE(widget))
    gtk_widget_show(widget);
  else
    gtk_widget_hide(widget);
}

void bufferEntryCallback(GtkWidget *widget, gpointer *data)
{
  char *entryString;

  entryString = gtk_entry_get_text(GTK_ENTRY(widget));
  if (strlen(entryString) == 0)     
    options.bufferSize = 0;
  else
    sscanf(entryString, "%d", &options.bufferSize);
}

void noBufferCallback(GtkWidget *widget, GtkWidget *entry)
{
  if (GTK_WIDGET_VISIBLE(entry))
    {
      gtk_entry_set_text(GTK_ENTRY(entry), "");    
      gtk_widget_hide(entry);
      options.bufferSize = 0;
    }
  else
    gtk_widget_show(entry);      
}

void lineOutCallback(GtkWidget *widget, gpointer *data)
{
  options.lineOut = ! options.lineOut;
} 

void outputCallback(GtkWidget *widget, int outputType)
{
  if (GTK_TOGGLE_BUTTON(widget)->active)
    options.output = outputType;
}

void downSampleCallback(GtkWidget *widget, int sampleVal)
{
  if (GTK_TOGGLE_BUTTON(widget)->active)
    options.downSample = sampleVal;
}

void borderCallback(GtkWidget *widget, gpointer *data)
{
  options.managed = ! options.managed;
}

void toolTipsCallback(GtkWidget *widget, gpointer *data)
{
  options.toolTips = ! options.toolTips;
}

void autoparseCallback(GtkWidget *widget, gpointer *data)
{
  options.autoparse = ! options.autoparse;
}

void autoloadEntryCallback(GtkWidget *widget, gpointer *data)
{
  char *entryString;

  entryString = gtk_entry_get_text(GTK_ENTRY(widget));
  if (strlen(entryString) == 0)     
    options.playListName[0] = '\0';
  else
    strncpy(options.playListName, entryString, 255);
}

void noAutoloadCallback(GtkWidget *widget, GtkWidget *entry)
{
  gtk_entry_set_text(GTK_ENTRY(entry), "");    
  options.playListName[0] = '\0';
}

void autoloadFileSelected(GtkWidget *widget, GtkEntry *entry)
{
  char *entryString;

  entryString = gtk_file_selection_get_filename(autoloadSel);
  
  if (strlen(entryString) == 0)     
    options.playListName[0] = '\0';
  else
    strncpy(options.playListName, entryString, 255);  
    
  gtk_entry_set_text(entry, options.playListName);
}

void createAutoloadFileSel(GtkWidget *widget, gpointer *entry)
{
  if (!autoloadSel)
    {
      autoloadSel = GTK_FILE_SELECTION(gtk_file_selection_new("Select Playlist"));
      
      gtk_signal_connect(GTK_OBJECT(autoloadSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(autoloadFileSelected), 
                         entry);
      
      gtk_signal_connect_object(GTK_OBJECT(autoloadSel->ok_button), "clicked",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(autoloadSel));                         
      gtk_signal_connect_object(GTK_OBJECT(autoloadSel->cancel_button), "clicked",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(autoloadSel));
      gtk_signal_connect_object(GTK_OBJECT(autoloadSel), "delete_event",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(autoloadSel));                                
                         
      gtk_widget_show(GTK_WIDGET(autoloadSel));
    }
  else
    {
      if (! GTK_WIDGET_VISIBLE(autoloadSel))
        gtk_widget_show(GTK_WIDGET(autoloadSel));
      else
        return;  
    }    
}

void mixerAppEntryCallback(GtkWidget *widget, gpointer *data)
{
  char *entryString;

  entryString = gtk_entry_get_text(GTK_ENTRY(widget));
  if (strlen(entryString) == 0)     
    options.mixerApp[0] = '\0';
  else
    strncpy(options.mixerApp, entryString, 255);
}

void noMixerAppCallback(GtkWidget *widget, GtkWidget *entry)
{
  gtk_entry_set_text(GTK_ENTRY(entry), "");    
  options.mixerApp[0] = '\0';
}

void mixerAppFileSelected(GtkWidget *widget, GtkEntry *entry)
{
  char *entryString;

  entryString = gtk_file_selection_get_filename(mixerSel);
  
  if (strlen(entryString) == 0)     
    options.mixerApp[0] = '\0';
  else
    strncpy(options.mixerApp, entryString, 255);  
    
  gtk_entry_set_text(entry, options.mixerApp);
}

void createMixerAppFileSel(GtkWidget *widget, gpointer *entry)
{
  if (!mixerSel)
    {
      mixerSel = GTK_FILE_SELECTION(gtk_file_selection_new("Select Mixer App"));
      
      gtk_signal_connect(GTK_OBJECT(mixerSel->ok_button), "clicked",
                         GTK_SIGNAL_FUNC(mixerAppFileSelected), 
                         entry);
      
      gtk_signal_connect_object(GTK_OBJECT(mixerSel->ok_button), "clicked",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(mixerSel));                         
      gtk_signal_connect_object(GTK_OBJECT(mixerSel->cancel_button), "clicked",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(mixerSel));
      gtk_signal_connect_object(GTK_OBJECT(mixerSel), "delete_event",
                                GTK_SIGNAL_FUNC(gtk_widget_hide),
                                GTK_OBJECT(mixerSel));                                
                         
      gtk_widget_show(GTK_WIDGET(mixerSel));
    }
  else
    {
      if (! GTK_WIDGET_VISIBLE(mixerSel))
        gtk_widget_show(GTK_WIDGET(mixerSel));
      else
        return;  
    }    
}

void themeEntryCallback(GtkWidget *widget, gpointer *data)
{
  char *entryString;

  entryString = gtk_entry_get_text(GTK_ENTRY(widget));
  if (strlen(entryString) == 0)     
    options.themePack[0] = '\0';
  else
    strncpy(options.themePack, entryString, 255);

}

void autoloadTagsCallback(GtkWidget *widget, gpointer *data)
{
  options.autoloadTags = ! options.autoloadTags;
}

void createOptionsDialog()
{
  static GtkWidget *optionsDialog = NULL;

  GtkWidget *notebook;
  GtkWidget *button;
  GtkWidget *entry;    
  GtkWidget *label;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *mainvbox;
  GtkWidget *separator;
  GtkWidget *fileButton;
  char strBuffer[255];

  if (optionsDialog)
    {
      gtk_widget_show(optionsDialog);
      return;
    } 

  optionsDialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(optionsDialog), "Options");

/* Set up the main dialog vbox */

  mainvbox = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(mainvbox);
  gtk_container_add(GTK_CONTAINER(optionsDialog), mainvbox);
  
/* Okay, set up the notebook */  

  notebook = gtk_notebook_new();
  gtk_widget_show(notebook);
  gtk_box_pack_start(GTK_BOX(mainvbox), notebook, TRUE, TRUE, 0);

/******************************/
/* Set up the first page vbox */
/******************************/

  vbox = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(vbox);

  label = gtk_label_new("Playback Options");
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, label);
      
/* Set up the buffer size widgets. */

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);   

  button = gtk_check_button_new_with_label("Buffer");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(bufferEntryCallback), NULL);
  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(noBufferCallback), entry);
                     
  if (options.bufferSize != 0)
    {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
      gtk_widget_show(entry);
      
      sprintf(strBuffer, "%d", options.bufferSize);
      gtk_entry_set_text(GTK_ENTRY(entry), strBuffer);
    }      
    
/* Now the downsampling stuff */  

  separator = gtk_hseparator_new();
  gtk_widget_show(separator);
  gtk_box_pack_start(GTK_BOX(vbox), separator, TRUE, TRUE, 0);

  label = gtk_label_new("Down Sampling");
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);  

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);   

  button = gtk_radio_button_new_with_label(NULL, "1:1");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.downSample == 0)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(downSampleCallback), (int *)(0));

  button = gtk_radio_button_new_with_label(
           gtk_radio_button_group(GTK_RADIO_BUTTON(button)), "1:2");
           
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.downSample == 2)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(downSampleCallback), (int *)(2));  

  button = gtk_radio_button_new_with_label(
           gtk_radio_button_group(GTK_RADIO_BUTTON(button)), "1:4");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.downSample == 4)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(downSampleCallback), (int *)(4));  
                     
/* Lineout checkbox */

  separator = gtk_hseparator_new();
  gtk_widget_show(separator);
  gtk_box_pack_start(GTK_BOX(vbox), separator, TRUE, TRUE, 0);

  label = gtk_label_new("Output Target");
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);  

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);   
                         
  button = gtk_check_button_new_with_label("Output to Lineout");  
  
  if (options.lineOut)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

  gtk_widget_show(button);  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(lineOutCallback), NULL);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
/* Output type */

  separator = gtk_hseparator_new();
  gtk_widget_show(separator);
  gtk_box_pack_start(GTK_BOX(vbox), separator, TRUE, TRUE, 0);

  label = gtk_label_new("Output Type");
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);  

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);   

  button = gtk_radio_button_new_with_label(NULL, "Left");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.output == LEFT)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(outputCallback), (int *)(LEFT));

  button = gtk_radio_button_new_with_label(
           gtk_radio_button_group(GTK_RADIO_BUTTON(button)), "Right");
           
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.output == RIGHT)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(outputCallback), (int *)(RIGHT));  

  button = gtk_radio_button_new_with_label(
           gtk_radio_button_group(GTK_RADIO_BUTTON(button)), "Stereo");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.output == STEREO)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(outputCallback), (int *)(STEREO));  

  button = gtk_radio_button_new_with_label(
           gtk_radio_button_group(GTK_RADIO_BUTTON(button)), "Mono");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  if (options.output == MONO)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);
    
  gtk_signal_connect(GTK_OBJECT(button), "toggled",
                     GTK_SIGNAL_FUNC(outputCallback), (int *)(MONO));  

/**************************/
/* Second page vbox setup */
/**************************/

  label = gtk_label_new("GMP3 Options");
  gtk_widget_show(label);
  
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(vbox);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, label);  
  
/* Themepack entry widget */
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  
  label = gtk_label_new("Theme");
  gtk_misc_set_alignment(GTK_MISC(label), 0.1, 0.5);
  gtk_widget_show(label);  
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);  

  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);

  strcpy(strBuffer, options.themePack);
  gtk_entry_set_text(GTK_ENTRY(entry), strBuffer);   
      
  gtk_widget_show(entry);
    
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(themeEntryCallback), NULL);    
  
/* Playlist Autoload Gizmos */
  
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

  button = gtk_check_button_new_with_label("Autoload");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
 
  fileButton = gtk_button_new_with_label("...");
  gtk_box_pack_start(GTK_BOX(hbox), fileButton, TRUE, TRUE, 0);
  
  if (options.playListName[0] != '\0')
    {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

      strcpy(strBuffer, options.playListName);
      gtk_entry_set_text(GTK_ENTRY(entry), strBuffer);   
      
      gtk_widget_show(entry);
      gtk_widget_show(fileButton);
    }          
      
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(autoloadEntryCallback), NULL);
                       
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(noAutoloadCallback), entry);                     
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(toggleVisible), entry);                     
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(toggleVisible), fileButton);
                     
  gtk_signal_connect(GTK_OBJECT(fileButton), "clicked",
                     GTK_SIGNAL_FUNC(createAutoloadFileSel), entry);

/* External Mixer App Gizmos */

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

  button = gtk_check_button_new_with_label("Ext. Mixer");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  
  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
 
  fileButton = gtk_button_new_with_label("...");
  gtk_box_pack_start(GTK_BOX(hbox), fileButton, TRUE, TRUE, 0);
  
  if (options.mixerApp[0] != '\0')
    {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

      strcpy(strBuffer, options.mixerApp);
      gtk_entry_set_text(GTK_ENTRY(entry), strBuffer);   
      
      gtk_widget_show(entry);
      gtk_widget_show(fileButton);
    }          
      
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(mixerAppEntryCallback), NULL);
                       
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(noMixerAppCallback), entry);                     
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(toggleVisible), entry);                     
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(toggleVisible), fileButton);
                     
  gtk_signal_connect(GTK_OBJECT(fileButton), "clicked",
                     GTK_SIGNAL_FUNC(createMixerAppFileSel), entry);

/* Border checkbox */

  button = gtk_check_button_new_with_label("Window is managed");  
  
  if (options.managed)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

  gtk_widget_show(button);  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(borderCallback), NULL);
  gtk_box_pack_start(GTK_BOX(vbox), button, TRUE, TRUE, 0);
  
/* Tooltips Stuff */

  button = gtk_check_button_new_with_label("Enable Tooltips");  
  
  if (options.toolTips)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

  gtk_widget_show(button);  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(toolTipsCallback), NULL);
  gtk_box_pack_start(GTK_BOX(vbox), button, TRUE, TRUE, 0);

/* Autoparsing Toggle */

  button = gtk_check_button_new_with_label("Enable Autoparsing");  
  
  if (options.autoparse)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

  gtk_widget_show(button);  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(autoparseCallback), NULL);
  gtk_box_pack_start(GTK_BOX(vbox), button, TRUE, TRUE, 0);    

/* Autoload Tags Toggle */

  button = gtk_check_button_new_with_label("Enable ID3 Tag Autoloading");  
  
  if (options.autoloadTags)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), TRUE);

  gtk_widget_show(button);  
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(autoloadTagsCallback), NULL);
  gtk_box_pack_start(GTK_BOX(vbox), button, TRUE, TRUE, 0);    

/* Control Buttons */

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(mainvbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);    
  
  button = gtk_button_new_with_label("Save");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(saveRCFile), NULL);
  
  button = gtk_button_new_with_label("Close");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            GTK_SIGNAL_FUNC(gtk_widget_hide),
                            GTK_OBJECT(optionsDialog));
                            
  gtk_signal_connect_object(GTK_OBJECT(optionsDialog), "delete_event",
                            GTK_SIGNAL_FUNC(gtk_widget_hide),
                            GTK_OBJECT(optionsDialog));                            
                                          
  gtk_widget_show(optionsDialog);
}
