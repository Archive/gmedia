/* GMP3 - A front end for mpg123
 * Copyright (C) 1998 Brett Kosinski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include "handlers.h"

GtkButtonHandler clist_button_press = NULL;
GtkButtonHandler clist_button_release = NULL;

GtkButtonHandler tree_button_press = NULL;
GtkButtonHandler tree_button_release = NULL;

/************************
  Handler Insertion Code 
 ************************/

void insertCListHandlers(GtkWidget *list)
{
  GtkWidgetClass *class;

  class = GTK_WIDGET_CLASS(GTK_OBJECT(list)->klass);
  clist_button_press = (GtkButtonHandler)(class->button_press_event);
  clist_button_release = (GtkButtonHandler)(class->button_release_event);
  
  class->button_press_event = new_clist_button_press;
  class->button_release_event = new_clist_button_release;
}

void insertTreeHandlers(GtkWidget *tree)
{
  GtkWidgetClass *class;
  
  class = GTK_WIDGET_CLASS(GTK_OBJECT(tree)->klass);
  
  tree_button_press = (GtkButtonHandler)(class->button_press_event);
  tree_button_release = (GtkButtonHandler)(class->button_release_event);
  
  class->button_press_event = new_tree_button_press;
  class->button_release_event = new_tree_button_release;
}

/*****************************
  Replacement Signal Handlers 
 *****************************/

gint new_clist_button_press(GtkWidget *widget, GdkEventButton *event)
{
  gint result;

  if (clist_button_press && (event->button == 1))
    result = clist_button_press(widget, event);
  else
    result = FALSE;
    
  return result;  
}

gint new_clist_button_release(GtkWidget *widget, GdkEventButton *event)
{
  gint result;

  if (clist_button_release && (event->button == 1))
    result = clist_button_release(widget, event);
  else
    result = FALSE;
    
  return result;
}

gint new_tree_button_press(GtkWidget *widget, GdkEventButton *event)
{
  gint result;

  if (tree_button_press && (event->button == 1))
    result = tree_button_press(widget, event);
  else
    result = FALSE;
    
  return result;  
}

gint new_tree_button_release(GtkWidget *widget, GdkEventButton *event)
{
  gint result;

  if (tree_button_release && (event->button == 1))
    result = tree_button_release(widget, event);
  else
    result = FALSE;
    
  return result;
}
