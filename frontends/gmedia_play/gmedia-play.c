/* gmedia - The GMedia library
   Copyright (C) 1998 Tristan Tarrant
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

*/

#include <config.h>
#include <libintl.h>
#include <glib.h>
#include "libgmedia/gmedia.h"

#define _(String) gettext (String)
#ifdef gettext_noop
# define N_(String) gettext_noop (String)
#else
# define N_(String) (String)
#endif

char *progname;
int  *mfiles;

void print_driver_list ()
{
  int i;
  gchar **drivers;

  gmedia_get_driver_list (&drivers);
  printf(_("Available drivers: "));
  for(i=0; drivers[i]; i++) {
    if(i>0)
      printf(", ");
    printf("%s", drivers[i]);
  }
  printf("\n");
}

void print_info (gpointer handle)
{
  GMediaDriverInfo gmdi;

  gmedia_get_driver_info ( gmedia_get_driver_name (handle), &gmdi );
  printf(_("Driver Name: %s\n"), gmdi.name);
  printf(_("Driver Description: %s\n"), gmdi.full_name);
  printf(_("Driver Version %d.%d\n"), gmdi.version_major, gmdi.version_minor);
}

void version ()
{
  printf(_("GMedia Player v"));
  printf(VERSION "\n");
}

void help ()
{
  printf(_("Usage: %s [-t paths] [options] [filename ...]\n"), progname);
  printf(_("Plays a file using the available gmedia backends\n\n"));
  printf(_("  -h\t\tThis help page\n"));
  printf(_("  -l\t\tList available drivers\n"));
  printf(_("  -d driver\tUse a specific driver\n"));
  printf(_("  -i\t\tPrint info on the currently selected driver\n"));
  printf(_("  -v\t\tPrint version information\n"));
  printf(_("  -t\t\tRun in development mode (must be 1st arg)\n"));
}

int main (int argc, char **argv)
{
  gpointer handle;
  int res, currarg, info, i;
  char *fname;
  int  *mfiles;
  int  nfiles;
  char *search_path;
  
  search_path = NULL;
  progname = argv[0];

  if(argc<2) {
    help ();
    exit (1);
  }

  currarg = 1;
  if(!strcmp(argv[1],"-t")) {
    if(argc<3) {
      help ();
      exit (1);
    }
    search_path = argv[2];
    currarg+=2;
  }
     
  res = gmedia_init (search_path);
  if(res!=GMEDIA_STATUS_GOOD) {
    if(res==GMEDIA_STATUS_NOT_FOUND)
      printf (_("%s: no gmedia drivers available\n"), progname);
    else
      printf (_("%s: could not initialize gmedia\n"), progname);
    
    exit (1);  
  }
  fname = 0;
  info = 0;
  nfiles = 0;
  mfiles = (int *)malloc(sizeof(int)*argc);
  while(currarg<argc) {
    if(!strcmp(argv[currarg],"-v")) {
      version ();
      ++currarg;
      continue;
    }
    if(!strcmp(argv[currarg],"-h")) {
      help ();
      ++currarg;
      continue;
    }
    if(!strcmp(argv[currarg],"-l")) {
      print_driver_list ();
      ++currarg;
      continue;
    }
    if(!strcmp(argv[currarg],"-i")) {
      info = 1;
      ++currarg;
      continue;
    }
    if(!strcmp(argv[currarg],"-t")) {
      printf("%s\n", argv[0]);
      ++currarg;
      continue;
    }
    /* It must be a filename */
    mfiles[nfiles++] = currarg++;
  }
  for(i=0;i<nfiles;i++) {
    fname = (char *) argv[mfiles[i]];
    gmedia_load (&handle, fname);
    if(!handle) {
      printf(_("Unknown file type for file %s\n"), fname);
      continue;
    }
    if(info)
      print_info (handle);
    gmedia_play (handle);
    printf(_("Track name : %s\n"), gmedia_get_track_name(handle,1));
    
    gmedia_wait (handle);
  }
  gmedia_exit ();
}
