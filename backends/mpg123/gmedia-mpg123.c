#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <stdarg.h>
#include <stdio.h>
#include <signal.h>

#include "libgmedia/gmedia.h"

#ifndef MAXPATH
#define MAXPATH 1024
#endif

#define DBG(msg, args...)	gmedia_dbg_printf (msg, ##args);

static struct mpg123_data_t {
  int loaded;
  GMediaMode playing_mode;
  gchar url[MAXPATH];
  int pid;
} mpg123_data;

static int debugging = -1;

void gmedia_dbg_printf(const char *fmt, ...)
{
  va_list ap;
  if(debugging<0)
    debugging = (getenv("DEBUG")!=NULL);
  if(debugging) {
    fprintf(stderr,"libgmedia_mpg123: ");
    va_start (ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end (ap);
  }
}

int gmedia_mpg123_init()
{
  
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_exit()
{
  DBG("-- mpg123_exit\n");
  if(mpg123_data.loaded)
    gmedia_mpg123_eject ();
}

int gmedia_mpg123_load(gchar *url)
{
  strcpy(mpg123_data.url,url);
  mpg123_data.loaded = 1;
  mpg123_data.playing_mode = GMEDIA_MODE_STOPPED;
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_play()
{
  int pid, status;
  
  if(!mpg123_data.loaded)
    return GMEDIA_STATUS_INVAL;
  
  if(mpg123_data.playing_mode!=GMEDIA_MODE_STOPPED)
    return GMEDIA_STATUS_INVAL;
  
  if ((pid = fork ())!=0) {
    mpg123_data.pid = pid;
    mpg123_data.playing_mode = GMEDIA_MODE_PLAYING;
    return GMEDIA_STATUS_GOOD;
  } else {	/* Player */
    char *cmdLine[4];
    
    cmdLine[0] = "mpg123";
    cmdLine[1] = "-v";
    cmdLine[2] = mpg123_data.url;
    cmdLine[3] = NULL;

    close(2);
    execvp("mpg123", cmdLine);
    
    _exit(0);
  }

  return -1;
}

int gmedia_mpg123_pause()
{
  switch(mpg123_data.playing_mode) {
  case GMEDIA_MODE_PLAYING:
    kill(mpg123_data.pid, SIGUSR2);
    mpg123_data.playing_mode = GMEDIA_MODE_PAUSED;
    break;
  case GMEDIA_MODE_PAUSED:
    kill(mpg123_data.pid, SIGCONT);
    mpg123_data.playing_mode = GMEDIA_MODE_PLAYING;
    break;
  default:
    break;
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_stop()
{
  DBG("-- mpg123_stop()\n");
  if(mpg123_data.playing_mode!=GMEDIA_MODE_STOPPED) {
    kill(mpg123_data.pid, SIGINT);
    gmedia_mpg123_wait();
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_wait()
{
  int status;

  DBG("-- mpg123_wait()\n");
  if(mpg123_data.pid>0)
    waitpid(mpg123_data.pid, &status, 0);
  mpg123_data.playing_mode = GMEDIA_MODE_STOPPED;
  mpg123_data.pid = 0;
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_forward()
{
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_rewind()
{
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_restart ()
{
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_eject()
{
  DBG("-- mpg123_eject()\n");
  if(mpg123_data.loaded) {
    DBG("---- loaded\n");
    if(mpg123_data.playing_mode!=GMEDIA_MODE_STOPPED) {
      DBG("---- stopping\n");
      gmedia_mpg123_stop();
    }
    mpg123_data.loaded = 0;
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_mpg123_get_n_frames()
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_mpg123_set_frame()
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_mpg123_get_frame()
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_mpg123_get_n_tracks ()
{
  return 1; /* Only one track available here */
}

int gmedia_mpg123_set_track (int ntrack)
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_mpg123_get_track ()
{
  if(mpg123_data.playing_mode==GMEDIA_MODE_STOPPED)
    return 0; /* Return 0 if we're not playing anything */
  else
    return 1; /* Otherwise we're on track 1 */
}

int gmedia_mpg123_check(gchar *url)
{
  if(strstr(url,".mp3"))
    return GMEDIA_STATUS_GOOD;
  else
    return GMEDIA_STATUS_UNSUPPORTED;
}

const gchar *gmedia_mpg123_get_media_name ()
{
  if(mpg123_data.loaded)
    return "MPG123 file";
  else
    return "";
}

const gchar *gmedia_mpg123_get_track_name (int ntrack)
{
  return "Yo!";
}

int gmedia_mpg123_is_streamable ()
{
  return FALSE;
}

int gmedia_mpg123_needs_drawable ()
{
  return FALSE;
}

int gmedia_mpg123_get_fd (int *fd)
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

GMediaMode gmedia_mpg123_get_mode ()
{
  return mpg123_data.playing_mode;
}

int gmedia_mpg123_get_num_parameters ()
{
  return 1;
}

int gmedia_mpg123_get_parameter (int n, GMediaValue *value)
{
  switch (n) {
  case 0:
    value->name = "Test";
    value->description = "A test parameter";
    value->type = GMEDIA_TYPE_BOOL;
    value->val.b = TRUE;
     return GMEDIA_STATUS_GOOD;
    break;
  default:
    return GMEDIA_STATUS_INVAL;
  }
  return GMEDIA_STATUS_INVAL;
}

int gmedia_mpg123_get_driver_info (GMediaDriverInfo *info)
{
  info->name = "MPG123";
  info->full_name = "MPEG Layer 3 GMedia driver";
  info->version_major = 0;
  info->version_minor = 1;
}
