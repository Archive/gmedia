#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>

#include "libgmedia/gmedia.h"

#ifndef MAXPATH
#define MAXPATH 1024
#endif

/* Shared memory stuff */
static int shmid;
static int pfd1[2], pfd2[2];
static struct sample_data_t {
  int loaded;
  GMediaMode pending_mode;
  GMediaMode playing_mode;
  time_t playing_time;
  gchar url[MAXPATH];
} *sample_data = 0;

/* A few useful IPC functions nicked from XMP */
static int wait_parent ()
{
  char c;
  
  return !((read(pfd1[0],&c,1)==1)&&(c=='p'));
}

static int wait_child ()
{
  char c;

  return !((read(pfd2[0],&c,1)==1)&&(c=='c'));
}

static int tell_parent ()
{
  return write (pfd2[1], "c", 1) != 1;
}

static int tell_child ()
{
  return write (pfd1[1], "p", 1) != 1;
}

static int wait_timeout (int fd, int msec)
{
  fd_set rfds;
  struct timeval tv;

  tv.tv_sec = msec /1000;
  tv.tv_usec = (msec % 1000) * 1000;
  FD_ZERO (&rfds);
  FD_SET (fd, &rfds);

  return select(fd+1,&rfds,NULL,NULL,&tv);
}

static int wait_parent_timeout (int msec)
{
  return wait_timeout (pfd1[0],msec);
}

static int wait_child_timeout (int msec)
{
  return wait_timeout (pfd2[0],msec);
}

/* The initialization function. Because a gmedia DLL should be asynchronous,
   it should setup IPC mechanisms for communication between the processes.
   Obviously everything is much easier if you use threads */
int gmedia_sample_init()
{
  int status;

  /* Prepare data structures and other amenities */
  shmid = shmget(IPC_PRIVATE, sizeof(struct sample_data_t),
		 IPC_CREAT | 0600);
  if (shmid==-1)
    return GMEDIA_STATUS_ERROR;
  sample_data = shmat(shmid,0,0);
  memset(sample_data,0,sizeof(struct sample_data_t));

  sample_data->playing_mode = GMEDIA_MODE_STOPPED;
  sample_data->pending_mode = GMEDIA_MODE_UNAVAILABLE;
  sample_data->playing_time = 0;
  
  status = pipe(pfd1) || pipe(pfd2);

  return status;
}

int gmedia_sample_exit()
{
  if(sample_data->loaded)
    gmedia_sample_eject ();

  
  /* Nuke stuff allocated at initialization time */
  shmctl (shmid, IPC_RMID, NULL);
  shmdt((void *)sample_data);
  /* Close the pipes */
  close(pfd1[0]);
  close(pfd1[1]);
  close(pfd2[0]);
  close(pfd2[1]);
}

int gmedia_sample_load(gchar *url)
{
  strcpy(sample_data->url,url);
  sample_data->loaded = 1;
  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_play()
{
  int pid, status;

  if(!sample_data->loaded)
    return GMEDIA_STATUS_INVAL;

  if(sample_data->playing_mode!=GMEDIA_MODE_STOPPED)
    return GMEDIA_STATUS_INVAL;

  /* Fork the process */
  pid = fork();
  if(pid) {
    sample_data->pending_mode = GMEDIA_MODE_PLAYING;
    tell_child ();
    wait_child ();
    return GMEDIA_STATUS_GOOD;
  } else {	/* Player */
    do {
      if(wait_parent_timeout (125)) {
	wait_parent();
	sample_data->playing_mode = sample_data->pending_mode;
	sample_data->pending_mode = GMEDIA_MODE_UNAVAILABLE;
	tell_parent ();
      }
      /* Do something useful... */
      if(sample_data->playing_mode==GMEDIA_MODE_PLAYING)
	sample_data->playing_time++;
    } while(sample_data->playing_mode!=GMEDIA_MODE_STOPPED);
    sample_data->playing_time = 0;
    _exit (0);
  }
  return -1;
}

int gmedia_sample_pause()
{
  switch(sample_data->playing_mode) {
  case GMEDIA_MODE_PLAYING:
    sample_data->pending_mode = GMEDIA_MODE_PAUSED;
    tell_child ();
    wait_child ();
    break;
  case GMEDIA_MODE_PAUSED:
    sample_data->pending_mode = GMEDIA_MODE_PLAYING;
    tell_child ();
    wait_child ();
    break;
  default:
    break;
  }

  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_stop()
{
  if(sample_data->playing_mode!=GMEDIA_MODE_STOPPED) {
    sample_data->pending_mode = GMEDIA_MODE_STOPPED;
    tell_child ();
    wait_child ();
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_forward()
{
  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_rewind()
{
  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_restart ()
{
  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_eject()
{
  if(sample_data->loaded) {
    if(sample_data->playing_mode!=GMEDIA_MODE_STOPPED)
      gmedia_sample_stop();
    sample_data->loaded = 0;
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_sample_get_n_frames()
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_sample_set_frame()
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_sample_get_frame()
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_sample_get_n_tracks ()
{
  return 1; /* Only one track available here */
}

int gmedia_sample_set_track (int ntrack)
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_sample_get_track ()
{
  if(sample_data->playing_mode==GMEDIA_MODE_STOPPED)
    return 0; /* Return 0 if we're not playing anything */
  else
    return 1; /* Otherwise we're on track 1 */
}

int gmedia_sample_check(gchar *url)
{
  if(strstr(url,".c"))
    return GMEDIA_STATUS_GOOD;
  else
    return GMEDIA_STATUS_UNSUPPORTED;
}

const gchar *gmedia_sample_get_media_name ()
{
  return "C File";
}

const gchar *gmedia_sample_get_track_name (int ntrack)
{
  if (ntrack==1)
    return "Cool track";
  else
    return "No track";
}

int gmedia_sample_is_streamable ()
{
  return FALSE;
}

int gmedia_sample_needs_drawable ()
{
  return FALSE;
}

int gmedia_sample_get_fd (int *fd)
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

GMediaMode gmedia_sample_get_mode ()
{
  return sample_data->playing_mode;
}

int gmedia_sample_get_num_parameters ()
{
  return 1;
}

int gmedia_sample_get_parameter (int n, GMediaValue *value)
{
  switch (n) {
  case 0:
    value->name = "Test";
    value->description = "A test parameter";
    value->type = GMEDIA_TYPE_BOOL;
    value->val.b = TRUE;
     return GMEDIA_STATUS_GOOD;
    break;
  default:
    return GMEDIA_STATUS_INVAL;
  }
  return GMEDIA_STATUS_INVAL;
}

int gmedia_sample_get_driver_info (GMediaDriverInfo *info)
{
  info->name = "Sample";
  info->full_name = "Sample GMedia driver";
  info->version_major = 0;
  info->version_minor = 1;
}

