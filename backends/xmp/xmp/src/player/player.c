/* Extended Module Player - player.c
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "xmpi.h"
#include "xxm.h"
#include "effects.h"
#include "player.h"
#include "driver.h"
#include "period.h"

extern struct xmp_drv_info *drv;

static int vol_base;
static int *vol_xlat;

static int global_flags = 0;
static int row_cnt;
static int loop_row;
static int loop_cnt;
static uint8 *row_ptr;
static uint8 *loop_ptr;
static uint8 *loop_stack;
static int jump_pattern = -1;
static int delay_pattern = 0;
static int jump_line = 0;
static double tick_duration;
static double playing_time;

int __bpm;
int __pause;
struct xmp_chctl __xctl[32];
struct xmp_ord_info xmpi_oinfo[256];

static struct xmp_channel xc[32];
static int tempo;
static int global_vslide;
static int offset = 0;
static double next_time = 0;


/* Vibrato/tremolo waveform tables */
static int waveform[4][64] = {
   {   0,  24,  49,  74,  97, 120, 141, 161, 180, 197, 212, 224,
     235, 244, 250, 253, 255, 253, 250, 244, 235, 224, 212, 197,
     180, 161, 141, 120,  97,  74,  49,  24,   0, -24, -49, -74,
     -97,-120,-141,-161,-180,-197,-212,-224,-235,-244,-250,-253,
    -255,-253,-250,-244,-235,-224,-212,-197,-180,-161,-141,-120,
     -97, -74, -49, -24  },	/* Sine */

   {   0,  -8, -16, -24, -32, -40, -48, -56, -64, -72, -80, -88,
     -96,-104,-112,-120,-128,-136,-144,-152,-160,-168,-176,-184,
    -192,-200,-208,-216,-224,-232,-240,-248, 255, 248, 240, 232,
     224, 216, 208, 200, 192, 184, 176, 168, 160, 152, 144, 136,
     128, 120, 112, 104,  96,  88,  80,  72,  64,  56,  48,  40,
      32,  24,  16,   8  },	/* Ramp down */

   { 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255,-255,-255,-255,-255,
    -255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,
    -255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,
    -255,-255,-255,-255  },	/* Square */

   {   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,
      96, 104, 112, 120, 128, 136, 144, 152, 160, 168, 176, 184,
     192, 200, 208, 216, 224, 232, 240, 248,-255,-248,-240,-232,
    -224,-216,-208,-200,-192,-184,-176,-168,-160,-152,-144,-136,
    -128,-120,-112,-104, -96, -88, -80, -72, -64, -56, -48, -40,
     -32, -24, -16,  -8  }	/* Ramp up */
};



struct xxm_header *xxh;
struct xxm_pattern **xxp;		/* Patterns */
struct xxm_track **xxt;			/* Tracks */
struct xxm_channel xxc[32];		/* Channel info */
uint8 xxo[256];				/* Orders */
struct xxm_instrument_header *xxih;	/* Instrument headers */
struct xxm_instrument_map *xxim;	/* Instrument map */
struct xxm_instrument **xxi;		/* Instruments */
struct xxm_sample *xxs;			/* Samples */
uint16 **xxae;				/* Amplitude envelope */
uint16 **xxpe;				/* Pan envelope */
uint16 **xxfe;				/* Pitch envelope */

struct xmp_options opt;
char module_name[MODULE_NAME_MAXSIZE];

int __current_ord;
int __global_vol;


#include "effects.c"


static void dummy ()
{
    /* dummy */
}


static int get_envelope (uint16 * env, uint8 p, uint16 x)
{
    uint16 x1, x2;
    short y1, y2;

    p--;
    p <<= 1;

    if ((x1 = env[p]) <= x)
	return env[p + 1];

    do {
	p -= 2;
	x1 = env[p];
    } while ((x1 > x) && p);

    y1 = env[p + 1];
    x2 = env[p + 2];
    y2 = env[p + 3];

    return ((y2 - y1) * (x - x1) / (x2 - x1)) + y1;
}


static void fetch_and_play (int t, struct xxm_event *e,
    struct xmp_channel *xc, struct xmp_chctl *xctl, int ch)
{
    uint8 note = 0;
    uint32 finalvol;
    uint16 vol_envelope;
    int finalpan, pan_envelope, vol2;

    if (!t) {
	/* Reset values */
	xc->flags = 0;
	if (!ch)
	    RESET_FLAG (global_flags, GLOBAL_VSLIDE);

	xc->delay = xc->retrig = 0;
	xc->a_idx = xc->a_val[1] = xc->a_val[2] = 0;

	if (e->ins > xxh->ins)
	    return;

	note = e->note;

	if (e->ins) {
	    SET (NEW_INS);
	    xc->oldins = xc->ins;
	    xc->ins = e->ins - 1;
	}

	if (note) {
	    if (note == 0x61)
		xc->release = 1;
	    if ((xc->ins != -1) && (note < 0x61) && XXIH.nsm) {
		SET (NEW_NOTE);
		note--;
		xc->s_end = note_to_period (
		    note + XXI[XXIM.ins[note]].xpo,
		    XXI[XXIM.ins[note]].fin,
		    xxh->flg & XXM_FLG_LINEAR);
	    } else
		RESET (NEW_INS);
	}

	if (e->vol) {		/* Volume set */
	    SET (NEW_VOL);
	    xc->volume = e->vol - 1;
	}

	/* Secondary effect is processed _first_ and can be overriden
	 * by the primary effect.
	 */
	process_fx (note, e->f2t, e->f2p, xc);
	process_fx (note, e->fxt, e->fxp, xc);

	/* Process note */
	if (TEST (NEW_NOTE)) {
	    xc->key = note;
	    xc->note = note + XXI[XXIM.ins[note]].xpo;
	    if (!TEST (FINETUNE))
		xc->finetune = XXI[XXIM.ins[note]].fin;
	    xc->period = note_to_period (xc->note, xc->finetune,
		xxh->flg & XXM_FLG_LINEAR);
	}

	/* Process instrument */
	if (TEST (NEW_INS) && !TEST (NEW_VOL))
	    xc->volume = XXI[XXIM.ins[xc->key]].vol;

	if (TEST (NEW_NOTE | NEW_INS)) {
	    xc->p_idx = xc->v_idx = xc->insvib_idx = xc->release = 0;
	    xc->fadeout = 0x1fff;
	    xc->insvib_idx = 0;
	    xc->insvib_swp = XXI->vsw;
	}

	if (TEST (TONEPORTA))
	    RESET (NEW_INS);

	/* Fadeout */
	if (xc->release)
	    xc->fadeout = (XXIH.aei.flg & XXM_ENV_ON) ?
		((xc->fadeout > XXIH.rls) ?  xc->fadeout - XXIH.rls : 0) : 0;

	/* Process "fine" effects */
	if (TEST (FINE_VOLS))
	    xc->volume += xc->v_fval;

	if (TEST (FINE_BEND))
	    xc->period = (4 * xc->period + xc->f_fval) / 4;

	if ((TEST (NEW_NOTE) || (TEST (NEW_INS) && (xc->ins != xc->oldins)))) {
	    xc->y_idx = xc->t_idx = 0;
	    drv->setpatch (ch, XXI[XXIM.ins[xc->key]].sid);
	    drv->setnote (ch, xc->note);

	    /* Pan effect setting fixed by Frederic Bujon <lvdl@bigfoot.com> */
	    xc->pan = (e->fxt == FX_SETPAN) ?
		e->fxp : XXI[XXIM.ins[xc->key]].pan;
	}

	if (offset) {
	    drv->voicepos (ch, offset);
	    offset = 0;
	}
    }

    if (xc->ins == -1)
	return;

    vol_envelope = (XXIH.aei.flg & XXM_ENV_ON) && !opt.noenv ?
	get_envelope (XXAE, XXIH.aei.npt, xc->v_idx) : 64;

    pan_envelope = (XXIH.pei.flg & XXM_ENV_ON) && !opt.noenv ?
	get_envelope (XXPE, XXIH.pei.npt, xc->p_idx) : 32;

    /* Update envelopes */
    DO_ENVELOPE (XXIH.aei.flg, xc->v_idx, XXAE,
	XXIH.aei.sus, XXIH.aei.lps, XXIH.aei.lpe);
    DO_ENVELOPE (XXIH.pei.flg, xc->p_idx, XXPE,
	XXIH.pei.sus, XXIH.pei.lps, XXIH.pei.lpe);

    vol2 = xc->volume;

    if (TEST (TREMOLO))
	vol2 += waveform[xc->t_type][xc->t_idx] * xc->t_depth / 512;
    if (vol2 > vol_base)
	vol2 = vol_base;
    if (vol2 < 0)
	vol2 = 0;

    finalvol = (uint32) (xc->fadeout * vol_envelope * __global_vol *
	((int) vol2 * 0x40 / vol_base)) >> 20;

    finalvol = vol_xlat ? (vol_xlat[finalvol >> 5] * 100) >> 6 :
	(finalvol * 80) >> 11;

    xc->pitchbend = period_to_bend (xc->period + (TEST (VIBRATO) ?
	waveform[xc->y_type][xc->y_idx] * xc->y_depth * 64 / 8192 : 0) +
	waveform[XXI->vwf][xc->insvib_idx] * XXI->vde / (1024 *
	(1 + xc->insvib_swp)),
	xc->note, xc->finetune, xxh->flg & XXM_FLG_MODRNG, xc->gliss,
	xxh->flg & XXM_FLG_LINEAR);

    /* From Takashi Iwai's awedrv FAQ:
     *
     * Q3.9: Many clicking noises can be heard in some midi files.
     *    A: If this happens when panning status changes, it is due to the
     *       restriction of Emu8000 chip. Try -P option with drvmidi. This
     *       option suppress the realtime pan position change. Otherwise,
     *       it may be a bug.
     */

    finalpan = opt.nopan ? 128 : xc->pan + (pan_envelope - 32) *
	(128 - abs (xc->pan - 128)) / 32;
    finalpan = xc->masterpan + (finalpan - 128) *
	(128 - abs (xc->masterpan - 128)) / 128;

    drv->echoback ((finalpan << 12) | (ch << 4) | XMP_ECHO_CHN);

    if (TEST (NEW_NOTE | NEW_INS | NEW_VOL | PITCHBEND | TONEPORTA)) {
	drv->echoback ((xc->key << 12) | (xc->ins << 4) | XMP_ECHO_INS);
	drv->echoback ((xc->volume << 4) | XMP_ECHO_VOL);
	drv->echoback ((xc->pitchbend << 4) | XMP_ECHO_PBD);
    }

    /* Do delay */
    if (xc->delay) {
	if (--xc->delay)
	    finalvol = 0;
	else
	    drv->setnote (ch, xc->note);
    }

    /* Do cut/retrig */
    if (xc->retrig) {
	if (!--xc->rcount) {
	    drv->setnote (ch, xc->note);
	    xc->volume += rval[xc->rtype].s;
	    xc->volume *= rval[xc->rtype].m;
	    xc->volume /= rval[xc->rtype].d;
	    xc->rcount = xc->retrig;
	}
    }

    /* Do global volume slide */
    if (t || opt.vef) {
	if (!ch && TEST_FLAG (global_flags, GLOBAL_VSLIDE)) {
	    __global_vol += global_vslide;
	    if (__global_vol < 0)
		__global_vol = 0;
	    else if (__global_vol > 0xff)
		__global_vol = 0xff;
	}

	if (TEST (VOL_SLIDE)) {
	    xc->volume += xc->v_val;
	    if (xc->volume < 0)
		xc->volume = 0;
	    else if (xc->volume > vol_base)
		xc->volume = vol_base;
	}
    }

    /* Do pan and pitch sliding */
    if (t) {
	if (TEST (PAN_SLIDE)) {
	    xc->pan += xc->p_val;
	    if (xc->pan < 0)
		xc->pan = 0;
	    else if (xc->pan > 0xff)
		xc->pan = 0xff;
	}

	if (TEST (PITCHBEND)) {
	    if (xxh->flg & XXM_FLG_LINEAR) {
		xc->period += xc->f_val;
		if (xc->period < MIN_PERIOD_L)
		    xc->period = MIN_PERIOD_L;
		else if (xc->period > MAX_PERIOD_L)
		    xc->period = MAX_PERIOD_L;
	    } else {
		xc->period += xc->f_val;
		if (xc->period < MIN_PERIOD_A)
		    xc->period = MIN_PERIOD_A;
		else if (xc->period > MAX_PERIOD_A)
		    xc->period = MAX_PERIOD_A;
	    }
	}

	/* Workaround for panic.s3m (from Toru Egashira's NSPmod) */
	if (xc->period <= 8)
	    xc->volume = 0;
    }

    /* Do tone portamento */
    if (TEST (TONEPORTA)) {
	xc->period += (xc->s_sgn * xc->s_val);
	if ((xc->s_sgn * xc->s_end) <= (xc->s_sgn * xc->period)) {
	    xc->period = xc->s_end;
	    RESET (TONEPORTA);
	}
    }

    /* Update vibrato, tremolo and arpeggio indexes */
    xc->insvib_idx += XXI->vra >> 2;
    xc->insvib_idx %= 64;
    if (xc->insvib_swp > 1)
	xc->insvib_swp -= 2;
    else
	xc->insvib_swp = 0;
    xc->y_idx += xc->y_rate;
    xc->y_idx %= 64;
    xc->t_idx += xc->t_rate;
    xc->t_idx %= 64;
    xc->a_idx++;
    xc->a_idx %= 3;

    /* Adjust pitch and pan, than play the note */
    drv->setbend (ch, (xc->pitchbend + xc->a_val[xc->a_idx]));
    drv->setpan (ch, (finalpan - 0x80) * opt.mix / 100 * opt.reverse);

    if (!xctl->mute) 
	drv->setvol (ch, finalvol);
}


void xmpi_play_module ()
{
    int r, o, t, c;		/* rows, order, tick, channel */
    uint8 *p = 0;		/* pattern */

    next_time = 0;
    if (__event_callback == NULL)
	__event_callback = dummy;

    o = opt.start;
    __global_vol = xmpi_oinfo[o].gvl;
    __bpm = xmpi_oinfo[o].bpm;
    tempo = xmpi_oinfo[o].tempo;
    tick_duration = opt.rrate / __bpm;
    playing_time = global_flags = offset = 0;
    vol_base = opt.vol_base;
    vol_xlat = opt.vol_xlat;

    for (c = 0; c < 32; c++)
	CLEAR_CHANNEL (c);

    drv->numvoices (32);
    drv->reset ();

    for (r = c = 0; c < xxh->chn; c++)
	xc[c].masterpan = xxc[c].pan;

    drv->numvoices (xxh->chn);
    drv->starttimer ();

    loop_stack = calloc (sizeof (uint8), ROW_MAX);
    jump_pattern = -1;
    __global_vol = 0x40;
    global_vslide = 0;

    for (; o < xxh->len; o++) {
	__current_ord = o;

	if ((xxo[o] >= xxh->pat) && (xxo[o] != 0xff))
	    continue;

	/* S3M uses 0xff as an end mark */
	if (xxo[o] == 0xff) {
	    if (opt.ignoreff) {
		for (c = 0; c < xxh->chn; c++) {
		    drv->setvol (c, 0);
		    xc[c].masterpan = xxc[c].pan;
		}
		tick_duration = opt.rrate / (__bpm = xxh->bpm);
		tempo = xxh->tpo;
		global_flags = 0;
		xc[c].flags = 0;
		continue;
	    }
	    if (opt.loop)
		o = xxh->rst;
	    else
		break;
	}
	r = (uint8) (xxp[xxo[o]]->rows - 1) + 1;
	if (jump_line >= r)
	    jump_line = 0;
	loop_row = -(jump_line + 1);
	row_cnt = jump_line;
	loop_cnt = jump_line = 0;

	for (; row_cnt < r; row_cnt++, row_ptr = p) {
	    if (__pause) {
		xmpi_timer_stop ();
		while (__pause) {
		    xmpi_select_read (1, 125);
		    __event_callback (0);
		}
		xmpi_timer_restart ();
	    }

	    drv->echoback ((tempo << 12) | (__bpm << 4) | XMP_ECHO_BPM);
	    drv->echoback ((__global_vol << 4) | XMP_ECHO_GVL);
	    drv->echoback ((xxo[o] << 12) | (o << 4) | XMP_ECHO_ORD);
	    drv->echoback ((xxp[xxo[o]]->rows << 12) |
		(row_cnt << 4) | XMP_ECHO_ROW);

	    for (t = 0; t < (tempo * (1 + delay_pattern)); t++) {

		/* xmp_player_ctl processing */

		if (o != __current_ord) {
		    if (__current_ord == -1)
			__current_ord++;

		    if (__current_ord == -2) {	/* set by xmp_module_stop */
			drv->bufwipe ();
			SET_FLAG (global_flags, MODULE_ABORT);
			break;
		    }

		    row_cnt = -1;
		    row_ptr = p;
		    o = __current_ord;
		    tempo = xmpi_oinfo[o].tempo;
		    __global_vol = xmpi_oinfo[o].gvl;
		    tick_duration = opt.rrate / (__bpm = xmpi_oinfo[o].bpm);
		    o--;
		    drv->bufwipe ();
		    drv->sync (next_time = 0);
		    drv->reset ();
		    for (c = 0; c < xxh->chn; c++)
			CLEAR_CHANNEL (c);
		    break;
		}

		for (c = 0; c < xxh->chn; c++) {
		    fetch_and_play (t, &EVENT (xxo[o], c, row_cnt),
			&xc[c], &__xctl[c], c);
		}

		if (opt.time && (opt.time < playing_time))
		    SET_FLAG (global_flags, MODULE_ABORT);

		playing_time += opt.rrate / (100 * __bpm);

		next_time += tick_duration;
		drv->sync (next_time);
		drv->bufdump ();
	    }

	    delay_pattern = 0;

	    if (row_cnt == -1)
		break;

	    if (TEST_FLAG (global_flags, MODULE_ABORT | PATTERN_BREAK))
		break;

	    if (TEST_FLAG (global_flags, PATTERN_LOOP)) {
		loop_cnt = 0;
		p = loop_ptr;
		if (loop_row < 0) {
		    row_cnt = -(loop_row + 2);
		    RESET_FLAG (global_flags, PATTERN_LOOP);
		} else
		    row_cnt = loop_row - 1;
	    }
	}

	if (TEST_FLAG (global_flags, MODULE_ABORT))
	    break;

	if (opt.loop && (o == (xxh->len - 1)))
	    o = xxh->rst - 1;

	if ((jump_pattern != -1) && (!opt.noback || (jump_pattern > o))) {
	    if (!opt.loop && (jump_pattern <= o))
		break;
	    o = jump_pattern - 1;
	}

	jump_pattern = -1;
	RESET_FLAG (global_flags, PATTERN_BREAK);
    }

    drv->echoback (XMP_ECHO_END);
    drv->reset ();
    while (drv->getmsg () != XMP_ECHO_END)
	drv->bufdump ();
    drv->stoptimer ();
    drv->writepatch (NULL);	/* Unload all patches */

    free (loop_stack);
    opt.start = 0;
}


void xmpi_timer_stop ()
{
    int c;

    for (c = 0; c < 32; c++)
	drv->setvol (c, 0);
    drv->stoptimer ();
    drv->bufdump ();
}


void xmpi_timer_restart ()
{
    drv->sync (next_time = 0);
    drv->starttimer ();
}

