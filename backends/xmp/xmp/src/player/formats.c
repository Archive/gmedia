/* Extended Module Player - formats.c
 * Copyright (C) 1996, 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#include "config.h"

#include <stdlib.h>
#include "xmpi.h"
#include "formats.h"

struct xmp_fmt_info *__fmt_head;


static void register_fmt (char *suffix, char *tracker, int (*loader) ())
{
    struct xmp_fmt_info *f;

    f = malloc (sizeof (struct xmp_fmt_info));
    f->tracker = tracker;
    f->suffix = suffix;
    f->loader = loader;

    if (!__fmt_head)
	__fmt_head = f;
    else {
	struct xmp_fmt_info *i;

	for (i = __fmt_head; i->next; i = i->next);
	i->next = f;
    }

    f->next = NULL;
}


void fmt_register ()
{
    register_fmt ("xm", "Fast Tracker II", xm_load);
    register_fmt ("mod", "Noise/Fast/Protracker", mod_load);
    register_fmt ("s3m", "Scream Tracker 3", s3m_load);
    register_fmt ("stm", "Scream Tracker 2", stm_load);
    register_fmt ("mtm", "Multitracker", mtm_load);
    /* register_fmt ("mdl", "Digitrakker", mdl_load); */
    /* register_fmt ("it", "Impulse Tracker", it_load); */
    /* register_fmt ("rtm", "Real Tracker", rtm_load); */
    register_fmt ("ptm", "Poly Tracker", ptm_load);
    register_fmt ("okt", "Oktalyzer", okt_load);
    register_fmt ("far", "Farandole Composer", far_load);
    register_fmt ("wow", "Mod's Grave", NULL);
#ifdef DRIVER_OSS_SEQ
    register_fmt ("amd", "Amusic Adlib Tracker", amd_load);
    register_fmt ("rad", "Reality Adlib Tracker", rad_load);
    /* register_fmt ("hsc", "HSC Adlib Tracker", hsc_load); */
#endif
    /* register_fmt ("fnk", "Funktracker", fnk_load); */
    register_fmt ("669", "Composer 669", ssn_load);
    register_fmt ("alm", "Aley's modules", alm_load);
    register_fmt ("mod", "Soundtracker 15 instruments", m15_load);
}
