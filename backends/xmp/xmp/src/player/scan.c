/* Extended Module Player
 * Copyright (C) 1996-98 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "xmpi.h"
#include "xxm.h"
#include "effects.h"

extern struct xmp_ord_info xmpi_oinfo[256];

static int order;
static int jmp_ord;
static int base_time;

static int
scan_pattern (int fst_row, int lst_row, int chn, int* tempo, int* bpm)
{
    int f1, f2, p, r, c;
    int cnt = 0;
    int loop = 0;
    int clock = 0;
    int finish = 0;

    for (r = fst_row; r <= lst_row; r++, cnt++) {
	for (c = 0; c < xxh->chn; c++) {
	    f1 = EVENT (xxo[order], c, r).fxt;
	    f2 = EVENT (xxo[order], c, r).f2t;

	    if ((f1 == FX_TEMPO) || (f2 == FX_TEMPO)) {
		p = (f1 == FX_TEMPO)?
		    EVENT (xxo[order], c, r).fxp:
		    EVENT (xxo[order], c, r).f2p;

		clock += cnt * *tempo * base_time * 100 / *bpm;
		cnt = 0;

		if (p > 32)
		    *bpm = p;
		else
		    *tempo = p;
	    }

	    if ((f1 == FX_JUMP) || (f2 == FX_JUMP)) {
		jmp_ord = (f1 == FX_JUMP)?
		    EVENT (xxo[order], c, r).fxp:
		    EVENT (xxo[order], c, r).f2p;
		finish = 1;
	    }

	    if ((f1 == FX_BREAK) || (f2 == FX_BREAK)) {
		finish = 1;
	    }

	    if ((f1 == FX_EXTENDED) || (f2 == FX_EXTENDED)) {
		p = (f1 == FX_EXTENDED)?
		    EVENT (xxo[order], c, r).fxp:
		    EVENT (xxo[order], c, r).f2p;

		if ((p >> 4) == EX_F_PATT_DELAY)
		    clock += (p & 0x0f) * *tempo * base_time * 100 / *bpm;

		if ((r == lst_row) && (c >= chn))
		    continue;

		if ((p >> 4) == EX_PATTERN_LOOP)
		    if (p &= 0x0f)
			clock += p * scan_pattern (loop, r, c, tempo, bpm);
		    else 
			loop = r;
	    }
	}
	if (finish) {
	    cnt++;
	    break;
	}
    }
    return clock += cnt * *tempo * base_time * 100 / *bpm;
}


int xmpi_scan_module (double replay_rate)
{
    int bpm, tempo;
    int clock = 0;

    bpm = xxh->bpm;
    tempo = opt.tempo ? opt.tempo : xxh->tpo;
    if (!tempo)
	tempo = 6;
    base_time = replay_rate;
    jmp_ord = -1;

    for (order = 0; order < xxh->len; order++) {
	if (xxo[order] >= xxh->pat)
	    if (!opt.ignoreff && xxo[order] == 0xff)
		break;
	    else
		continue;

	xmpi_oinfo[order].bpm = bpm;
	xmpi_oinfo[order].tempo = tempo;
	xmpi_oinfo[order].gvl = 64;		/* gvl */
	xmpi_oinfo[order].time = clock / 10;

	clock += scan_pattern (0, xxp[xxo[order]]->rows - 1, 256, &tempo, &bpm);

	if (jmp_ord != -1) {
	    if (jmp_ord <= order)
		if (!opt.noback)
		    break;
	    else 
		order = jmp_ord;
	    jmp_ord = -1;
	}
    }
    return clock / 10;
}

