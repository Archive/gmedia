/*
** adpcm.h - include file for adpcm coder.
**
** Version 1.0, 7-Jul-92.
*/

struct adpcm_state {
    short valprev;		/* Previous output value */
    unsigned char index;	/* Index into stepsize table */
    unsigned char pad;
};

void adpcm_coder(const unsigned char*, unsigned char*, int, struct adpcm_state *);
void adpcm_decoder(const unsigned char*, unsigned char*, char *, int, struct adpcm_state *);
