

#ifndef __IFF_H
#define __IFF_H

#define IFF_NOBUFFER 0x0001
#define IFF_BIG_ENDIAN 0
#define IFF_LITTLE_ENDIAN 1

struct iff_info {
    char id[5];
    void (*loader) ();
    struct iff_info *next;
};

void iff_chunk (FILE *);
void iff_register (char *, void ());
void iff_distort (int, int);
int iff_process (char *, long, FILE *);

#endif /* __IFF_H */
