/* Extended Module Player - xxm.h
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#ifndef __XXM_H
#define __XXM_H

#define XXM_FLG_LINEAR	0x01
#define XXM_FLG_MODRNG	0x02
#define XXM_FLG_LOOPS3M	0x04
#define XXM_FLG_LOOPXXM	0x08

struct xxm_header {
    uint8 ver;				/* Version */
    uint8 flg;				/* Flags */
    uint8 pat;				/* Number of patterns */
    uint8 ptc;				/* Number of patches */
    uint16 trk;				/* Number of tracks */
    uint8 chn;				/* Tracks per pattern */
    uint8 ins;				/* Number of instruments */
    uint8 smp;				/* Number of samples */
    uint8 tpo;				/* Initial tempo */
    uint8 bpm;				/* Initial BPM */
    uint8 len;				/* Module length in patterns */
    uint8 rst;				/* Restart position */
    uint8 gvl;				/* Global volume */
    uint8 chd;				/* Chord mode */
    uint8 rsvd[3];			/* Reserved */
    uint8 title[32];
};

#define XXM_CHANNEL_FM 0x01

struct xxm_channel {
    uint8 pan;
    uint8 vol;
    uint8 rsvd;
    uint8 flg;
};

struct xxm_trackinfo {
    uint16 index;			/* Track index */
    int8 xpose;				/* Track transpose */
    uint8 flags;			/* Flags */
};

struct xxm_pattern {
    uint8 rows;				/* Number of rows */
    struct xxm_trackinfo info[1];
};

#define XXM_EVENT_PACK 0x80
#define XXM_EVENT_MASK 0x7f
#define XXM_EVENT_NOT 0x01
#define XXM_EVENT_INS 0x02
#define XXM_EVENT_VOL 0x04
#define XXM_EVENT_FXT 0x08
#define XXM_EVENT_FXP 0x10

struct xxm_event {
    uint8 note;				/* Note number (0==no note) */
    uint8 ins;				/* Patch number */
    uint8 vol;				/* Volume (0 to 64) */
    uint8 fxt;				/* Effect type */
    uint8 fxp;				/* Effect parameter */
    uint8 f2t;				/* Secondary effect type */
    uint8 f2p;				/* Secondary effect parameter */
};

struct xxm_track {
    uint8 rows;				/* Number of rows */
    struct xxm_event event[1];
};

#define XXM_ENV_ON	0x01
#define XXM_ENV_SUS	0x02
#define XXM_ENV_LOOP	0x04
#define XXM_ENV_RLS	0x08

struct xxm_envinfo {
    uint8 flg;				/* Flags */
    uint8 npt;				/* Number of envelope points */
    uint8 scl;				/* Envelope scaling */
    uint8 sus;				/* Sustain point */
    uint8 lps;				/* Loop start point */
    uint8 lpe;				/* Loop end point */
};

struct xxm_instrument_header {
    uint8 name[32];			/* Instrument name */
    uint8 zero;				/* Always zero */
    uint8 nsm;				/* Number of samples */
    uint16 rls;
    struct xxm_envinfo aei;		/* Amplitude envelope info */
    struct xxm_envinfo pei;		/* Pan envelope info */
    struct xxm_envinfo fei;		/* Frequency envelope info */
};

struct xxm_instrument_map {
    uint8 ins[96];			/* Instrument number for each key */
};

struct xxm_instrument {
    uint8 vol;				/* Volume */
    uint8 pan;				/* Pan */
    int8 xpo;				/* Transpose */
    int8 fin;				/* Finetune */
    uint8 vos;				/* Volume scaling -- NOT USED */
    uint8 fsc;				/* Frequency scaling -- NOT USED */
    uint8 vsc;				/* Velocity scaling -- NOT USED */
    uint16 flg;				/* Flags */
    uint8 vwf;				/* Vibrato waveform */
    uint8 vde;				/* Vibrato depth */
    uint8 vra;				/* Vibrato rate */
    uint8 vsw;				/* Vibrato sweep */
    uint8 dly;				/* Delay rate -- NOT USED */
    uint8 dre;				/* Delay repetitions -- NOT USED */
    uint8 dam;				/* Delay amplitude -- NOT USED */
    uint8 psp;				/* Portamento speed -- NOT USED */
    uint8 rvv;				/* Random volume var -- NOT USED */
    uint8 rpv;				/* Random pan variation -- NOT USED */
    uint8 sid;				/* Sample number */
};

#define XXM_SMP_16BIT	0x01
#define XXM_SMP_FDLOOP	0x02
#define XXM_SMP_BDLOOP	0x04
#define XXM_SMP_RLS	0x02

struct xxm_sample {
    uint8 name[32];			/* Sample name */
    uint32 len;				/* Sample length */
    uint32 lps;				/* Loop start */
    uint32 lpe;				/* Loop end */
    uint16 flg;				/* Flags */
};

#endif /* __XM_H */
