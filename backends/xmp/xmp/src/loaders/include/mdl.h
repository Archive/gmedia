/* Extended Module Player - mdl.h
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */


/* Pattern packing flags */

#define MDL_NOTE_FOLLOWS	0x04
#define MDL_INSTRUMENT_FOLLOWS	0x08
#define MDL_VOLUME_FOLLOWS	0x10
#define MDL_EFFECT_FOLLOWS	0x20
#define MDL_PARAMETER1_FOLLOWS	0x40
#define MDL_PARAMETER2_FOLLOWS	0x80


struct mdl_in_chunk {
    char name[32];
    char author[20];
    uint16 length;
    uint16 restart;
    uint8 gvol;
    uint8 tempo;
    uint8 bpm;
    uint8 chinfo[32];
    uint8 orders[1];
} PACKED;

