/* Extended Module Player - mod.h
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */


struct mod_instrument_header {
    uint8 name[22];		/* Instrument name */
    uint16 length;		/* Sample length in 16-bit words */
    int8 finetune;		/* Finetune (signed nibble) */
    int8 volume;		/* Linear playback volume */
    uint16 loopstart;		/* Loop start in 16-bit words */
    uint16 looplength;		/* Loop length in 16-bit words */
} PACKED;

struct mod_file_header {
    uint8 name[20];
    struct mod_instrument_header ins[31];
    uint8 songlen;
    uint8 restart;		/* Number of patterns in Soundtracker,
				 * Restart in Noisetracker/Startrekker,
				 * 0x7F in Protracker
				 */
    uint8 orders[128];
    uint8 magic[4];
} PACKED;


/* Soundtracker 15-instrument module header */

struct mod15_file_header {
    uint8 name[20];
    struct mod_instrument_header ins[15];
    uint8 songlen;
    uint8 restart;
    uint8 orders[128];
} PACKED;


union mod_header {
    struct mod_file_header m31;
    struct mod15_file_header m15;
} PACKED;

