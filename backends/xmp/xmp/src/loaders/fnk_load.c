/* Extended Module Player - fnk_load.c
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/Copying
 * for more information.
 */

/* INCOMPLETE LOADER!!! NO EFFECTS! */

#include "config.h"

#include "load.h"
#include "fnk.h"


static uint8 fx[] =
{
    FX_0, FX_0, FX_0, FX_0,
    FX_0, FX_0, FX_0, FX_0,
    FX_0, FX_0, FX_1, FX_2,
    FX_3, FX_E, FX_4, FX_0
};


int fnk_load (FILE * f, struct dev_info *d)
{
    int i, j;
    struct xxm_event *event;
    struct fnk_file_header ffh;
    uint8 ev[3];

    LOAD_INIT ();

    fread (&ffh, 1, sizeof (ffh), f);
    if (strncmp ((char *) ffh.marker, "Funk", 4) ||
	strncmp ((char *) ffh.format, "F2", 2))
	return -1;

    xxh->chn = (ffh.format[2] < '0') || (ffh.format[2] > '9') ||
	(ffh.format[3] < '0') || (ffh.format[3] > '9') ? 8 :
	(ffh.format[2] - '0') * 10 + ffh.format[3] - '0';
    xxh->ins = 64;
    for (i = 0; (i < 256) && (ffh.order[i] != 0xff); i++)
	if (ffh.order[i] > xxh->pat)
	    xxh->pat = i;
    xxh->len = i;
    xxh->trk = xxh->chn * xxh->pat;
    memcpy (xxo, ffh.order, xxh->len);
    xxh->tpo = 6;
    xxh->bpm = ffh.info[3] >> 1;
    if (xxh->bpm & 64)
	xxh->bpm = -(xxh->bpm & 63);
    xxh->bpm += 125;
    xxh->smp = xxh->ins;
    strcpy (module_type, "FunktrackerGOLD module");

    MODULE_INFO ();

    INSTRUMENT_INIT ();

    /* Convert instruments */
    for (i = 0; i < xxh->ins; i++) {
	xxi[i] = calloc (sizeof (struct xxm_instrument), 1);
	L_ENDIAN32 (ffh.fih[i].length);
	L_ENDIAN32 (ffh.fih[i].loopstart);
	xxih[i].nsm = !!(xxs[i].len = ffh.fih[i].length);
	xxs[i].lps = ffh.fih[i].loopstart;
	if (xxs[i].lps == -1)
	    xxs[i].lps = 0;
	xxs[i].lpe = ffh.fih[i].length;
	xxs[i].flg = ffh.fih[i].loopstart != -1 ? WAVE_LOOPING : 0;
	xxi[i][0].vol = ffh.fih[i].volume;
	xxi[i][0].pan = ffh.fih[i].pan;
	xxi[i][0].sid = i;
	strncpy ((char *) xxih[i].name, ffh.fih[i].name, 19);
	str_adj ((char *) xxih[i].name);
	if ((opt.verbose > 1) && (strlen ((char *) xxih[i].name) || (xxs[i].len > 2)))
	    report ("[%2X] %-20.20s %04x %04x %04x %c V%02x P%02x\n", i,
		xxih[i].name, xxs[i].len, xxs[i].lps, xxs[i].lpe,
		xxs[i].flg & WAVE_LOOPING ? 'L' : ' ', xxi[i][0].vol, xxi[i][0].pan);
    }

    PATTERN_INIT ();

    /* Read and convert patterns */
    if (opt.verbose)
	report ("Stored patterns: %d ", xxh->pat);
    for (i = 0; i < xxh->pat; i++) {
	PATTERN_ALLOC (i);
	xxp[i]->rows = 64;
	TRACK_ALLOC (i);
	EVENT (i, 1, ffh.pbrk[i]).f2t = FX_D;
	for (j = 0; j < 64 * xxh->chn; j++) {
	    event = &EVENT (i, j % xxh->chn, j / xxh->chn);
	    fread (&ev, 1, 3, f);
	    switch (ev[0] >> 2) {
	    case 0x3f:
	    case 0x3e:
	    case 0x3d:
		break;
	    default:
		event->not = 25 + (ev[0] >> 2);
		event->ins = 1 + MSN (ev[1]) + ((ev[0] & 0x03) << 4);
		event->vol = ffh.fih[event->ins - 1].volume;
	    }
	    if (ev[2] + 1) {
		event->fxt = fx[MSN (ev[2])];
		event->fxp = fx[LSN (ev[2])];
		if (!event->fxp)
		    event->fxp = 0;
		switch (event->fxt) {
		case FX_0:
		    event->fxp = 0;
		    break;
		case FX_4:
		    event->fxp |= 0x80;		/* Wild guess */
		    break;
		case FX_E:
		    event->fxp = 0x53;	/* Wild guess */
		    break;
		}
	    }
	}
	if (opt.verbose)
	    report (".");
    }

    /* Read samples */
    if (opt.verbose)
	report ("\nStored samples : %d ", xxh->smp);
    for (i = 0; i < xxh->ins; i++) {
	if (xxs[i].len <= 2)
	    continue;
	device_writepatch (f, d, xxi[i][0].sid, c4_rate, 0, &xxs[i], NULL);
	if (opt.verbose)
	    report (".");
    }
    if (opt.verbose)
	report ("\n");

    for (i = 0; i < xxh->chn; i++)
	xxc[i].pan = (i % 2) * 0xff;
    vol_base = 0x100;

    return 0;
}
