/* Extended Module Player - rtm_load.c
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#include "config.h"

#include "load.h"
#include "rtm.h"


static int load_header (struct ObjectHeader *object, char *id, FILE * f)
{
    static char tmp[256];
    int i;

    fread (object, sizeof (struct ObjectHeader), 1, f);
    if (strncmp (object->id, id, 4))
	return 0;

    L_ENDIAN16 (object->version);
    L_ENDIAN16 (object->headerSize);

    for (i = object->headerSize - sizeof (object); i > 0; i -= 256)
	fread (tmp, 1, i > 256 ? 256 : i, f);

    return 1;
}


int rtm_load (FILE * f, struct dev_info *d)
{
    int i, j, r;
    int instr_no = 0;
    uint8 *patbuf, *p, b;
    struct ObjectHeader object;
    struct RTMMHeader rtmm;
    struct RTNDHeader rtnd;
    struct RTINHeader rtin;
    struct RTSMHeader rtsm;

    LOAD_INIT ();

    /* Load object header */
    if (!load_header (&object, "RTMM", f))
	return -1;

    strncpy (module_name, object.name, 32);
    strcpy (module_type, "Real Tracker module");

    fread (&rtmm, 1, sizeof (rtmm), f);

    L_ENDIAN16 (rtmm.flags);
    L_ENDIAN16 (rtmm.nposition);
    L_ENDIAN16 (rtmm.npattern);
    L_ENDIAN32 (rtmm.extraDataSize);

    xxh->len = rtmm.nposition;
    xxh->rst = 0;
    xxh->chn = rtmm.ntrack;
    xxh->pat = rtmm.npattern;
    xxh->trk = xxh->pat * xxh->chn;
    xxh->ins = rtmm.ninstr;
    xxh->tpo = rtmm.speed;
    xxh->bpm = rtmm.tempo;
    xxh->flg = rtmm.flags & 1;
    fread (xxo, 1, xxh->len, f);
    fseek (f, sizeof (object) + sizeof (rtmm) + rtmm.extraDataSize, SEEK_SET);

    MODULE_INFO ();

    PATTERN_INIT ();

    if (opt.verbose)
	report ("Stored patterns: %d ", xxh->pat);

    for (i = 0; i < xxh->pat; i++) {
	if (!load_header (&object, "RTND", f))
	    return -1;
	if (opt.verbose)
	    report (".");
    }
    if (opt.verbose)
	report ("\n");
#if 0
    fread (&xph, sizeof (xph), 1, f);
    L_ENDIAN16 (xph.rows);
    L_ENDIAN16 (xph.datasize);
    PATTERN_ALLOC (i);
    if (!(r = xxp[i]->rows = xph.rows))
	r = 0x100;
    TRACK_ALLOC (i);

    p = patbuf = calloc (1, xph.datasize);
    fread (patbuf, 1, xph.datasize, f);
    for (j = 0; j < (xxh->chn * r); j++) {
	if ((p - patbuf) > xph.datasize)
	    break;
	event = &EVENT (i, j % xxh->chn, j / xxh->chn);
	if ((b = *p++) & XM_EVENT_PACKING) {
	    if (b & XM_EVENT_NOTE_FOLLOWS)
		event->note = *p++;
	    if (b & XM_EVENT_INSTRUMENT_FOLLOWS) {
		if (*p & XM_END_OF_SONG)
		    break;
		event->ins = *p++;
	    }
	    if (b & XM_EVENT_VOLUME_FOLLOWS)
		event->vol = *p++;
	    if (b & XM_EVENT_FXTYPE_FOLLOWS)
		event->fxt = *p++;
	    if (b & XM_EVENT_FXPARM_FOLLOWS)
		event->fxp = *p++;
	} else {
	    event->note = b;
	    event->ins = *p++;
	    event->vol = *p++;
	    event->fxt = *p++;
	    event->fxp = *p++;
	}
	if (!event->vol)
	    continue;

	if ((event->vol >= 0x10) && (event->vol <= 0x50)) {	/* Volume set */
	    event->vol -= 0x0f;
	    continue;
	}
	/* Volume column effects */

	switch (event->vol >> 4) {
	case 0x06:		/* Volume slide down */
	    event->f2t = FX_A;
	    event->f2p = event->vol - 0x60;
	    break;
	case 0x07:		/* Volume slide up */
	    event->f2t = FX_A;
	    event->f2p = (event->vol - 0x70) << 4;
	    break;
	case 0x08:		/* Fine volume slide down */
	    event->f2t = FX_E;
	    event->f2p = 0xb0 | (event->vol - 0x80);
	    break;
	case 0x09:		/* Fine volume slide up */
	    event->f2t = FX_E;
	    event->f2p = 0xa0 | (event->vol - 0x90);
	    break;
	case 0x0a:		/* Set vibrato speed */
	    event->f2t = FX_4;
	    event->f2p = (event->vol - 0xa0) << 4;
	    break;
	case 0x0b:		/* Vibrato */
	    event->f2t = FX_4;
	    event->f2p = event->vol - 0xb0;
	    break;
	case 0x0c:		/* Set panning */
	    event->f2t = FX_8;
	    event->f2p = ((event->vol - 0xc0) << 4) + 8;
	    break;
	case 0x0d:		/* Pan slide left */
	    event->f2t = FX_P;
	    event->f2p = (event->vol - 0xd0) << 4;
	    break;
	case 0x0e:		/* Pan slide right */
	    event->f2t = FX_P;
	    event->f2p = event->vol - 0xe0;
	    break;
	case 0x0f:		/* Tone portamento */
	    event->f2t = FX_3;
	    event->f2p = (event->vol - 0xf0) << 4;
	    break;
	}
	event->vol = 0;
    }
    free (patbuf);
    if (opt.verbose)
	report (".");
}

PATTERN_ALLOC (i);
xxp[i]->rows = 64;
xxt[i * xxh->chn] = calloc (1, sizeof (struct xxm_track) +
    sizeof (struct xxm_event) * 64);
xxt[i * xxh->chn]->rows = 64;
for (j = 0; j < xxh->chn; j++)
    xxp[i]->info[j].index = i * xxh->chn;

if (opt.verbose)
    report ("\nInstruments    : %d ", xxh->ins);
if (opt.verbose > 1)
    report ("\n");

    /* ESTIMATED value! We don't know the actual value at this point */
xxh->smp = 255;

INSTRUMENT_INIT ();

for (i = 0; i < xxh->ins; i++) {
    fread (&xih, sizeof (xih), 1, f);
    L_ENDIAN16 (xih.samples);
    strncpy (xxih[i].name, xih.name, 22);
    str_adj (xxih[i].name);
    xxih[i].nsm = xih.samples;
    if ((opt.verbose > 1) && (strlen ((char *) xxih[i].name) || xxih[i].nsm))
	report ("[%2X] %-22.22s %2d ", i, xxih[i].name, xxih[i].nsm);

    xxi[i] = calloc (sizeof (struct xxm_instrument), xxih[i].nsm);
    if (xxih[i].nsm) {
	fread (&xi, sizeof (xi), 1, f);
	for (j = 0; j < 24; j++) {
	    L_ENDIAN16 (xi.v_env[j]);
	    L_ENDIAN16 (xi.p_env[j]);
	}
	L_ENDIAN16 (xi.v_fade);

	/* Envelope */
	xxih[i].rls = xi.v_fade;
	xxih[i].aei.npt = xi.v_pts;
	xxih[i].aei.sus = xi.v_sus;
	xxih[i].aei.lps = xi.v_start;
	xxih[i].aei.lpe = xi.v_end;
	xxih[i].aei.flg = xi.v_type;
	xxih[i].pei.npt = xi.p_pts;
	xxih[i].pei.sus = xi.p_sus;
	xxih[i].pei.lps = xi.p_start;
	xxih[i].pei.lpe = xi.p_end;
	xxih[i].pei.flg = xi.p_type;
	xxae[i] = calloc (4, xxih[i].aei.npt);
	xxpe[i] = calloc (4, xxih[i].pei.npt);
	memcpy (xxae[i], xi.v_env, xxih[i].aei.npt * 4);
	memcpy (xxpe[i], xi.p_env, xxih[i].pei.npt * 4);

	memcpy (&xxim[i], xi.sample, 96);
	for (j = 0; j < xxih[i].nsm; j++) {
	    fread (&xsh[j], sizeof (xsh[j]), 1, f);
	    L_ENDIAN32 (xsh[j].length);
	    L_ENDIAN32 (xsh[j].loop_start);
	    L_ENDIAN32 (xsh[j].loop_length);
	    xxi[i][j].vol = xsh[j].volume;
	    xxi[i][j].pan = xsh[j].pan;
	    xxi[i][j].xpo = xsh[j].relnote;
	    xxi[i][j].fin = xsh[j].finetune;
	    xxi[i][j].vwf = xi.y_wave;
	    xxi[i][j].vde = xi.y_depth;
	    xxi[i][j].vra = xi.y_rate;
	    xxi[i][j].vsw = xi.y_sweep;
	    xxi[i][j].sid = instr_no;
	    xxs[instr_no].len = xsh[j].length;
	    xxs[instr_no].lps = xsh[j].loop_start;
	    xxs[instr_no].lpe = xsh[j].loop_start + xsh[j].loop_length;
	    xxs[instr_no].flg = xsh[j].type & XM_SAMPLE_16BIT ? WAVE_16_BITS : 0;
	    xxs[instr_no].flg |= xsh[j].type & XM_LOOP_FORWARD ? WAVE_LOOPING : 0;
	    xxs[instr_no].flg |= xsh[j].type & XM_LOOP_PINGPONG ?
		WAVE_LOOPING | WAVE_BIDIR_LOOP : 0;
	    instr_no++;
	}
	for (j = 0; j < xxih[i].nsm; j++) {
	    if ((opt.verbose > 1) && xsh[j].length)
		report ("%s[%1x] %05x%c%05x %05x %c "
		    "V%02x F%+04d P%02x R%+03d",
		    j ? "\n\t\t\t\t" : "\t", j,
		    xxs[xxi[i][j].sid].len,
		    xxs[xxi[i][j].sid].flg & WAVE_16_BITS ? '+' : ' ',
		    xxs[xxi[i][j].sid].lps,
		    xxs[xxi[i][j].sid].lpe,
		    xxs[xxi[i][j].sid].flg & WAVE_LOOPING ? 'L' :
		    xxs[xxi[i][j].sid].flg & WAVE_BIDIR_LOOP ? 'B' : ' ',
		    xsh[j].volume, xsh[j].finetune,
		    xsh[j].pan, xsh[j].relnote);
	    device_writepatch (f, d, xxi[i][j].sid, c4_rate,
		XMP_SMP_DIFF, &xxs[xxi[i][j].sid], NULL);
	}
	if (opt.verbose == 1)
	    report (".");
    }
    if ((opt.verbose > 1) && (strlen ((char *) xxih[i].name) || xih.samples))
	report ("\n");
}

xxh->smp = instr_no;
if (opt.verbose > 1)
    report ("Stored samples : %d", xxh->smp);
if (opt.verbose)
    report ("\n");
for (i = 0; i < xxh->chn; i++)
    xxc[i].pan = 128;
#endif

return 0;
}
