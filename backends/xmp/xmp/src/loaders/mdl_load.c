/* Extended Module Player - mdl_load.c
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#include "config.h"

#include "load.h"
#include "iff.h"
#include "mdl.h"

static struct dev_info *dinfo;
static int i_index[256];
static int i_map[16];


static void get_chunk_in (int size, struct mdl_in_chunk *buffer)
{
    char author[21];
    int i;

    strncpy (module_name, buffer->name, 32);
    memset (author, 0, 21);
    strncpy (author, buffer->author, 20);

    L_ENDIAN16 (buffer->length);
    L_ENDIAN16 (buffer->restart);

    xxh->len = buffer->length;
    xxh->rst = buffer->restart;
    xxh->tpo = buffer->tempo;
    xxh->bpm = buffer->bpm;

    for (i = 0; i < 32; i++) {
	if (buffer->chinfo[i] & 0x80)
	    break;
	xxc[i].pan = buffer->chinfo[i] << 1;
    }
    xxh->chn = i;

    memcpy (xxo, buffer->orders, xxh->len);

    MODULE_INFO ();
    if (opt.verbose)
	report ("Module author  : %s\n", author);
}


static void get_chunk_pa (int size, uint8 * buffer)
{
    int i, j, chn;
    uint16 x16;

    xxh->pat = *buffer++;
    xxh->trk = xxh->pat * xxh->chn;	/* Estimated */

    PATTERN_INIT ();
    if (opt.verbose)
	report ("Stored patterns: %d ", xxh->pat);

    for (i = 0; i < xxh->pat; i++) {
	PATTERN_ALLOC (i);
	chn = *buffer++;
	xxp[i]->rows = (int) (*buffer++) + 1;

	buffer += 16;		/* Skip pattern name */
	for (j = 0; j < chn; j++, buffer += 2) {
	    x16 = *(uint16 *) buffer;
	    L_ENDIAN16 (x16);
	    xxp[i]->info[j].index = x16;
	}
	if (opt.verbose)
	    report (".");
    }
    if (opt.verbose)
	report ("\n");
}


static void get_chunk_tr (int size, uint8 * buffer)
{
    int i, j, k, row, len;
    struct xxm_track *track;

    xxh->trk = *(uint16 *) buffer;
    buffer += 2;

    if (opt.verbose)
	report ("Stored tracks  : %d ", xxh->trk);

    track = calloc (1, sizeof (struct xxm_track) + sizeof (struct xxm_event) * 256);

    /* Track 0 is an empty pattern not stored in the file */
    xxt[0] = calloc (1, sizeof (struct xxm_track) + sizeof (struct xxm_event));
    xxt[0]->rows = 1;

    for (i = 1; i < xxh->trk; i++) {
	/* Length of the track in bytes */
	len = *(uint16 *) buffer;
	buffer += 2;

	memset (track, 0, sizeof (*track));

	for (row = 0; len;) {
	    j = *buffer++;
	    len--;
	    switch (j & 0x03) {
	    case 0:
		row += j >> 2;
		break;
	    case 1:
		for (k = 0; k < (j >> 2); k++)
		    memcpy (&track->event[row + k], &track->event[row - 1],
			sizeof (struct xxm_event));
		row += k - 1;
		break;
	    case 2:
		memcpy (&track->event[row], &track->event[j >> 2],
		    sizeof (struct xxm_event));
		break;
	    case 3:
		if (j & MDL_NOTE_FOLLOWS)
		    len--, track->event[row].note = *buffer++;
		if (j & MDL_INSTRUMENT_FOLLOWS)
		    len--, track->event[row].ins = *buffer++;
		if (j & MDL_VOLUME_FOLLOWS)
		    len--, track->event[row].vol = *buffer++;
		if (j & MDL_EFFECT_FOLLOWS) {
		    len--, k = *buffer++;
		    track->event[row].fxt = LSN (k);
		    track->event[row].f2t = MSN (k);
		}
		if (j & MDL_PARAMETER1_FOLLOWS)
		    len--, track->event[row].fxp = *buffer++;
		if (j & MDL_PARAMETER2_FOLLOWS)
		    len--, track->event[row].f2p = *buffer++;
		break;
	    }
	    row++;
	}

	if (row <= 32)
	    row = 32;
	else if (row <= 64)
	    row = 64;
	else if (row <= 128)
	    row = 128;

	xxt[i] = calloc (1, sizeof (struct xxm_track) +
	    sizeof (struct xxm_event) * row);
	memcpy (xxt[i], track, sizeof (struct xxm_track) +
	    sizeof (struct xxm_event) * row);
	xxt[i]->rows = row;

	if (opt.verbose && !(i % xxh->chn))
	    report (".");
    }
    if (opt.verbose)
	report ("\n");
}


static void get_chunk_ii (int size, uint8 * buffer)
{
    int i, j;
    char *instr;

    xxh->ins = *buffer++;

    for (i = 0; i < xxh->ins; i++) {
	i_index[i] = *buffer++;
	xxih[i].nsm = *buffer++;
	instr = buffer;
	strncpy (xxih[i].name, buffer, 24);
	buffer += 32;

	if ((opt.verbose > 1) && (strlen ((char *) xxih[i].name) || xxih[i].nsm))
	    report ("[%2X] %-32.32s %2d ", i, instr, xxih[i].nsm);

	xxi[i] = calloc (sizeof (struct xxm_instrument), xxih[i].nsm);

	for (j = 0; j < xxih[i].nsm; j++) {
	    xxi[i][j].sid = *buffer++;
	    i_map[j] = *buffer++;
	    xxi[i][j].vol = *buffer++;
	    buffer++;		/* Volume envelope */
	    xxi[i][j].pan = *buffer++ << 1;
	    buffer++;		/* Pan envelope */
	    buffer += 2;	/* Fadeout speed */
	    xxi[i][j].vra = *buffer++;
	    xxi[i][j].vde = *buffer++;
	    xxi[i][j].vsw = *buffer++;
	    xxi[i][j].vwf = *buffer++;
	    buffer++;		/* Reserved */
	    buffer++;		/* Pitch envelope */

	    if (opt.verbose > 1) {
		report ("%s[%1x] V%02x S%02x",
		    j ? "\n\t\t\t\t" : "\t", j, xxi[i][j].vol, xxi[i][j].sid);
	    } else if (opt.verbose == 1)
		report (".");
	}
	if ((opt.verbose > 1) && (strlen ((char *) xxih[i].name) || xxih[i].nsm))
	    report ("\n");
    }
}


static void get_chunk_is (int size, uint8 * buffer)
{
    int n, i;

    n = *buffer++;

    for (i = 0; i < n; i++) {
	buffer++;		/* Sample number */
	buffer += 32;		/* Sample name */
	buffer += 8;		/* Sample filename */

    }
}


static void get_chunk_sa (int size, uint8 * buffer)
{
}


int mdl_load (FILE * f, struct dev_info *d)
{
    char buf[8];

    LOAD_INIT ();

    /* Check magic and get version */
    fread (buf, 1, 4, f);
    if (strncmp (buf, "DMDL", 4))
	return -1;
    fread (buf, 1, 1, f);

    /* IFFoid chunk IDs */
    iff_register ("IN", get_chunk_in);	/* Module info */
    iff_register ("PA", get_chunk_pa);	/* Patterns */
    iff_register ("TR", get_chunk_tr);	/* Tracks */
    iff_register ("II", get_chunk_ii);	/* Instruments */
    iff_register ("IS", get_chunk_is);	/* Sample info */
    iff_register ("SA", get_chunk_sa);	/* Sampled data */

    /* MDL uses a degenerated IFF-style file format with 16 bit IDs and
     * little endian 32 bit chunk size. There's only one chunk per data
     * type i.e. one huge chunk for all the sampled instruments.
     */
    iff_distort (2, IFF_LITTLE_ENDIAN);

    sprintf (module_type, "Digitrakker module version %d.%d",
	MSN (*buf), LSN (*buf));

    dinfo = d;

    /* Load IFFoid chunks */
    while (!feof (f))
	iff_chunk (f);

    if (opt.verbose)
	report ("\n");

    return 0;
}

