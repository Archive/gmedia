Summary: Extended Module Player
Name: xmp
Version: 1.1.5
Release: 1
Packager: Claudio Matsuoka <claudio@pos.inf.ufpr.br>
URL: http://puma.inf.ufpr.br/~claudio/xmp
Group: Applications/Sound
Source: http://xmp.home.ml.org/pkg/1.1.5/xmp-1.1.5.tar.gz
Copyright: GPL

%description
xmp is a tracked music player that recognizes XM, MOD, NST, WOW, S3M, STM,
669, PTM, OKT, MTM, FAR, ALM, RAD, AMD and ALM modules. xmp can play modules
using the OSS sequencer (for Gravis Ultrasound and AWE32) and software
mixing (for /dev/dsp and file output).

%prep
%setup

%build
./configure --prefix=/usr
make

%install
make install

%clean
rm -rf $RPM_BUILD_ROOT

%post

%files
%doc README etc/magic docs/bugs.txt
%doc docs/changes.txt docs/FAQ.html docs/FAQ.txt
/etc/xmprc
/usr/bin/xmp
/usr/bin/xxmp
/usr/man/man1/xmp.1
/usr/man/man1/xxmp.1

