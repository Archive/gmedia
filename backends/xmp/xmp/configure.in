dnl configure.in for xmp
dnl
dnl Process this file with autoconf to produce a configure script.

AC_INIT(src/include/xmp.h)
AC_CONFIG_HEADER(src/include/config.h)
AC_CONFIG_AUX_DIR(./scripts)

AC_CANONICAL_HOST

AC_SUBST(CINCS)
CINCS=""

AC_SUBST(X_XMP)
X_XMP="xxmp"

AC_SUBST(DRIVERS)
DRIVERS="file.o"

AC_ARG_ENABLE(oss-support,
    [  --disable-oss-support   Don't use the Open Sound System],
    [if test "${enable_oss_support}" = no; then
	AC_DEFINE(DISABLE_OSS)
    fi])

AC_ARG_ENABLE(net-driver,
    [  --enable-net-driver     Compile the net driver],
    [if test "${enable_net_driver}" = yes; then
	DRIVERS="${DRIVERS} net.o"
	AC_DEFINE(DRIVER_NET)
    fi])

AC_ARG_ENABLE(esd-driver,
    [  --enable-net-driver     Compile the Enlightened Sound Daemon driver],
    [if test "${enable_esd_driver}" = yes; then
	DRIVERS="${DRIVERS} esd.o"
	LIBS="${LIBS} -lesd"
	AC_DEFINE(DRIVER_ESD)
    fi])

AC_ARG_WITH(oss,
    [  --with-oss=<path>       OSS location if not in /usr/lib/oss],
    oss_path="$withval",
    oss_path="/usr/lib/oss")

if test "${target}" = NONE; then
    target=${host}
    target_cpu=${host_cpu}
    target_verndor=${host_vendor}
    target_os=${host_os}
fi

AC_SUBST(PLATFORM)
PLATFORM="${target}"

AC_SUBST(AR)
AR="${CROSSBIN}ar"

AC_SUBST(STRIP)
STRIP="${CROSSBIN}strip"

if test -n "${CROSSBIN}"; then
    CC="${CROSSBIN}gcc"
    RANLIB="${CROSSBIN}ranlib"
fi



case ${target_os} in
solaris*)
    dnl We need this to compile the package on barracuda.inf.ufpr.br
    dnl Broken gcc, I guess
    dnl
    AC_DEFINE(__EXTENSIONS__)
    AC_DEFINE(__lint)
    ;;
esac


dnl Checks for programs.

AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_PROG_RANLIB


AC_HEADER_STDC

dnl Checks for libraries.

dnl Check for OSS

dnl XMP_CACHED_TEST(<message>,<cache-var>,<test>,<ifyes>,<ifno>)
define(XMP_CACHED_TEST,[
    AC_CACHE_CHECK([$1],[$2],[
        if test [$3]; then
            $2=yes
        else
            $2=no
        fi])
    if test "x$$2" = xyes; then
        true
        $4
    else
        true
        $5
    fi])

XMP_CACHED_TEST(for ${oss_path},ac_cv_dir_usr_lib_oss,-d /usr/lib/oss,
    CINCS=-I/usr/lib/oss/include,
    XMP_CACHED_TEST(for /usr/src/sys/i386/isa/sound,
        ac_cv_dir_usr_src_sys_i386_isa_sound,
        -d /usr/src/sys/i386/isa/sound,
        CINCS=-I/usr/src/sys/i386/isa/sound,))


dnl Checks for header files.

case ${target_os} in
linux*) 
    if test "${target_cpu}" = "sparc"; then
	CINCS="${CINCS} -I/usr/src/linux/drivers"
	CPPFLAGS="${CINCS}"
	AC_CHECK_HEADERS(sbus/audio/audio.h)
	if test "${ac_cv_header_sbus_audio_audio_h}" = "yes"; then
	    DRIVERS="${DRIVERS} solaris.o"
	    AC_DEFINE(DRIVER_SOLARIS)
	fi
    fi
esac

CPPFLAGS="${CINCS}"

AC_CHECK_HEADERS(strings.h getopt.h sys/select.h \
    sys/soundcard.h machine/soundcard.h \
    sys/ultrasound.h linux/ultrasound.h machine/ultrasound.h \
    awe_voice.h sys/awe_voice.h linux/awe_voice.h \
    sys/audio.h \
    sys/audioio.h sys/audio.io.h sun/audioio.h)

if test "${ac_cv_header_sys_soundcard_h}" = "yes" || \
    test "${ac_cv_header_machine_soundcard_h}" = "yes"; then
    DRIVERS="${DRIVERS} oss_seq.o oss_mix.o"
    AC_DEFINE(DRIVER_OSS_SEQ)
    AC_DEFINE(DRIVER_OSS_MIX)
fi 

if test "${ac_cv_header_sys_audio_h}" = "yes"; then
    case ${target_os} in
    hpux*)
	DRIVERS="${DRIVERS} hpux.o"
	AC_DEFINE(DRIVER_HPUX)
    esac
fi

if test "${ac_cv_header_sys_audioio_h}" = "yes" || \
    test "${ac_cv_header_sys_audio_io_h}" = "yes" || \
    test "${ac_cv_header_sun_audioio_h}" = "yes"; then
    case ${target_os} in
    solaris*)
	DRIVERS="${DRIVERS} solaris.o"
	AC_DEFINE(DRIVER_SOLARIS)
	AC_DEFINE(DRIVER_SUNOS)
    esac
fi 


dnl Checks for typedefs, structures, and compiler characteristics.

dnl XMP_CACHED_TRY_COMPILE(<message>,<cache-var>,<headers>,<body>,<ifyes>,<ifno>)
define(XMP_CACHED_TRY_COMPILE,[
    AC_CACHE_CHECK([$1],[$2],[AC_TRY_COMPILE([$3],[$4],[$2=yes],[$2=no])])
    if test "x$$2" = xyes; then
        true
        $5
    else
        true
        $6
    fi])

dnl XMP_CACHED_TRY_RUN(<message>,<cache-var>,<flags>,<program>,<ifyes>,<ifno>)
define(XMP_CACHED_TRY_RUN,[
    if test "${cross_compiling}" = no; then
	AC_CACHE_CHECK([$1],[$2],[
            oldcflags="${CFLAGS}"
	    CFLAGS="${CFLAGS} $3"
	    AC_TRY_RUN($4,[$2=yes],[$2=no])
	    CFLAGS="${oldcflags}"])
	if test "x$$2" = xyes; then
	    CFLAGS="${CFLAGS} $3"
	    $5
	else
	    true
	    $6
	fi
    else
	echo "not checking $1 (cross-compiling)"
    fi])

AC_C_BIGENDIAN

if test "${GCC-no}" = yes; then
    CFLAGS="${CFLAGS} -Wall -finline-functions"
    XMP_CACHED_TRY_RUN(whether gcc needs -fsigned-char,
        ac_cv_c_flag_f_signed_char,,[
        main(){char foo=0xff;return(foo<0);}],
	CFLAGS="${CFLAGS} -fsigned-char")
fi

for sound_hdr in sys/soundcard.h machine/soundcard.h; do
    sound_safe=`echo "$sound_hdr" | sed 'y%./+-%__p_%'`
    if eval "test \"`echo '$ac_cv_header_'$sound_safe`\" = yes"; then
	XMP_CACHED_TRY_COMPILE(whether $sound_hdr knows about audio_buf_info,
	    ac_cv_audio_buf_info,[
#include <$sound_hdr>],
	audio_buf_info info;,
	AC_DEFINE(HAVE_AUDIO_BUF_INFO),)
    fi
done

if test "${GCC-no}" = yes; then
    XMP_CACHED_TRY_COMPILE(whether gcc understands __attribute__((packed)),
	ac_cv_c_attribute_packed,
        ,struct foo{int bar;}__attribute__((packed));,
        AC_DEFINE(HAVE_ATTRIBUTE_PACKED)
        PACKED=yes,PACKED=no)
else
    dnl These two are for the XL C compiler
    XMP_CACHED_TRY_RUN(whether the compiler needs -qalign=packed,
        ac_cv_c_flag_q_align_packed,-qalign=packed,[
        struct foobar{char foo;int bar;};
        main(){return(sizeof(struct foobar)-5);}],
	PACKED=yes,PACKED=no)
    XMP_CACHED_TRY_RUN(whether the compiler needs -qchars=signed,
        ac_cv_c_flag_q_chars_signed,,[
        main(){char foo=0xff;return(foo<0);}],
	CFLAGS="${CFLAGS} -qchars=signed")
fi
    
if test "${PACKED}" = no; then
    AC_MSG_ERROR(Don't know how to use packed data structures!)
fi

dnl Bizarre test, but barracuda.inf.ufpr.br actually has a broken stdarg
dnl
XMP_CACHED_TRY_RUN(
    whether we have a broken stdarg,
    ac_cv_c_stdarg_works,,[
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
    void testarg(char *b,char *fmt,...){va_list a;va_start(a,fmt);
    vsprintf(b,fmt,a);va_end(a);}int main(){char *b=(char*)calloc(1,6);
    testarg(b,"%d %c %s",1,'2',"3");return !strcmp(b,"1 2 3");}],
    AC_DEFINE(HAVE_BROKEN_STDARG))

dnl Checks for library functions.

AC_PROG_GCC_TRADITIONAL
AC_TYPE_SIGNAL
AC_FUNC_VPRINTF
AC_CHECK_FUNCS(select getopt_long)

AC_PATH_XTRA

if test "$no_x" = yes; then
     X_XMP=""
fi

dnl Check if we're building for gmedia

if test -f ../gmedia-xmp.c; then
	GMEDIA=yes
	echo "Enabling gmedia build"
fi

AC_SUBST(GMEDIA)

AC_OUTPUT(Makefile.rules)

