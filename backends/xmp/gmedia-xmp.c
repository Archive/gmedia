#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <stdarg.h>
#include <stdio.h>

#include "libgmedia/gmedia.h"
#include "xmp.h"

#ifndef MAXPATH
#define MAXPATH 1024
#endif

#define DBG(msg, args...)	gmedia_dbg_printf (msg, ##args);

static struct xmp_data_t {
  struct xmp_module_info mi;
  int vol[32];
  int mute[32];
  int pat;
  int ord;
  int progress;
  int row;
  int loaded;
  int cmd;
  int param;
  GMediaMode pending_mode;
  GMediaMode playing_mode;
  time_t playing_time;
  gchar url[MAXPATH];
  int pid;
} *xmp_data = 0;

struct xmp_options xmp_opt;

static int debugging = -1;

void gmedia_dbg_printf(const char *fmt, ...)
{
  va_list ap;
  if(debugging<0)
    debugging = (getenv("DEBUG")!=NULL);
  if(debugging) {
    fprintf(stderr,"libgmedia_xmp: ");
    va_start (ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end (ap);
  }
}

int gmedia_xmp_init()
{
  xmp_init(0, NULL, &xmp_opt);
  if(xmp_open_audio (&xmp_opt)<0)
    return GMEDIA_STATUS_ERROR;

  if ((xmp_data = xmp_get_shared_mem (sizeof (struct xmp_data_t))) <= 0)
    return GMEDIA_STATUS_ERROR;

  memset(xmp_data,0,sizeof(struct xmp_data_t));

  xmp_data->playing_mode = GMEDIA_MODE_STOPPED;
  xmp_data->pending_mode = GMEDIA_MODE_UNAVAILABLE;
  
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_exit()
{
  DBG("-- xmp_exit\n");
  if(xmp_data->loaded)
    gmedia_xmp_eject ();

  xmp_close_audio ();
  /* Nuke stuff allocated at initialization time */
  xmp_detach_shared_mem (xmp_data);
}

int gmedia_xmp_load(gchar *url)
{
  strcpy(xmp_data->url,url);
  xmp_load_module (url);
  xmp_get_module_info (&xmp_data->mi);
  xmp_data->loaded = 1;

  return GMEDIA_STATUS_GOOD;
}

static int gmedia_xmp_callback(long i)
{
  static int chn = 0;
  long msg = i >> 4;
  int cmd;
  cmd = xmp_data->cmd;

  switch (i & 0xf) {
  case XMP_ECHO_ORD:
    xmp_data->ord = msg & 0xff;
    xmp_data->pat = msg >> 8;
    break;
  case XMP_ECHO_ROW:
    xmp_data->progress = xmp_data->ord * 100 / xmp_data->mi.len +
      (msg & 0xff) * 100 / xmp_data->mi.len / (msg >> 8);
    xmp_data->row = msg & 0xff;
    break;
  case XMP_ECHO_CHN:
    chn = msg & 0xff;
    break;
  case XMP_ECHO_VOL:
    xmp_data->vol[chn] = msg & 0xff;
    break;
  default:
    break;
  }
  switch(cmd) {
  case 's':
    if(xmp_data->playing_mode ==  GMEDIA_MODE_PLAYING) {
      xmp_mod_stop ();
      xmp_tell_parent ();
    }
    break;
  case 'p':
    if(xmp_data->playing_mode == GMEDIA_MODE_PLAYING) {
      xmp_mod_pause ();
      xmp_data->playing_mode = GMEDIA_MODE_PAUSED;
      xmp_tell_parent ();
    } else if(xmp_data->playing_mode == GMEDIA_MODE_PAUSED) {
      xmp_mod_pause ();
      xmp_data->playing_mode = GMEDIA_MODE_PLAYING;
      xmp_tell_parent ();
    }
    break;
  case 'f':
    if(xmp_data->playing_mode == GMEDIA_MODE_PLAYING) {
      xmp_ord_next ();
      xmp_tell_parent ();
    }
  case 'b':
    if(xmp_data->playing_mode == GMEDIA_MODE_PLAYING) {
      xmp_ord_prev ();
      xmp_tell_parent ();
    }
    break; 
  case 'r':
    if(xmp_data->playing_mode != GMEDIA_MODE_STOPPED) {
      xmp_mod_restart ();
      xmp_tell_parent ();
    }
    break;
  case 'g':
    if(xmp_data->playing_mode != GMEDIA_MODE_STOPPED) {
      xmp_ord_set(xmp_data->param);
      xmp_tell_parent ();
    }
    break;
  default:
    break;
  }
  xmp_data->cmd = 0;
}

int gmedia_xmp_play()
{
  int pid, status;

  if(!xmp_data->loaded)
    return GMEDIA_STATUS_INVAL;

  if(xmp_data->playing_mode!=GMEDIA_MODE_STOPPED)
    return GMEDIA_STATUS_INVAL;

  if ((pid = fork ())!=0) {
    xmp_data->pid = pid;
    xmp_data->pending_mode = GMEDIA_MODE_PLAYING;
    xmp_tell_child ();
    xmp_wait_child ();
    return GMEDIA_STATUS_GOOD;
  } else {	/* Player */
    int err;

    xmp_register_event_callback (gmedia_xmp_callback);
    do {
      xmp_wait_parent();
      switch(xmp_data->pending_mode) {
      case GMEDIA_MODE_PLAYING:
	if(xmp_data->loaded==1) {
	  xmp_load_module (xmp_data->url);
	  xmp_get_module_info (&xmp_data->mi);
	  xmp_data->loaded = 2;
	}
	xmp_data->playing_mode = GMEDIA_MODE_PLAYING;
	xmp_data->pending_mode = GMEDIA_MODE_UNAVAILABLE;
	xmp_tell_parent();
	xmp_play_module(&err);
	xmp_data->playing_mode = GMEDIA_MODE_STOPPED;
	break;
      default:
	break;
      }
    } while(xmp_data->playing_mode!=GMEDIA_MODE_STOPPED);
    _exit (0);
  }
  return -1;
}

int gmedia_xmp_pause()
{
  switch(xmp_data->playing_mode) {
  case GMEDIA_MODE_PLAYING:
    xmp_data->pending_mode = GMEDIA_MODE_PAUSED;
    xmp_data->cmd = 'p';
    xmp_wait_child ();
    break;
  case GMEDIA_MODE_PAUSED:
    xmp_data->pending_mode = GMEDIA_MODE_PLAYING;
    xmp_data->cmd = 'p';
    xmp_wait_child ();
    break;
  default:
    break;
  }

  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_stop()
{
  DBG("-- xmp_stop()\n");
  if(xmp_data->playing_mode!=GMEDIA_MODE_STOPPED) {
    xmp_data->pending_mode = GMEDIA_MODE_STOPPED;
    xmp_data->cmd = 's';
    xmp_wait_child ();
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_wait()
{
  int status;

  DBG("-- xmp_wait()\n");
  if(xmp_data->pid>0)
    waitpid(xmp_data->pid, &status, 0);
  xmp_data->pid = 0;
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_forward()
{
  xmp_data->cmd = 'f';
  xmp_wait_child ();
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_rewind()
{
  xmp_data->cmd = 'b';
  xmp_wait_child ();
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_restart ()
{
  xmp_data->cmd = 'r';
  xmp_wait_child ();
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_eject()
{
  DBG("-- xmp_eject()\n");
  if(xmp_data->loaded) {
    DBG("---- loaded\n");
    if(xmp_data->playing_mode!=GMEDIA_MODE_STOPPED) {
      DBG("---- stopping\n");
      gmedia_xmp_stop();
    }
    xmp_data->loaded = 0;
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_get_n_frames()
{
  if(xmp_data->loaded==2)
    return xmp_data->mi.len;
  else
    return 0;
}

int gmedia_xmp_set_frame(int nframe)
{
  if(xmp_data->playing_mode) {
    xmp_data->cmd = 'g';
    xmp_data->param = nframe;
    xmp_wait_child ();
  }
  return GMEDIA_STATUS_GOOD;
}

int gmedia_xmp_get_frame()
{
  if(xmp_data->playing_mode != GMEDIA_MODE_STOPPED) {
    return xmp_data->ord;
  } else
    return 0;
}

int gmedia_xmp_get_n_tracks ()
{
  return 1; /* Only one track available here */
}

int gmedia_xmp_set_track (int ntrack)
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_xmp_get_track ()
{
  if(xmp_data->playing_mode==GMEDIA_MODE_STOPPED)
    return 0; /* Return 0 if we're not playing anything */
  else
    return 1; /* Otherwise we're on track 1 */
}

static int check_ext(char *url, char *fext)
{
  int l;

  l = strlen(url)-strlen(fext);
  if(l>0)
    return (!strcmp(&url[l], fext));
  else
    return 0;
}

int gmedia_xmp_check(gchar *url)
{
  if(check_ext(url,".s3m")||
     check_ext(url,".mod")||
     check_ext(url,".669")||
     check_ext(url,".okt")||
     check_ext(url,".far"))
    return GMEDIA_STATUS_GOOD;
  else
    return GMEDIA_STATUS_UNSUPPORTED;
}

const gchar *gmedia_xmp_get_media_name ()
{
  if(xmp_data->loaded)
    return "Module file";
  else
    return "";
}

const gchar *gmedia_xmp_get_track_name (int ntrack)
{
  if(xmp_data->loaded==2) {
    if (ntrack==1)
      return xmp_data->mi.title;
    else
      return "No track";
  } else
    return "";
}

int gmedia_xmp_is_streamable ()
{
  return FALSE;
}

int gmedia_xmp_needs_drawable ()
{
  return FALSE;
}

int gmedia_xmp_get_fd (int *fd)
{
  return GMEDIA_STATUS_UNSUPPORTED;
}

GMediaMode gmedia_xmp_get_mode ()
{
  return xmp_data->playing_mode;
}

int gmedia_xmp_get_num_parameters ()
{
  return 1;
}

int gmedia_xmp_get_parameter (int n, GMediaValue *value)
{
  switch (n) {
  case 0:
    value->name = "Test";
    value->description = "A test parameter";
    value->type = GMEDIA_TYPE_BOOL;
    value->val.b = TRUE;
     return GMEDIA_STATUS_GOOD;
    break;
  default:
    return GMEDIA_STATUS_INVAL;
  }
  return GMEDIA_STATUS_INVAL;
}

int gmedia_xmp_get_driver_info (GMediaDriverInfo *info)
{
  info->name = "XMP";
  info->full_name = "XMP GMedia driver";
  info->version_major = 0;
  info->version_minor = 1;
}
