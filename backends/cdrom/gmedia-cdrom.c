#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/cdrom.h>

#include "libgmedia/gmedia.h"

#ifndef MAXPATH
#define MAXPATH 1024
#endif

/* Shared memory stuff */
static int shmid;
static int pfd1[2], pfd2[2];
static struct cdrom_data_t {
    int loaded;
    GMediaMode pending_mode;
    GMediaMode playing_mode;
    time_t playing_time;

    gchar device[MAXPATH];
    int fd;
    int trk_first, trk_last;	/* length of the cd */

    int trk_current;		/* this is what the user wants, set by set_track */
    int trk_playing;		/* this is what's actually happening. */
} *cdrom_data = 0;

/* The initialization function. Because a gmedia DLL should be asynchronous,
   it should setup IPC mechanisms for communication between the processes.
   Obviously everything is much easier if you use threads */
int gmedia_cdrom_init()
{
    int status;

    /* Prepare data structures and other amenities */
    shmid = shmget(IPC_PRIVATE, sizeof(struct cdrom_data_t),
		   IPC_CREAT | 0600);
    if (shmid==-1)
	return GMEDIA_STATUS_ERROR;
    cdrom_data = shmat(shmid,0,0);
    memset(cdrom_data,0,sizeof(struct cdrom_data_t));

    cdrom_data->playing_mode = GMEDIA_MODE_STOPPED;
    cdrom_data->pending_mode = GMEDIA_MODE_UNAVAILABLE;
    cdrom_data->playing_time = 0;
  
    status = pipe(pfd1) || pipe(pfd2);

    return status;
}

int gmedia_cdrom_exit()
{
/*    if(cdrom_data->loaded)
      gmedia_cdrom_eject();*/	/* we don't really want to eject on exit */

    close(cdrom_data->fd);
  
    /* Nuke stuff allocated at initialization time */
    shmctl (shmid, IPC_RMID, NULL);
    shmdt((void *)cdrom_data);
    /* Close the pipes */
    close(pfd1[0]);
    close(pfd1[1]);
    close(pfd2[0]);
    close(pfd2[1]);
}

int gmedia_cdrom_load(gchar *dev)
{
    struct cdrom_tochdr toc;

    strcpy(cdrom_data->device,dev);
    cdrom_data->loaded = 1;
    
    cdrom_data->fd = open(cdrom_data->device, O_RDONLY | O_NONBLOCK);
    if(cdrom_data->fd < 0)
	return GMEDIA_STATUS_INVAL;

    if(ioctl(cdrom_data->fd, CDROMREADTOCHDR, &toc) < 0)
	return GMEDIA_STATUS_ERROR;

    cdrom_data->trk_first = toc.cdth_trk0;
    cdrom_data->trk_last = toc.cdth_trk1;

    return GMEDIA_STATUS_GOOD;
}

int _update_time(void)
{
    struct cdrom_subchnl sc;

    sc.cdsc_format = CDROM_MSF;

    if(ioctl(cdrom_data->fd, CDROMSUBCHNL, &sc) < 0)
	return GMEDIA_STATUS_ERROR;

    switch(sc.cdsc_audiostatus)
    {
    case CDROM_AUDIO_PLAY:
	cdrom_data->playing_mode = GMEDIA_MODE_PLAYING;
	break;
    case CDROM_AUDIO_PAUSED:
	cdrom_data->playing_mode = GMEDIA_MODE_PAUSED;
	break;
    case CDROM_AUDIO_NO_STATUS:
    case CDROM_AUDIO_COMPLETED:
	cdrom_data->playing_mode = GMEDIA_MODE_STOPPED;
    case CDROM_AUDIO_INVALID:
    case CDROM_AUDIO_ERROR:
    default:
	cdrom_data->playing_mode = GMEDIA_MODE_UNAVAILABLE;
	break;
    }

    cdrom_data->trk_playing = sc.cdsc_trk;
    /* FIXME get current time and such here */
}    
    
int gmedia_cdrom_play()
{
    int pid, status;
    struct cdrom_ti ti;

    if(!cdrom_data->loaded)
	return GMEDIA_STATUS_INVAL;

    if(cdrom_data->playing_mode!=GMEDIA_MODE_STOPPED)
	return GMEDIA_STATUS_INVAL;

    /* make sure trk_current holds something we can play */
    if(cdrom_data->trk_current < cdrom_data->trk_first ||
       cdrom_data->trk_current > cdrom_data->trk_last)
	cdrom_data->trk_current = cdrom_data->trk_first;

    /* play */
    cdrom_data->pending_mode = GMEDIA_MODE_PLAYING;
    ti.cdti_trk0 = cdrom_data->trk_current;
    ti.cdti_ind0 = 1;
    ti.cdti_trk1 = cdrom_data->trk_current+1; /* FIXME this may be broken on some drives */
    ti.cdti_ind1 = 1;
    
    if(ioctl(cdrom_data->fd, CDROMPLAYTRKIND, &ti) < 0)
	return GMEDIA_STATUS_ERROR;
    cdrom_data->playing_mode = GMEDIA_MODE_PLAYING;
    
    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_pause()
{
    switch(cdrom_data->playing_mode) {
    case GMEDIA_MODE_PLAYING:
	cdrom_data->pending_mode = GMEDIA_MODE_PAUSED;
	if(ioctl(cdrom_data->fd, CDROMPAUSE) < 0)
	    return GMEDIA_STATUS_ERROR;
	cdrom_data->playing_mode = GMEDIA_MODE_PAUSED;
	break;
    case GMEDIA_MODE_PAUSED:
	cdrom_data->pending_mode = GMEDIA_MODE_PLAYING;
	if(ioctl(cdrom_data->fd, CDROMRESUME) < 0)
	    return GMEDIA_STATUS_ERROR;
	cdrom_data->playing_mode = GMEDIA_MODE_PLAYING;
	break;
    default:
	break;
    }

    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_stop()
{
    if(cdrom_data->playing_mode!=GMEDIA_MODE_STOPPED) {
	cdrom_data->pending_mode = GMEDIA_MODE_STOPPED;
	if(ioctl(cdrom_data->fd, CDROMSTOP) < 0)
	    return GMEDIA_STATUS_ERROR;
	cdrom_data->playing_mode = GMEDIA_MODE_STOPPED;
    }
    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_forward()
{
    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_rewind()
{
    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_restart ()
{
    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_eject()
{
    if(cdrom_data->loaded) {
	if(cdrom_data->playing_mode!=GMEDIA_MODE_STOPPED)
	    gmedia_cdrom_stop();
	if(ioctl(cdrom_data->fd, CDROMEJECT) < 0)
	    return GMEDIA_STATUS_ERROR;
	cdrom_data->loaded = 0;
    }
    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_get_n_frames()
{
    return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_cdrom_set_frame()
{
    return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_cdrom_get_frame()
{
    return GMEDIA_STATUS_UNSUPPORTED;
}

int gmedia_cdrom_get_n_tracks ()
{
    return cdrom_data->trk_last;
}

int gmedia_cdrom_set_track (int ntrack)
{
    if(ntrack < cdrom_data->trk_first ||
       ntrack > cdrom_data->trk_last)
	return GMEDIA_STATUS_INVAL;

    cdrom_data->trk_current = ntrack;

    return GMEDIA_STATUS_GOOD;
}

int gmedia_cdrom_get_track ()
{
    if(cdrom_data->playing_mode==GMEDIA_MODE_STOPPED)
	return 0; /* Return 0 if we're not playing anything */
    else
	return cdrom_data->trk_playing; /* FIXME this doesn't work yet */
}

int gmedia_cdrom_check(gchar *dev)
{
    if(strstr(dev,"dev"))	/* FIXME this isn't good enough */
	return GMEDIA_STATUS_GOOD;
    else
	return GMEDIA_STATUS_UNSUPPORTED;
}

const gchar *gmedia_cdrom_get_media_name ()
{
    return "CDROM Audio";
}

/* this should support cddbslave sometime...sometime... */
const gchar *gmedia_cdrom_get_track_name (int ntrack)
{
    char *tmp;

    if(ntrack < cdrom_data->trk_first ||
       ntrack > cdrom_data->trk_last)
	return "No track";
    else
	return "Track";
}

int gmedia_cdrom_is_streamable ()
{
    return FALSE;
}

int gmedia_cdrom_needs_drawable ()
{
    return FALSE;
}

int gmedia_cdrom_get_fd (int *fd)
{
    return cdrom_data->fd;
}

GMediaMode gmedia_cdrom_get_mode ()
{
    return cdrom_data->playing_mode;
}

int gmedia_cdrom_get_num_parameters ()
{
    return 1;
}

int gmedia_cdrom_get_parameter (int n, GMediaValue *value)
{
    switch (n) {
    case 0:
	value->name = "Test";
	value->description = "A test parameter";
	value->type = GMEDIA_TYPE_BOOL;
	value->val.b = TRUE;
	return GMEDIA_STATUS_GOOD;
	break;
    default:
	return GMEDIA_STATUS_INVAL;
    }
    return GMEDIA_STATUS_INVAL;
}

int gmedia_cdrom_get_driver_info (GMediaDriverInfo *info)
{
    info->name = "Cdrom";
    info->full_name = "Cdrom GMedia driver";
    info->version_major = 0;
    info->version_minor = 1;
}

